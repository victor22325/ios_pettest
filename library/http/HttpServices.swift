//
//  HttpServices.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import SwiftyJSON

class HttpServices{
    
    let httpUrl = "http://service.broadmaster-biotech.com:50002/web/http/post.php"
    
    static let sharedInstance = HttpServices()
    
    func postConnect(_ funcName: String,httpString:String,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, httpString: httpString, method: "POST", completion: completion)
    }
    
    func getConnect(_ funcName: String,httpString:String,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, httpString: httpString, method: "GET", completion: completion)
    }
    
    func putConnect(_ funcName: String,httpString:String,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, httpString: httpString, method: "PUT", completion: completion)
    }
    
    
    func connect(_ funcName: String,httpString:String,method:String,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        
        let request = NSMutableURLRequest(url: NSURL(string: httpUrl)! as URL)
        request.httpMethod = "\(method)"
        let httpAction = "\(funcName)"
        let postString = "func=\(httpAction)&vars=\(httpString)"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest,completionHandler: { (Data, URLResponse, Error) in
            if Error == nil {
                if let data = Data, let result = String(data: data, encoding: String.Encoding.utf8) {
                    completion(true,result)
                }
            } else {
                completion(false,"")
                print(Error?.localizedDescription ?? Error.debugDescription)
            }
            
        })
        task.resume()

        
    }
    
    func getAppCall (_ data: String)->JSON{
        //回傳json
        let json = JSON(parseJSON: data)
    
        return json
    }
}
