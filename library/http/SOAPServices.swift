//
//  SOAPServices.swift
//
//
//  Created by John on 3/9/17.
//
//

import UIKit
import SwiftyJSON

class SOAPServices: NSObject {
    //設定asmx位置
    fileprivate var urlXML: String = "http://service.broadmaster-biotech.com:50002/web/index.php?option=com_csoap&id=1#"
    //soap 1.1標頭
    fileprivate var headerXML: String = "<?xml version='1.0' encoding='utf-8'?><soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:appcall'><soapenv:Header/><soapenv:Body>"
    //soap 1.1結尾
    fileprivate var footerXML: String = "</soapenv:Body></soapenv:Envelope>"
    
    static let sharedInstance = SOAPServices()
    
    //重新設定asmx
    func initHostSoapService(_ urlXML: String!) {
        self.urlXML = urlXML
    }
    
    //重新設定 asmx header footet
    func initHostSoapService(_ urlXML: String!,headerXML: String!,footerXML: String!) {
        self.urlXML = urlXML
        self.headerXML = headerXML
        self.footerXML = footerXML
    }
    
    func postConnect(_ funcName: String,dic:NSDictionary,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, dic: dic, method: "POST", completion: completion)
    }
    
    func getConnect(_ funcName: String,dic:NSDictionary,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, dic: dic, method: "GET", completion: completion)
    }
    
    func putConnect(_ funcName: String,dic:NSDictionary,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        self.connect(funcName, dic: dic, method: "PUT", completion: completion)
    }
    
    func connect(_ funcName: String,dic:NSDictionary,method:String,completion: @escaping (_ status:Bool,_ result: String?) -> Void) {
        var stringXML : String = ""
        
        for (key,value) in dic {
            stringXML += "<\(key) xsi:type='xsd:string'>\(value)</\(key)>"
        }
        //SOAP Method 需更換xmlns才可使用
        let headerString = "\(self.headerXML)<\(funcName) xmlns='http://service.broadmaster-biotech.com'>"
        let footerString = "</\(funcName)>\(self.footerXML)"
        
        let soapMessage = "\(headerString)\(stringXML)\(footerString)"
        
        
        if let url = URL(string: self.urlXML) {
            var theRequest = URLRequest(url: url)
            theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
            theRequest.addValue((soapMessage), forHTTPHeaderField: "Content-Length")
            theRequest.httpMethod = method
            theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false)
            theRequest.timeoutInterval = TimeInterval(240)
            URLSession.shared.dataTask(with: theRequest, completionHandler: { (Data, URLResponse, Error) in
                if Error == nil {
                    if let data = Data, let result = String(data: data, encoding: String.Encoding.utf8) {
                        completion(true,result)
                    }
                } else {
                    completion(false,"")
                    print(Error?.localizedDescription ?? Error.debugDescription)
                }
                
            }).resume()
        }
        
    }
    
    func setDataDic(soapFunc:String,data:NSDictionary)->NSDictionary{
        //設定要傳的soap value
        let dataDic:NSDictionary = [
            "vars": "\(self.setVarsString(data: data))",
            "func": "\(soapFunc)"
        ]
        return dataDic
    }
    
    func getAppCall (_ data: String)->JSON{
        //取得中間Data資料 並過濾無用資料
        
        let getData = data.components(separatedBy: "<return xsi:type=\"xsd:string\">")
        let getDataEnd = getData[1].components(separatedBy: "</return>")
        let replaceDate = getDataEnd[0].replacingOccurrences(of: "&quot;", with: "\"")
        //回傳json

        return JSON(parseJSON: replaceDate)
    }
    
    func setVarsString (data:NSDictionary)->String{
        //轉換成Vars需要的字串型態
        var result : String = ""
        
        result += "{"
        for (key,value) in data {
            result += "'\(key)' : '\(value)',"
        }
        //去除最後一個逗號 從尾巴往前一位
        let index = result.index(result.endIndex, offsetBy: -1)
        result = result.substring(to: index)
        result += "}"
        
        return result
    }
    
    //上傳Data(血糖 血壓 額溫)用
    
    func setDataDicData(soapFunc:String,data:NSDictionary)->NSDictionary{
        //設定要傳的soap value
        let dataDic:NSDictionary = [
            "vars": "\(self.setVarsStringData(data: data))",
            "func": "\(soapFunc)"
        ]
        return dataDic
    }
    
    func setVarsStringData (data:NSDictionary)->String{
        //轉換成Vars需要的字串型態
        var result : String = ""
        var split = 0
        
        result += "{"
        for (key,value) in data {
            if(split == 0){
                result += "'\(key)' : '\(value)',"
            }
            else{
                result += "'\(key)' : \(value),"
            }
            split = split + 1
        }
        //去除最後一個逗號 從尾巴往前一位
        let index = result.index(result.endIndex, offsetBy: -1)
        result = result.substring(to: index)
        result += "}"
        
        return result
    }

    
}
