//
//  optionBar.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/31.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import UIKit
import Presentr

class OptionBar{
    
    init(){
    
    }
    
     // MARK: - 建立option選單 並回傳
    
    func addOptionsMenu(ViewController: UIViewController)->CAPSOptionsMenu{
        
        var optionsMenu: CAPSOptionsMenu
        
        let image = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        
        let addButton = UIBarButtonItem(image: image, style: .plain, target: nil, action: nil)
        
        
        
        //let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
        ViewController.navigationItem.rightBarButtonItem = addButton
        
        optionsMenu = CAPSOptionsMenu(viewController: ViewController, barButtonItem: addButton, keepBarButtonAtEdge: true)
        optionsMenu.menuActionButtonsHighlightedColor(UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0))
        optionsMenu.menuCornerRadius(2.0)
        
        let menuAction1: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Profile") { (action: CAPSOptionsMenuAction) -> Void in
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            ViewController.present(controller, animated: true, completion: nil)
            
        }
        
        optionsMenu.addAction(menuAction1)
        
        let menuAction8: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Select Pet") { (action: CAPSOptionsMenuAction) -> Void in
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.80)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = false
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: SelectPetViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "selectPetViewController")
                return popupViewController as! SelectPetViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            ViewController.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            
        }
        
         optionsMenu.addAction(menuAction8)
        
        let menuAction2: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Instruction") { (action: CAPSOptionsMenuAction) -> Void in
            
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            ViewController.present(controller, animated: true, completion: nil)
            
        }
        optionsMenu.addAction(menuAction2)
        
        let menuAction3: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "About") { (action: CAPSOptionsMenuAction) -> Void in
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.30)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: aboutViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "aboutViewController")
                return popupViewController as! aboutViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            ViewController.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            
        }
        optionsMenu.addAction(menuAction3)
        
        let menuAction4: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Version") { (action: CAPSOptionsMenuAction) -> Void in
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.custom(size: 200)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: versionViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "versionViewController")
                return popupViewController as! versionViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            ViewController.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
        }
        optionsMenu.addAction(menuAction4)
        
//        let menuAction5: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Device Select") { (action: CAPSOptionsMenuAction) -> Void in
//            let storyboard = UIStoryboard(name: "Base",bundle:nil)
//            let controller = storyboard.instantiateViewController(withIdentifier: "devictSelect")
//            ViewController.present(controller, animated: true, completion: nil)
//
//        }
        //取消裝置選擇
        //optionsMenu.addAction(menuAction5)
        
        let menuAction6: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Logout") { (action: CAPSOptionsMenuAction) -> Void in
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            ViewController.present(controller, animated: true, completion: nil)
        }
        optionsMenu.addAction(menuAction6)
        
        let menuAction7: CAPSOptionsMenuAction = CAPSOptionsMenuAction(title: "Exit") { (action: CAPSOptionsMenuAction) -> Void in
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
        }
        optionsMenu.addAction(menuAction7)
        
        return optionsMenu
    }
    
}
