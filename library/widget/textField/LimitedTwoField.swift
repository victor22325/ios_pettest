//
//  LimitedTwoField.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/4.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

@IBDesignable class LimitedTwoField: UITextField {
    @IBInspectable var maxLength: Int = 2    // set a default value
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
    }
    
    @objc func editingChanged(sender: UITextField) {
        guard let text = sender.text?.prefix(maxLength) else { return }
        sender.text = String(text)
    }
}
