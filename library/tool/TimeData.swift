//
//  TimeData.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/20.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation


class TimeData{

    static let sharedInstance = TimeData()
    
    //MARK: - 設定日期時間字串
    func setDayTimeFormat(data: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = dateFormatter.string(from: data)
        
        return dateString
    }
    
    
    //MARK: - 設定日期字串
    func setDayLabel(data: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "/dd/yyyy"
        let monthDormatter = DateFormatter()
        monthDormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        monthDormatter.dateFormat = "MMM"
        
        let dateString = monthDormatter.string(from: data) + dateFormatter.string(from: data)
        return dateString
    }
    
    //MARK: - 設定日期字串(database format)
    func setDayLabelDatabase(data: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = dateFormatter.string(from: data)
        return dateString
    }
    
    //MARK: - 設定日期
    func setDay(data: Date,dateChange: Date)->Date{
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: data)
        var componentsClick = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateChange)
        
        components.year = componentsClick.year
        components.month = componentsClick.month
        components.day = componentsClick.day
        
        return gregorian.date(from: components)!
    }
    
    //MARK: - 設定時間字串
    func setTimeLabel(data: Date)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        
        let dateString = dateFormatter.string(from: data)
        
        return dateString
    }
    
    //MARK: - 設定時間
    func setTime(data: Date,dateChange: Date)->Date{
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: data)
        var componentsClick = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateChange)
        
        components.hour = componentsClick.hour
        components.minute = componentsClick.minute
        
        
        return gregorian.date(from: components)!
    }
    
    //MARK: - 字串轉Unix Time
    func stringToTimestamp(timeString: String)->Int64{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        let date = dfmatter.date(from: "\(timeString)")
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int64 = Int64(dateStamp)
        return dateSt
    }
    
    //MARK: - 移動目前月份(須為負數) 並回傳Unix Time
    func timestampShift(shiftMonth: Int64)->Int64{
        let now = Date()
        let monthsToAdd = shiftMonth
        var dateComponent = DateComponents()
        dateComponent.month = Int(monthsToAdd)
        let dateSet = Calendar.current.date(byAdding: dateComponent, to: now)
        let timeIntervalSet:TimeInterval = dateSet!.timeIntervalSince1970
        let timeStampSet = Int64(timeIntervalSet)
        
        return timeStampSet
    }
    
    //MARK: - 回傳Unix Time
    func timestampShift(date: Date)->Int{
        let timeIntervalSet:TimeInterval = date.timeIntervalSince1970
        let timeStampSet = Int(timeIntervalSet)
        
        return timeStampSet
    }
    
    
    //MARK: - 設定字串對應TimeInterval
    
    func getTimeInterval(time: Int,type: String)->Int{
        var unit = 1
        
        switch type {
        case "sec":
            unit = 1
        case "min":
            unit = 60
        case "hour":
            unit = 3600
        case "day":
            unit = 86400
        default:
            break
        }
        
        return (time * unit)
        
    }
    
    
    //MARK: - 設定停醒時間字串(water)
    
    func setTimeInterval(startTime: String,endTime: String,timeInterval: Int)->[Date]{
        var dateArray:[Date] = []
        let startTimeArray = startTime.components(separatedBy: ":")
        let endTimeArray = endTime.components(separatedBy: ":")
        let startTimeInt:Int = Int(startTimeArray[0])!*60 + Int(startTimeArray[1])!
        let endTimeInt:Int = Int(endTimeArray[0])!*60 + Int(endTimeArray[1])!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        var startTimeDate = dateFormatter.date(from: startTime)
        dateArray.append(startTimeDate!)
        
        if(endTimeInt - startTimeInt < timeInterval){
            return dateArray
        }
        
        for _ in stride(from: 0, to: endTimeInt - startTimeInt, by: timeInterval){
            var dateComponent = DateComponents()
            dateComponent.minute = timeInterval
            startTimeDate = Calendar.current.date(byAdding: dateComponent, to: startTimeDate!)
            dateArray.append(startTimeDate!)
        }
        
        return dateArray
    }
    
    //MARK: - 設定停醒時間字串(medicine)
    
    func setTimeInterval(selectTimeArray: [String],timeBefore: Int)->[Date]{
        var dateArray:[Date] = []
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        for i in 0 ..< selectTimeArray.count{
            var startTimeDate = dateFormatter.date(from: selectTimeArray[i])
            var dateComponent = DateComponents()
            dateComponent.minute = -timeBefore
            startTimeDate = Calendar.current.date(byAdding: dateComponent, to: startTimeDate!)
            dateArray.append(startTimeDate!)
        }
        
        return dateArray
    }

}
