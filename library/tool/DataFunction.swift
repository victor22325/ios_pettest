//
//  DataFunction.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/18.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import UIKit


class DataFunction{

    init(){
    
    }
    
    // MARK: - 輸出目前所選則單位
    
    func setUnit(DeviceStatus: Int)->String{
        var result = ""
        switch DeviceStatus{
        case 0:
            if(UserGetSetData.GlucoseUnit == "1" ){
                result = "mg/dL"
            }
            else{
                result = "mmol"
            }
        case 1:
            if(UserGetSetData.PressureUnit == "0" ){
                result = "mmHg"
            }
            else{
                result = "kPa"
            }
        case 2:
            if(UserGetSetData.TemperatureUnit == "0" ){
                result = "°C"
            }
            else{
                result = "°F"
            }
        default:
            result = "Unit"
            break
        }
        return result
    }
    
    // MARK: - 輸出目前所選擇裝置
    
    func setDeviceStatus(statusString: String)->Int{
        var result = 0
        switch statusString{
        case "glucose":
            result = 0
        case "pressure":
            result = 1
        case "temperature":
            result = 2
        default:
            
            break
        }
        return result
    }
    
    
    // MARK: - 輸出目前所選擇裝置
    
    func setStatus(statusString: String)->Int{
        var result = 0
        switch statusString{
        case "Post-Meal","Diastolic","Ambient":
            result = 1
        case "Pre-Meal","Systolic","Forehead":
            result = 2
        case "General","HeartRate","General":
            result = 3
        default:
            
            break
        }
        return result
    }

    
    // MARK: - 設定資料Status對應String
    
    func setDataStatus(DeviceStatus: Int,Status: Int)->String{
        var result = ""
        switch DeviceStatus{
        case 0:
            if(Status == 1){
                result = "Post-Meal"
            }
            else if(Status == 2){
                result = "Pre-Meal"
            }
            else{
                result = "General"
            }
        case 1:
            if(Status == 1){
                result = "Diastolic"
            }
            else if(Status == 2){
                result = "Systolic"
            }
            else{
                result = "HeartRate"
            }
        case 2:
            if(Status == 1){
                result = "Ambient"
            }
            else if(Status == 2){
                result = "Forehead"
            }
            else{
                result = "General"
            }
        default:
            
            break
        }
        return result
    }
    
    func setDataStatusImage(DeviceStatus: Int,Status: Int)->UIImage{
        var result = UIImage(named: "dog_list")!
        switch DeviceStatus{
        case 0:
            if(Status == 2){
                result = UIImage(named: "dog_list")!
            }
            else if(Status == 3){
                result = UIImage(named: "cat_list")!
            }
            else{
                result = UIImage(named: "dog_list")!
            }
        default:
            
            break
        }
        return result
    }

    
    // MARK: - 設定資料合理範圍顏色（血壓 血糖 額溫）
    
    func setColor(value: Int,DeviceStatus: Int,Status: Int)->UIColor{
        var result = UIColor(red: 127.0/255.0, green: 15.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        let max = setMax(DeviceStatus: DeviceStatus,Status: Status)
        let min = setMin(DeviceStatus: DeviceStatus,Status: Status)
        
        if(max<value){
            result = UIColor(red: 219.0/255.0, green: 68.0/255.0, blue: 55.0/255.0, alpha: 1.0)
        }
        else if(min>value){
            result = UIColor(red: 253.0/255.0, green: 216.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        }
        else{
            result = UIColor(red: 104.0/255.0, green: 159.0/255.0, blue: 56.0/255.0, alpha: 1.0)
        }
        return result
    }
    
    // MARK: - 設定資料合理範圍顏色 附範圍（血壓 血糖 額溫)
    
    func setColor(value: Int,DeviceStatus: Int,Status: Int,max: Int,min: Int)->UIColor{
        var result = UIColor(red: 127.0/255.0, green: 15.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        
        if(max<value){
            result = UIColor(red: 219.0/255.0, green: 68.0/255.0, blue: 55.0/255.0, alpha: 1.0)
        }
        else if(min>value){
            result = UIColor(red: 253.0/255.0, green: 216.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        }
        else{
            result = UIColor(red: 104.0/255.0, green: 159.0/255.0, blue: 56.0/255.0, alpha: 1.0)
        }
        return result
    }
    
    // MARK: - 設定資料合理範圍顏色 附範圍（cell)
    
    func setCellColor(value: Int,DeviceStatus: Int,Status: Int,max: Int,min: Int)->UIColor{
        var result = UIColor(red: 127.0/255.0, green: 15.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        
        if(max<value){
            result = UIColor(red: 249.0/255.0, green: 168.0/255.0, blue: 173.0/255.0, alpha: 1.0)
        }
        else if(min>value){
            result = UIColor(red: 249.0/255.0, green: 235.0/255.0, blue: 168.0/255.0, alpha: 1.0)
        }
        else{
            result = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }
        return result
    }
    
    // MARK: - 設定資料合理範圍最大值（血壓 血糖 額溫）
    
    func setMax(DeviceStatus: Int,Status: Int)->Int{
        var result = 0
        switch DeviceStatus{
        case 0:
            if(Status == 1){
                result = Int(UserGetSetData.PostMealUpper)!
            }
            else if(Status == 2){
                result = Int(UserGetSetData.PreMealUpper)!
            }
            else{
                result = Int(UserGetSetData.PostMealUpper)!
            }
        case 1:
            if(Status == 1){
                result = Int(UserGetSetData.SystolicUpper)!
            }
            else if(Status == 2){
                result = Int(UserGetSetData.DiastolicUpper)!
            }
            else{
                result = 90
            }
        case 2:
            if(Status == 1){
                let splitData = UserGetSetData.AmbientUpper.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            else if(Status == 2){
                let splitData = UserGetSetData.ForeheadUpper.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            else{
                let splitData = UserGetSetData.ForeheadUpper.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            
        default:
            
            break
        }
        return result
    }
    
    // MARK: - 設定資料合理範圍最小值（血壓 血糖 額溫）
    
    func setMin(DeviceStatus: Int,Status: Int)->Int{
        var result = 0
        switch DeviceStatus{
        case 0:
            if(Status == 1){
                result = Int(UserGetSetData.PostMealLower)!
            }
            else if(Status == 2){
                result = Int(UserGetSetData.PreMealLower)!
            }
            else{
                result = Int(UserGetSetData.PostMealLower)!
            }
        case 1:
            if(Status == 1){
                result = Int(UserGetSetData.SystolicLower)!
            }
            else if(Status == 2){
                result = Int(UserGetSetData.DiastolicLower)!
            }
            else{
                result = 70
            }
        case 2:
            if(Status == 1){
                let splitData = UserGetSetData.AmbientLower.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            else if(Status == 2){
                let splitData = UserGetSetData.ForeheadLower.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            else{
                let splitData = UserGetSetData.ForeheadLower.components(separatedBy: "-")
                result = Int("\(splitData[0])\(splitData[1])")!
            }
            
        default:
            
            break
        }
        return result
    }
    
    
    
    // MARK: - 個位數值補零
    
    func addZero(input: UInt64)->String{
        var result = String(input)
        if (input<10){
            result = "0\(result)"
        }
        return result
    }


}
