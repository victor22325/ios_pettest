//
//  BleFunction.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/16.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
//import RxBluetoothKit
//import RxSwift
//import CoreBluetooth

class BleFunction{
    
//    static var manager: CentralManager!
//    static var scannedPeripheral: ScannedPeripheral!
    static var connectFlag:Bool = false
    
    static var serviceUUID = ""
    static var notifyUUID = ""
    static var writeUUID = ""
    static var downloadCmd = ""
    static var closeCmd = ""
    
    static var downloadResult: Dictionary<String, [UInt64]> = [:]
    
    // MARK: - 依照目前所選擇裝置 初始化藍牙參數
    
    init(DeviceStatus: Int){
        switch DeviceStatus {
        case 0:
            BleFunction.serviceUUID = "FFF0"
            BleFunction.notifyUUID = "FFF1"
            BleFunction.writeUUID = "FFF2"
            BleFunction.downloadCmd = "CE00B10D0A"
            BleFunction.closeCmd = "CE01A7000D0A"
        case 1:
            BleFunction.serviceUUID = "FC00"
            BleFunction.notifyUUID = "FCA1"
            BleFunction.writeUUID = "FCA0"
            BleFunction.downloadCmd = "0400ACB0"
            BleFunction.closeCmd = "0400A6AA"
        case 2:
            BleFunction.serviceUUID = "FC00"
            BleFunction.notifyUUID = "FCA1"
            BleFunction.writeUUID = "FCA0"
            BleFunction.downloadCmd = "FE6A725B01000038"
            BleFunction.closeCmd = ""
        default:
            break
        }
    }
    
    // MARK: - 血壓Notify初始化
    
    func pressureInit()->String{
        return "0400A0A4"
    }
    
    // MARK: - 血糖Meter設備名稱Function
    
    func glucoseDeviceName()->String{
        return "CE00B30D0A"
    }
    
    // MARK: - 確認血糖Meter設備名稱（檢查回傳值）
    
    func glucoseCheckDeviceName(NotifyResult: String)->String{
        var result = ""
        let NotifyResultArray : [String] = NotifyResult.components(separatedBy: "-")
        
        for i in 0 ..< NotifyResultArray.count{
            if(NotifyResultArray[i].contains("ce0ae5") && NotifyResultArray[i].contains("0d0a")){
                for j in 0..<5{
                    result = result + String(describing: UnicodeScalar(Int(NotifyResultArray[i].substring(with: (j*2)+6..<(j*2)+8), radix:16)!)!)
                }
            }
        }        
        return result
    }
    
    // MARK: - 血糖Meter時間設定Function
    
    func glucoseTime()->String{
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        
        return "CE05A0\(addZero(input: year%100))\(addZero(input: month))\(addZero(input: day))\(addZero(input: hour))\(addZero(input: minute))0D0A"
    }
    
    // MARK: - 確認血糖Meter時間（檢查回傳值）
    
    func glucoseCheckOK(NotifyResult: String)->Bool{
        if(NotifyResult.contains("ce02c04f4b0d0a")){
            return true
        }
        else{
            return false
        }
    }
    
    // MARK: - 血糖Meter 回傳資料 Formater
    
    func glucoseDataFormat(NotifyResult: String)->Dictionary<String, [UInt64]>{
        var temperature:[UInt64] = []
        var status:[UInt64] = []
        var value:[UInt64] = []
        var year:[UInt64] = []
        var month:[UInt64] = []
        var day:[UInt64] = []
        var hour:[UInt64] = []
        var minute:[UInt64] = []
        
        let NotifyResultArray : [String] = NotifyResult.components(separatedBy: "-")
        
        for i in 0 ..< NotifyResultArray.count{
            if(NotifyResultArray[i].contains("ce0fe1") && NotifyResultArray[i].contains("0d0a") && checkPetData(petSpecies: UserGetSetData.PetSpecies,status: UInt64(NotifyResultArray[i].substring(with: 14..<16), radix:16)!)){
                temperature.append(UInt64(NotifyResultArray[i].substring(with: 10..<14), radix:16)!)
                status.append(UInt64(NotifyResultArray[i].substring(with: 14..<16), radix:16)!)
                value.append(UInt64(NotifyResultArray[i].substring(with: 16..<20), radix:16)!)
                year.append(UInt64(NotifyResultArray[i].substring(with: 20..<22), radix:16)!)
                month.append(UInt64(NotifyResultArray[i].substring(with: 22..<24), radix:16)! % 13)
                day.append(UInt64(NotifyResultArray[i].substring(with: 24..<26), radix:16)! % 32)
                hour.append(UInt64(NotifyResultArray[i].substring(with: 26..<28), radix:16)! % 60)
                minute.append(UInt64(NotifyResultArray[i].substring(with: 28..<30), radix:16)! % 60)
            }
        }
        
        let DictionaryResult: Dictionary<String, [UInt64]> = [
            "temperature" : temperature,
            "status" : status,
            "value" : value,
            "year" : year,
            "month" : month,
            "day" : day,
            "hour" : hour,
            "minute" : minute
        ]
        
        return DictionaryResult
    }
    
    // MARK: - 血壓Meter 月份日期設定 Function
    
    func pressureDay()->String{
        let date = Date()
        let calendar = Calendar.current
        let year = String(format:"%02X",calendar.component(.year, from: date)%100)
        let month = String(format:"%02X",calendar.component(.month, from: date))
        let day = String(format:"%02X",calendar.component(.day, from: date))
        let check = 7+(10*16)+8+calendar.component(.year, from: date)%100+calendar.component(.month, from: date)+calendar.component(.day, from: date)
        let checkHex = String(format:"%02X",check)
        
        return "0700A8\(year)\(month)\(day)\(checkHex)"
    }
    
    // MARK: - 血壓Meter 時間設定 Function
    
    func pressureTime()->String{
        //溢位問題未解決
        let date = Date()
        let calendar = Calendar.current
        let hour = String(format:"%02X",calendar.component(.hour, from: date))
        let minute = String(format:"%02X",calendar.component(.minute, from: date))
        var second = String(format:"%02X",calendar.component(.second, from: date))
        second = "00"
        //let check = 7+(10*16)+9+calendar.component(.hour, from: date)+calendar.component(.minute, from: date)+calendar.component(.second, from: date)
                let check = 7+(10*16)+9+calendar.component(.hour, from: date)+calendar.component(.minute, from: date)
        let checkHex = String(format:"%02X",check)
            
        print("pressureTime : 0700A9\(hour)\(minute)\(second)\(checkHex)")
        return "0700A9\(hour)\(minute)\(second)\(checkHex)"
    }

    
    // MARK: - 血壓Meter 回傳資料 Formater
    
    func pressureDataFormat(NotifyResult: String)->Dictionary<String, [UInt64]>{
        var bleDataArray:[String] = []
        var bleDayArray:[String] = []
        var bleTimeArray:[String] = []
        
        var temperature:[UInt64] = []
        var status:[UInt64] = []
        var value:[UInt64] = []
        var year:[UInt64] = []
        var month:[UInt64] = []
        var day:[UInt64] = []
        var hour:[UInt64] = []
        var minute:[UInt64] = []
        
        let NotifyResultArray : [String] = NotifyResult.components(separatedBy: "-")
        var NotifyResultSum:String  = ""
        for i in 0 ..< NotifyResultArray.count{
            NotifyResultSum = NotifyResultSum + NotifyResultArray[i]
        }
        
        for i in 0 ..< NotifyResultSum.count-12{
            if checkData(Data: NotifyResultSum.substring(with: i..<i+6),Checker: "0801b8"){
                if(i+16<NotifyResultSum.count){
                    print(NotifyResultSum.substring(with: i..<i+16))
                    bleDataArray.append(NotifyResultSum.substring(with: i..<i+16))
                }
            }
            else if checkData(Data: NotifyResultSum.substring(with: i..<i+6),Checker: "0701bd"){
                if(i+14<NotifyResultSum.count){
                    print(NotifyResultSum.substring(with: i..<i+14))
                    bleDayArray.append(NotifyResultSum.substring(with: i..<i+14))
                }
            }
            else if checkData(Data: NotifyResultSum.substring(with: i..<i+6),Checker: "0601be"){
                print(NotifyResultSum.substring(with: i..<i+12))
                bleTimeArray.append(NotifyResultSum.substring(with: i..<i+12))
            }
        }
        
        //to fix pressure ble data bug
        
        if(bleDataArray.count > bleDayArray.count){
            bleDayArray.append(bleDayArray[0])
        }
        if(bleDataArray.count > bleTimeArray.count){
            bleTimeArray.append(bleTimeArray[0])
        }
        
        
        for i in 0 ..< bleDataArray.count{
            for j in 1 ... 3{
                temperature.append(UInt64(bleDataArray[i].substring(with: 6..<8), radix:16)!)
                status.append(UInt64(j))
                year.append(UInt64(bleDayArray[i].substring(with: 6..<8), radix:16)!)
                month.append(UInt64(bleDayArray[i].substring(with: 8..<10), radix:16)!)
                day.append(UInt64(bleDayArray[i].substring(with: 10..<12), radix:16)!)
                hour.append(UInt64(bleTimeArray[i].substring(with: 6..<8), radix:16)!)
                minute.append(UInt64(bleTimeArray[i].substring(with: 8..<10), radix:16)!)
                switch j {
                case 1 :
                    value.append(UInt64(bleDataArray[i].substring(with: 10..<12), radix:16)!)
                case 2 :
                    value.append(UInt64(bleDataArray[i].substring(with: 8..<10), radix:16)!)
                case 3 :
                    value.append(UInt64(bleDataArray[i].substring(with: 12..<14), radix:16)!)
                default:
                    break
                }
            }
        }

        let DictionaryResult: Dictionary<String, [UInt64]> = [
            "temperature" : temperature,
            "status" : status,
            "value" : value,
            "year" : year,
            "month" : month,
            "day" : day,
            "hour" : hour,
            "minute" : minute
        ]
        return DictionaryResult
    }
    
    // MARK: - -額溫槍Meter 回傳資料 Formater
    
    func temperatureDataFormat(NotifyResult: String,bleStatus: Int)->Dictionary<String, [UInt64]>{
        var bleData:String = ""
        
        var temperature:[UInt64] = []
        var status:[UInt64] = []
        var value:[UInt64] = []
        var year:[UInt64] = []
        var month:[UInt64] = []
        var day:[UInt64] = []
        var hour:[UInt64] = []
        var minute:[UInt64] = []
        
        let NotifyResultArray : [String] = NotifyResult.components(separatedBy: "-")
        var NotifyResultSum:String  = ""
        for i in 0 ..< NotifyResultArray.count{
            NotifyResultSum = NotifyResultSum + NotifyResultArray[i]
        }
        
        for i in 0 ..< NotifyResultSum.count-12{
            if checkDataTemperature(Data: NotifyResultSum.substring(with: i..<i+10),Checker: "fe6a725a55"){
                if(i+15<NotifyResultSum.count){
                    print(NotifyResultSum.substring(with: i..<i+16))
                    bleData = NotifyResultSum.substring(with: i..<i+16)
                }
            }
        }
        
        let date = Date()
        let calendar = Calendar.current

        temperature.append(UInt64(bleData.substring(with: 6..<8), radix:16)!)
        status.append(UInt64(bleStatus))
        year.append(UInt64(addZero(input: (calendar.component(.year, from: date))%100))!)
        month.append(UInt64(addZero(input: calendar.component(.month, from: date)))!)
        day.append(UInt64(addZero(input: calendar.component(.day, from: date)))!)
        hour.append(UInt64(addZero(input: calendar.component(.hour, from: date)))!)
        minute.append(UInt64(addZero(input: calendar.component(.minute, from: date)))!)
        //value.append((UInt64(bleData.substring(with: 8..<12), radix:16)!)/10)
        value.append(setTemperatureUnit(value: UInt64(bleData.substring(with: 8..<12), radix:16)!, Unit: UserGetSetData.TemperatureUnit))
        
        let DictionaryResult: Dictionary<String, [UInt64]> = [
            "temperature" : temperature,
            "status" : status,
            "value" : value,
            "year" : year,
            "month" : month,
            "day" : day,
            "hour" : hour,
            "minute" : minute
        ]
        
        return DictionaryResult
    }
    
    //MARK: - 依據溫度單位轉換數據
    
    func setTemperatureUnit(value: UInt64 ,Unit: String)->UInt64{
        
        //除100 還原數值
        var calculate = Double(value)/100
        
        if(Unit != "0" ){
            calculate = (calculate * 9 / 5) + 32
        }
        
        return UInt64(calculate * 10)
        
    }
    
    
    
    func checkData(Data: String, Checker: String)->Bool{
        var result = true
        for i in 0 ..< Checker.count{
            if(Data.substring(with: i..<i+1) != Checker.substring(with: i..<i+1)){
                result = false
            }
        }
        return result
    }
    
    //mark: - 檢查Checker 末兩位數不相等 其他相等
    
    func checkDataTemperature(Data: String, Checker: String)->Bool{
        var result = true
        for i in 0 ..< Checker.count-1{
            if(i>Checker.count-3){
                if(Data.substring(with: i..<i+1) == Checker.substring(with: i..<i+1) && Data.substring(with: i+1..<i+2) == Checker.substring(with: i+1..<i+2)){
                    result = false
                }
            }
            else{
                if(Data.substring(with: i..<i+1) != Checker.substring(with: i..<i+1)){
                    result = false
                }
            }
        }
        return result
    }
    
     //mark: - 檢查傳入資料是否符合目前種類
    
    func checkPetData(petSpecies: String,status: UInt64)->Bool{
        var result = false
        
        if(petSpecies == "Canine"){
            if(status == 2){
                result = true
            }
        }
        else if(petSpecies == "Feline"){
            if(status == 3){
                result = true
            }
        }
        
        return result
    }
    
    // MARK: - 個位數值補零
    
    func addZero(input: Int)->String{
        var result = String(input)
        if (input<10){
            result = "0\(result)"
        }
        return result
    }
    
    
}
