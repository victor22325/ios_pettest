//
//  colorFunction.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    
    var borderColorFromUIColor: UIColor {
        get {
            return UIColor(cgColor: self.borderColor!)
        } set {
            self.borderColor = newValue.cgColor
        }
    }
}
