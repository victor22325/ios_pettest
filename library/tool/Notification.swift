//
//  Notification.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/16.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import UserNotifications

class Notification {
    
    static let sharedInstance = Notification()
    
    //MARK: - 移除Notification 防止內存泄露 防止重複註冊調用
    
    func removeAllNotification(){
        
        //record
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeGlucoseViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removePressureViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeTemperatureViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeScanBleViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeBleAutoViewController"), object: nil)
        
        //notes
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeNoteTableViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeNoteListTableViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeCalendarViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeNoteViewController"), object: nil)
        
        //reminder
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeReminderViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeMedicineTableViewController"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeMedicineReminderViewController"), object: nil)
        
        //news
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeNewsDetiaiViewController"), object: nil)
        
        //base
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeSelectPetViewController"), object: nil)
        
        
    }
    
    func addNotification(type: String,identifier: String,contentString: String,alertType: String,timeInterval: TimeInterval){
        print(identifier)
        
        var image = ""
        switch type{
        case "clinic":
            image = "doctor"
        case "medicine":
            image = "medicine"
        case "water":
            image = "water"
        default :
            break
        }
        
        var contenttemp = contentString
        if(contentString == ""){
            contenttemp = " "
        }
        
        let content = UNMutableNotificationContent()
        content.title = "Advocate PetTest \(type) reminder"
        content.subtitle = " "
        content.body = "\(contenttemp)"
        if(alertType == "notification + sound"){
            content.sound = UNNotificationSound.default
        }
        content.categoryIdentifier = "\(type)"
        
        let imageURL = Bundle.main.url(forResource: "\(image)", withExtension: "png")
        let attachment = try! UNNotificationAttachment(identifier: "", url: imageURL!, options: nil)
        content.attachments = [attachment]
        content.userInfo = ["type":"\(alertType)"]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let request = UNNotificationRequest(identifier: "\(identifier)", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func addNotificationRepeat(type: String,identifier: String,contentString: String,alertType: String,date: Date){
        
        print(identifier)
        
        var image = ""
        switch type{
        case "clinic":
            image = "doctor"
        case "medicine":
            image = "medicine"
        case "water":
            image = "water"
        default :
            break
        }
        
        var contenttemp = contentString
        if(contentString == ""){
            contenttemp = " "
        }
        
        let content = UNMutableNotificationContent()
        content.title = "Advocate \(type) reminder"
        content.subtitle = " "
        content.body = "\(contenttemp)"
        if(alertType == "notification + sound"){
            content.sound = UNNotificationSound.default
        }
        content.categoryIdentifier = "\(type)"
        
        let imageURL = Bundle.main.url(forResource: "\(image)", withExtension: "png")
        let attachment = try! UNNotificationAttachment(identifier: "", url: imageURL!, options: nil)
        content.attachments = [attachment]
        content.userInfo = ["type":"\(alertType)"]
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([ .hour, .minute],from: date)
        
        
        let trigger = UNCalendarNotificationTrigger(dateMatching:components, repeats: true)
        let request = UNNotificationRequest(identifier: "\(identifier)", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)


    }
    
    func removeNotification(identifier: String){
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
            var identifiers: [String] = []
            for notification:UNNotificationRequest in notificationRequests {
                if notification.identifier == "\(identifier)" {
                    identifiers.append(notification.identifier)
                }
            }
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
        }
    }
}
