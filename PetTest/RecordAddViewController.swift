//
//  RecordAddViewController.swift
//  PetTest
//
//  Created by Victor on 2019/7/15.
//  Copyright © 2019 BMBMAC. All rights reserved.
//

import UIKit
import CoreData

class RecordAddViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var bloodValue: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    let app = UIApplication.shared.delegate as! AppDelegate
    var viewContent: NSManagedObjectContext!
    var fetchResultController: NSFetchedResultsController<GlucoseModel>!
    var Allglucose:[GlucoseModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContent = app.persistentContainer.viewContext
//        queryAllGlucose()
        
        petName.text = UserGetSetData.PetName
        category.text = UserGetSetData.PetSpecies
        
        bloodValue.delegate = self
    }
    
    private func queryAllGlucose() {
        do {
            let allGlucose = try viewContent.fetch(GlucoseModel.fetchRequest())
            for glucose in allGlucose as! [GlucoseModel] {
                print("AddRecord bloodStatus:" + String(glucose.bloodStatus) + " ,bloodValue:" + String(glucose.bloodValue) + ", date:" + String(glucose.createDate!))
            }
        } catch {
            print(error)
        }
    }
    
    @IBAction func insertAction(_ sender: Any) {
        if bloodValue.text!.isEmpty {
            let controller = UIAlertController(title: "Error", message: "BloodValue cannot empty!", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            controller.addAction(okAction)
            self.present(controller, animated: true, completion: nil)
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            print("datePick:" + formatter.string(from: datePicker.date))
            
            print("glucoseMin:" + UserGetSetData.PetGlucoseLower)
            print("glucoseMax:" + UserGetSetData.PetGlucoseUpper)
            
            let glucoseData = NSEntityDescription.insertNewObject(forEntityName: "Glucose", into: viewContent) as! GlucoseModel
            glucoseData.bloodValue = Int64(bloodValue.text!)!
            glucoseData.userId = UserGetSetData.Id
            glucoseData.bloodStatus = getPetSpecies(petSpecies: UserGetSetData.PetSpecies)
            glucoseData.petName = UserGetSetData.PetName
            if let glucose_max = Int64(UserGetSetData.PostMealUpper) {
                glucoseData.max = glucose_max
            }
            if let glucose_min = Int64(UserGetSetData.PostMealLower) {
                glucoseData.min = glucose_min
            }
            if glucoseData.max == 0 && glucoseData.min == 0 {       // isEmpty
                glucoseData.max = 111
                glucoseData.min = 50
            }
            glucoseData.createDate = formatter.string(from: datePicker.date)
            glucoseData.changed = 0
            glucoseData.sended = 0
            app.saveContext()
            dismiss(animated: false, completion: nil)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
    }
    
    private func getPetSpecies(petSpecies:String) ->Int64 {
        switch petSpecies {
        case "Canine":
            return Int64(PetSpecies.canine)
        case "Feline":
            return Int64(PetSpecies.feline)
        default:
            return Int64(PetSpecies.other)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
