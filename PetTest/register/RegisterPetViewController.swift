//
//  RegisterPetViewController.swift
//  HenrySchein
//
//  Created by BMBMAC on 2017/8/3.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import DatePickerDialog
import EZLoadingActivity
import AudioToolbox

class RegisterPetViewController: UIViewController , SSRadioButtonControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate , UITextFieldDelegate{

    let species = ["Canine","Feline"]
    
    let gender = ["male","Female"]
    
    @IBOutlet weak var petNameTextField: UITextField!
    @IBOutlet weak var petSpeciesTextField: UITextField!
    @IBOutlet weak var petBreedTextField: UITextField!
    @IBOutlet weak var petWeightTextField: UITextField!
    @IBOutlet weak var petGenderTextField: UITextField!
    @IBOutlet weak var petGlucoseLowerTextField: UITextField!
    @IBOutlet weak var petGlucoseUppperTextField: UITextField!
    
    @IBOutlet weak var petDibetesYearLabel: UILabel!
    @IBOutlet weak var petDibetesYearTextField: UITextField!
    
    @IBOutlet weak var petBirthButton: UIButton!
    
    @IBOutlet weak var lbButton: UIButton!
    @IBOutlet weak var kgButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var diabetesNAbutton: UIButton!
    @IBOutlet weak var diabetesTypeOnebutton: UIButton!
    @IBOutlet weak var diabetesTypeTwobutton: UIButton!
    
    @IBOutlet var avoidingView: UIView!
    
    var petBirth = ""
    var soapConnect: Bool = false
    var soapResult: String = ""
    var message: String = ""

    var radioButtonController: SSRadioButtonsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.hideKeyboardWhenTappedAround()
        
        setMode(Login_status: UserGetSetData.Login_Status)
        
        //init
        setKeyboard()
        
        KeyboardAvoiding.avoidingView = self.avoidingView
        
        radioButtonController = SSRadioButtonsController(buttons: diabetesNAbutton, diabetesTypeOnebutton, diabetesTypeTwobutton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        let speciesPickerView = UIPickerView()
        speciesPickerView.delegate = self
        speciesPickerView.tag = 1
        petSpeciesTextField.inputView = speciesPickerView
        
        
        let genderPickerView = UIPickerView()
        genderPickerView.delegate = self
        genderPickerView.tag = 2
        petGenderTextField.inputView = genderPickerView

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectButton(aButton: UIButton?) {
        
    }
    
    func setKeyboard(){
        petWeightTextField.keyboardType = UIKeyboardType.decimalPad
        petGlucoseLowerTextField.keyboardType = UIKeyboardType.decimalPad
        petGlucoseUppperTextField.keyboardType = UIKeyboardType.decimalPad
        petDibetesYearTextField.keyboardType = UIKeyboardType.decimalPad
    }
    
    
    
    @IBAction func actionButton(sender: UIButton) {
        // Yes button clicked
        if sender == diabetesNAbutton {
            setClick(sender:diabetesNAbutton)
            UserGetSetData.PetDiabetesType = "na"
        }
        else if sender == diabetesTypeOnebutton {
            setClick(sender:diabetesTypeOnebutton)
            UserGetSetData.PetDiabetesType = "type1"
        }
        else if sender == diabetesTypeTwobutton {
            setClick(sender:diabetesTypeTwobutton)
            UserGetSetData.PetDiabetesType = "type2"
        }
        else if sender == lbButton {
            setClick(sender:lbButton)
            UserGetSetData.WeightUnit = "0";
            
        }
        else if sender == kgButton {
            setClick(sender:kgButton)
            UserGetSetData.WeightUnit = "1";
        }
        
        else if(sender == petBirthButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .date) { (date) in
                if let dateCheck = date {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    
                    self.petBirthButton.setTitle("\(dateFormatter.string(from: dateCheck))",for: .normal)
                    
                    self.petBirth = dateFormatter.string(from: dateCheck)
                }
            }
        }
        
        else  if sender == saveButton {
            let check = checkout()
            
            if check == "OK"{
                UserGetSetData.GlucoseUnit = "1"
                UserGetSetData.PetName = petNameTextField.text!
                UserGetSetData.PetSpecies = petSpeciesTextField.text!
                UserGetSetData.PetBreed = petBreedTextField.text!
                UserGetSetData.PetWeight = petWeightTextField.text!
                UserGetSetData.PetDiabetesYear = petDibetesYearTextField.text!
                UserGetSetData.PetGender = petGenderTextField.text!
                UserGetSetData.PetBirth = petBirth
                UserGetSetData.PetGlucoseUpper = petGlucoseUppperTextField.text!
                UserGetSetData.PetGlucoseLower = petGlucoseLowerTextField.text!
                UserGetSetData.PetAppUse = "1"
                UserGetSetData.AppUSE = "0"
                 
                EZLoadingActivity.show("Loading...", disableUI: true)
                if Reachability.isConnectedToNetwork() == true {
                    
                    self.cellSoapService()
                    
                    let queue = DispatchQueue(label:"signup",qos: DispatchQoS.userInitiated)
                    
                    queue.async {
                        var i = 0
                        let timeOut = 25
                        repeat{
                            Thread.sleep(forTimeInterval: 1)
                            i = i+1
                        }while(!self.soapConnect && i < timeOut)
                        
                        self.checkSignUp()
                    }
                }
                else{
                    let delayInSeconds = 1.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                        EZLoadingActivity.hide(false, animated: false)
                    }
                }
                
            }
            else {
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                alert(alertText: check)
            }
            
            
        }


    }
    
    func setClick(sender: UIButton){
        if sender == diabetesNAbutton {
            petDibetesYearTextField.isHidden = true
            petDibetesYearLabel.isHidden = true
        }
        else if sender == diabetesTypeOnebutton {
            petDibetesYearTextField.isHidden = false
            petDibetesYearLabel.isHidden = false
        }
        else if sender == diabetesTypeTwobutton {
            petDibetesYearTextField.isHidden = false
            petDibetesYearLabel.isHidden = false
        }
        else if sender == lbButton {
            lbButton.setImage(UIImage(named: "lb_selected.png"), for: UIControl.State.normal)
            kgButton.setImage(UIImage(named: "kilogram_unselected.png"), for: UIControl.State.normal)
        }
        else if sender == kgButton {
            lbButton.setImage(UIImage(named: "lb_unselected.png"), for: UIControl.State.normal)
            kgButton.setImage(UIImage(named: "kilogram_selected.png"), for: UIControl.State.normal)
        }
        
    }
    
    func setButton(sender: UIButton){
        if sender == diabetesNAbutton {
            diabetesNAbutton.isSelected = true
            diabetesTypeOnebutton.isSelected = false
            diabetesTypeTwobutton.isSelected = false
        }
        else if sender == diabetesTypeOnebutton {
            diabetesNAbutton.isSelected = false
            diabetesTypeOnebutton.isSelected = true
            diabetesTypeTwobutton.isSelected = false
            
        }
        else if sender == diabetesTypeTwobutton {
            diabetesNAbutton.isSelected = false
            diabetesTypeOnebutton.isSelected = false
            diabetesTypeTwobutton.isSelected = true
            
        }
        
    }
    
    func checkout() -> String{
        var result = "OK";
        
        if(petNameTextField.text?.isEmpty)!   {
            result = "Name can't not be empty"
        }
        else if(petSpeciesTextField.text?.isEmpty)! {
            result = "Pecies can't not be empty"
        }
        else if(petBreedTextField.text?.isEmpty)! {
            result = "Breed can't not be empty"
        }
        else if(petWeightTextField.text?.isEmpty)! {
            result = "Weight can't not be empty"
        }
        else if(petGenderTextField.text?.isEmpty)! {
            result = "Gender can't not be empty"
        }
        else if(petBirth == "") {
            result = "Date of birth can't not be empty"
        }
        else if(petGlucoseUppperTextField.text?.isEmpty)! {
            result = "Glucose upper can't not be empty"
        }
        else if(petGlucoseLowerTextField.text?.isEmpty)! {
            result = "Glucose lower can't not be empty"
        }
        else if((UserGetSetData.PetDiabetesType == "type1" || UserGetSetData.PetDiabetesType == "type2") && (petDibetesYearTextField.text?.isEmpty)!){
            result = "Diagnosed year can't not be empty"
        }
        else{
            let glucoseUpper: Int =  Int(petGlucoseUppperTextField.text!)!
            let glucoselower: Int =  Int(petGlucoseLowerTextField.text!)!

            if glucoseUpper <= glucoselower {
                result = "Glucose lower >= upper"
            }
        }
        
        
        return result
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return species.count
        }
        
        if pickerView.tag == 2 {
            return gender.count
        }
        
        
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            petSpeciesTextField.text = species[0]
            return species[row]
        }
        
        if pickerView.tag == 2 {
            petGenderTextField.text = gender[0]
            return gender[row]
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            petSpeciesTextField.text = species[row]
        }
        
        if pickerView.tag == 2 {
            petGenderTextField.text = gender[row]
        }
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func alert (alertText : String){
        let alertController = UIAlertController(title: "Warning", message: alertText, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func cellSoapService() {
        
        var soapFunc = ""
        
        var dataSetup = [
            "appId":"1",
            "appUse":"\(UserGetSetData.AppUSE)",
            "petAppUse":"\(UserGetSetData.PetAppUse)",
            "petGlucoseUpper":"\(UserGetSetData.PetGlucoseUpper)",
            "petGlucoseLower":"\(UserGetSetData.PetGlucoseLower)",
            "petBirth":"\(UserGetSetData.PetBirth)",
            "petGender":"\(UserGetSetData.PetGender)",
            "petDiabetesYear":"\(UserGetSetData.PetDiabetesYear)",
            "petDiabetesType":"\(UserGetSetData.PetDiabetesType)",
            "petWeight":"\(UserGetSetData.PetWeight)",
            "petBreed":"\(UserGetSetData.PetBreed)",
            "petSpecies":"\(UserGetSetData.PetSpecies)",
            "petName":"\(UserGetSetData.PetName)",
            "glucoseUnit":"\(UserGetSetData.GlucoseUnit)",
            "weightUnit":"\(UserGetSetData.WeightUnit)",
            "city":"\(UserGetSetData.City)",
            "region":"\(UserGetSetData.Region)",
            "country":"\(UserGetSetData.Country)",
            "email":"\(UserGetSetData.Email)",
            "password":"\(UserGetSetData.Password)",
            "name":"\(UserGetSetData.LastName)",
            "firstname":"\(UserGetSetData.FristName)"
        ]
        
        if(UserGetSetData.Login_Status){
            soapFunc = "editPet"
            dataSetup.updateValue("\(UserGetSetData.Id)", forKey: "id")
            
        }
        else{
            soapFunc = "registerPetSelect"
        }
        
        let data = dataSetup as NSDictionary
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall",dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            let json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(json)
            self.soapConnect = status
            self.soapResult = json["status"].string!
            self.message = json["message"].string!
        }
    }
    
    func checkSignUp(){
        if(self.soapConnect){
            print("YES")
            if(self.soapResult == "true"){
                EZLoadingActivity.Settings.SuccessText = "\(self.message)"
                EZLoadingActivity.hide(true, animated: false)
                let delayInSeconds = 1.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    if(UserGetSetData.Login_Status){
                        self.dismiss(animated: true, completion: nil)
                    }
                    else{
                        self.performSegue(withIdentifier: "createSuccess", sender: self)
                    }
                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "\(self.message)"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    
    
    //修改profile時（登入後）
    func setMode(Login_status : Bool){
        if(Login_status){
            saveButton.setTitle("Save", for: .normal)
            petNameTextField.isEnabled = false
            petNameTextField.textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
            
            petNameTextField.text = UserGetSetData.PetName
            petSpeciesTextField.text = UserGetSetData.PetSpecies
            petBreedTextField.text = UserGetSetData.PetBreed
            petWeightTextField.text = UserGetSetData.Weight
            petDibetesYearTextField.text = UserGetSetData.PetDiabetesYear
            petGenderTextField.text = UserGetSetData.PetGender
            petBirthButton.setTitle("\(UserGetSetData.PetBirth)", for: .normal)
            petGlucoseLowerTextField.text = UserGetSetData.PreMealLower
            petGlucoseUppperTextField.text = UserGetSetData.PreMealUpper
            
            self.petBirth = UserGetSetData.PetBirth
            
            if(UserGetSetData.WeightUnit == "0"){
                setClick(sender:lbButton)
            }
            else{
                setClick(sender:kgButton)
            }
            
            if(UserGetSetData.PetDiabetesType == "na"){
                setClick(sender:diabetesNAbutton)
                 setButton(sender: diabetesNAbutton)
            }
            else if(UserGetSetData.PetDiabetesType == "type1"){
                setClick(sender:diabetesTypeOnebutton)
                 setButton(sender: diabetesTypeOnebutton)
            }
            else if(UserGetSetData.PetDiabetesType == "type2"){
                setClick(sender:diabetesTypeTwobutton)
                 setButton(sender: diabetesTypeTwobutton)
            }
        }
        else{
            UserGetSetData.WeightUnit = "0";
            UserGetSetData.PetDiabetesType = "na";
            petDibetesYearTextField.isHidden = true
            petDibetesYearLabel.isHidden = true
        }
    }
    
    //設定鍵盤擋住輸入視窗事件
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == petNameTextField || textField == petSpeciesTextField || textField == petBreedTextField || textField == petWeightTextField){
            KeyboardAvoiding.avoidingView = textField
        }
        else{
            KeyboardAvoiding.avoidingView = self.avoidingView
        }
        
        return true
    }

    

}
