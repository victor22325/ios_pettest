//
//  RegisterAccountViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/4/27.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import AudioToolbox
import IHKeyboardAvoiding

class RegisterAccountViewController: UIViewController, UITextFieldDelegate , UIPickerViewDataSource, UIPickerViewDelegate{
    
    let country = ["America"]
    
    let state = ["America":["NewYork", "Delaware"]]
    
    let city = ["NewYork": ["Manhattan", "Queens", "Brooklyn", "The Bronx", "Staten Island"], "Delaware": ["New Castle", "Kent", "Sussex"]]
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    
    @IBOutlet var avoidingView: UIView!
    
    @IBOutlet weak var nextbutton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        KeyboardAvoiding.avoidingView = self.avoidingView
        
        setMode(Login_status: UserGetSetData.Login_Status)
        
        let countryPickerView = UIPickerView()
        countryPickerView.delegate = self
        countryPickerView.tag = 1
        countryTextField.inputView = countryPickerView
        
        
        let statePickerView = UIPickerView()
        statePickerView.delegate = self
        statePickerView.tag = 2
        stateTextField.inputView = statePickerView
        
        
        let cityPickerView = UIPickerView()
        cityPickerView.delegate = self
        cityPickerView.tag = 3
        cityTextField.inputView = cityPickerView
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func actionButton(sender: UIButton) {
        // Yes button clicked
        if sender == nextbutton {
            let check = checkout()
            
            if check == "OK" {
                UserGetSetData.FristName = firstNameTextField.text!
                UserGetSetData.LastName = lastNameTextField.text!
                UserGetSetData.Email = emailTextField.text!
                UserGetSetData.Password = passwordTextField.text!
                UserGetSetData.Country = countryTextField.text!
                UserGetSetData.Region = stateTextField.text!
                UserGetSetData.City = cityTextField.text!
                UserGetSetData.Zipcode = zipCodeTextField.text!
                
                performSegue(withIdentifier: "showPetInformation", sender: self)
            }
            else {
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                alert(alertText: check)
            }
            
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return country.count
        }
        
        if pickerView.tag == 2 && countryTextField.text != ""{
            return state[countryTextField.text!]!.count
        }
        
        if pickerView.tag == 3 && stateTextField.text != "" {
            return city[stateTextField.text!]!.count
        }
        
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            countryTextField.text = country[0]
            return country[row]
        }
        
        if pickerView.tag == 2 && countryTextField.text != ""{
            stateTextField.text = state[countryTextField.text!]?[0]
            return state[countryTextField.text!]?[row]
        }
        
        if pickerView.tag == 3 && stateTextField.text != ""{
            cityTextField.text = city[stateTextField.text!]?[0]
            return city[stateTextField.text!]?[row]
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            countryTextField.text = country[row]
        }
        
        if pickerView.tag == 2 && countryTextField.text != ""{
            stateTextField.text = state[countryTextField.text!]?[row]
        }
        
        if pickerView.tag == 3 && stateTextField.text != "" {
            cityTextField.text = city[stateTextField.text!]?[row]
        }
    }

    
    
    //彈出警示視窗
    func alert (alertText : String){
        let alertController = UIAlertController(title: "Warning", message: alertText, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    
    }
    
    //驗證輸入
    func checkout() -> String{
        var result = "OK";
        
        if(firstNameTextField.text?.isEmpty)! {
            result = "First name can't not be empty"
        }
        else if(lastNameTextField.text?.isEmpty)! {
            result = "Last ame can't not be empty"
        }
        else if(emailTextField.text?.isEmpty)! {
          result = "Email  can't not be empty"
        }
        else if !(isValidEmail(testStr: emailTextField.text!)){
            result = "Enter a valid email address"
        }
        else if(passwordTextField.text?.isEmpty)! {
          result = "Password can't not be empty"
        }
            
        else if (passwordTextField.text?.count)! < 6 {
            result = "Password less than six characters"
        }
        else if(confirmPasswordTextField.text?.isEmpty)! {
          result = "Confirm password can't not be empty"
        }
        else if passwordTextField.text! != confirmPasswordTextField.text! {
          result = "Password don't match"
        }
       
        return result
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //修改profile時（登入後）
    func setMode(Login_status : Bool){
        if(Login_status){
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back", style: .plain, target: self, action: #selector(backAction))
            
            emailTextField.isEnabled = false
            emailTextField.textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
            
            firstNameTextField.text = UserGetSetData.FristName
            lastNameTextField.text = UserGetSetData.LastName
            emailTextField.text = UserGetSetData.Email
            passwordTextField.text = UserGetSetData.Password
            confirmPasswordTextField.text = UserGetSetData.Password
            countryTextField.text = UserGetSetData.Country
            stateTextField.text = UserGetSetData.Region
            cityTextField.text = UserGetSetData.City
        }
    }
    @objc func backAction(){
        dismiss(animated: true, completion: nil)
    }
    
    //設定鍵盤擋住輸入視窗事件
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == firstNameTextField || textField == lastNameTextField ){
            KeyboardAvoiding.avoidingView = textField
        }
        else{
            KeyboardAvoiding.avoidingView = self.avoidingView
        }
        
        return true
    }



}
