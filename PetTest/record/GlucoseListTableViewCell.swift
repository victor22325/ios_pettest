//
//  GlucoseListTableViewCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/2.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class GlucoseListTableViewCell: UITableViewCell {
    
    @IBOutlet  var dateLable: UILabel!
    @IBOutlet  var timeLable: UILabel!
    @IBOutlet  var modeImageView: UIImageView!
    @IBOutlet  var valueLable: UILabel!
    @IBOutlet  var minLable: UILabel!
    @IBOutlet  var maxLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
