//
//  RecordViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/5.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class RecordViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        set bar item background color
//        let numberOfItems = CGFloat((tabBarController?.tabBar.items!.count)!)
//        
//        let tabBarItemSize = CGSize(width: (tabBarController?.tabBar.frame.width)! / numberOfItems,
//                                    height: (tabBarController?.tabBar.frame.height)!)
//        
//        tabBarController?.tabBar.selectionIndicatorImage
//            = UIImage.imageWithColor(color: UIColor(red: 21/255, green: 73/255, blue: 140/255, alpha: 1.0),
//                                     size: tabBarItemSize).resizableImage(withCapInsets: .zero)
//        
//        tabBarController?.tabBar.frame.size.width = self.view.frame.width + 4
//        tabBarController?.tabBar.frame.origin.x = -2
        
        switch UserGetSetData.Device_Status {
        case 0:
            performSegue(withIdentifier: "showGlucose", sender: self)
        case 1:
            performSegue(withIdentifier: "showPressure", sender: self)
        case 2:
            performSegue(withIdentifier: "showTemperature", sender: self)
        default:
            break
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated) // No need for semicolon
        switch UserGetSetData.Device_Status {
        case 0:
            performSegue(withIdentifier: "showGlucose", sender: self)
        case 1:
            performSegue(withIdentifier: "showPressure", sender: self)
        case 2:
            performSegue(withIdentifier: "showTemperature", sender: self)
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
