//
//  BleDownloadTableViewCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/17.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class BleDownloadTableViewCell: UITableViewCell {
    
    @IBOutlet  var dateLable: UILabel!
    @IBOutlet  var timeLable: UILabel!
    @IBOutlet  var modeLable: UILabel!
    @IBOutlet  var modeImageView: UIImageView!
    @IBOutlet  var valueLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
