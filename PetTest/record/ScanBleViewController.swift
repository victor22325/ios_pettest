//
//  ScanBleViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/16.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
//import RxBluetoothKit
//import RxSwift
//import CoreBluetooth
import EZLoadingActivity

class ScanBleViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var scansTableView: UITableView!
    @IBOutlet weak var titleUINavigationItem: UINavigationItem!
    
    private var isScanInProgress = false
//    private var scheduler: ConcurrentDispatchQueueScheduler!
//    private let manager = BluetoothState(rawValue: .min)
//    private var scanningDisposable: Disposable?
//    fileprivate var peripheralsArray: [ScannedPeripheral] = []
    fileprivate let scannedPeripheralCellIdentifier = "peripheralCellId"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let timerQueue = DispatchQueue(label: "timer")
//        scheduler = ConcurrentDispatchQueueScheduler(queue: timerQueue)
        scansTableView.delegate = self
        scansTableView.dataSource = self
        scansTableView.estimatedRowHeight = 80.0
        scansTableView.rowHeight = UITableView.automaticDimension
        
        NotificationCenter.default.addObserver(self, selector: #selector(stopScanning), name: NSNotification.Name(rawValue: "stop"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeScanBleViewController"), object: nil)
        
        let delayInSeconds = 1.0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            self.startScanning()
        }

        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return peripheralsArray.count
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: scannedPeripheralCellIdentifier, for: indexPath)
//        let peripheral = peripheralsArray[indexPath.row]
//        if let peripheralCell = cell as? ScannedPeripheralCell {
//            peripheralCell.configure(with: peripheral)
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stop"), object: nil)
        print("ScanBleViewController tableView")
//        BleFunction.scannedPeripheral = peripheralsArray[indexPath.row]
//        BleFunction.manager = manager
//        popBleView()

    }
    
    //start ble
    func startScanning() {
        isScanInProgress = true
        
        print("ScanBleViewController startScanning")
//        titleUINavigationItem.title = "Scanning..."
//        scanningDisposable = manager.rx_state
//            .timeout(4.0, scheduler: scheduler)
//            .take(1)
//            .flatMap { _ in self.manager.scanForPeripherals(withServices: nil, options:nil) }
//            .subscribeOn(MainScheduler.instance)
//            .subscribe(onNext: {
//                self.addNewScannedPeripheral($0)
//            }, onError: { error in
//            })
    }
    
    //stop ble
    @objc func stopScanning() {
//        scanningDisposable?.dispose()
        isScanInProgress = false
        titleUINavigationItem.title = ""
    }
    
//    func addNewScannedPeripheral(_ peripheral: ScannedPeripheral) {
//        let mapped = peripheralsArray.map { $0.peripheral }
//        if let indx = mapped.firstIndex(of: peripheral.peripheral) {
//            peripheralsArray[indx] = peripheral
//        } else {
//            self.peripheralsArray.append(peripheral)
//        }
//        DispatchQueue.main.async {
//            self.scansTableView.reloadData()
//        }
//    }
    
    //點擊選擇指定ble meter 並轉到 bleViewController page
    private func popBleView(){
        
//        EZLoadingActivity.show("Loading...", disableUI: true)
//
//        let presenter: Presentr = {
//            let width = ModalSize.fluid(percentage: 0.60)
//            let height = ModalSize.fluid(percentage: 0.10)
//            let customType = PresentationType.custom(width: width, height: height, center: .center)
//
//            let presenter = Presentr(presentationType: customType)
//
//            presenter.transitionType = TransitionType.coverHorizontalFromRight
//            presenter.dismissOnSwipe = true
//            return presenter
//        }()
//        let storyboard = UIStoryboard(name: "Record",bundle:nil)
//        let popupViewController: BleAutoViewController = {
//            let popupViewController = storyboard.instantiateViewController(withIdentifier: "bleAutoViewController")
//            return popupViewController as! BleAutoViewController
//        }()
//
//        presenter.transitionType = nil
//        presenter.dismissTransitionType = nil
//        presenter.keyboardTranslationType = .compress
//        self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)

    }

    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
}

// MARK: - Presentr Delegate

extension ScanBleViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension ScanBleViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

//extension ScannedPeripheralCell {
//    func configure(with peripheral: ScannedPeripheral) {
//        //mac address display
//        //peripheralNameLabel.text = peripheral.advertisementData.localName ?? peripheral.peripheral.identifier.uuidString
//
//        //unknow device display
//        peripheralNameLabel.text = peripheral.advertisementData.localName ?? "Unknow device"
//    }
//}
