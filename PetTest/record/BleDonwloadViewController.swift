//
//  BleDonwloadViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/17.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import CoreData

class BleDonwloadViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,NSFetchedResultsControllerDelegate{
    
    var httpConnect: Bool = false
    var httpResult: String = ""
    var json: JSON = []
    
    let userDefults = UserDefaults.standard
    
    var glucose: GlucoseModel!
    var pressure: PressureModel!
    var temperatureModel: TemperatureModel!
    
    let dataFunction = DataFunction()
    
    var temperature:[UInt64] = []
    var status:[UInt64] = []
    var value:[UInt64] = []
    var year:[UInt64] = []
    var month:[UInt64] = []
    var day:[UInt64] = []
    var hour:[UInt64] = []
    var minute:[UInt64] = []
    
    var count: CountModel!
    var countList: [CountModel] = []
    var fetchResultController: NSFetchedResultsController<CountModel>!
    
    var note: NoteModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valueInit()
        viewInit()
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        
         navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back", style: .plain, target: self, action: #selector(backAction))
        
        
        let bleImage = UIImage(named: "done")?.withRenderingMode(.alwaysOriginal)
        let bleBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        bleBtn.setImage(bleImage, for: UIControl.State.normal)
        bleBtn.addTarget(self, action: #selector(setMode), for: UIControl.Event.touchUpInside)
        bleBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let bleUIbutton = UIBarButtonItem(customView: bleBtn)
        navigationItem.setRightBarButtonItems([bleUIbutton], animated: false)
        
        
    }
    
    func valueInit(){
//        temperature = BleFunction.downloadResult["temperature"]!
//        status = BleFunction.downloadResult["status"]!
//        value = BleFunction.downloadResult["value"]!
//        year = BleFunction.downloadResult["year"]!
//        month = BleFunction.downloadResult["month"]!
//        day = BleFunction.downloadResult["day"]!
//        hour = BleFunction.downloadResult["hour"]!
//        minute = BleFunction.downloadResult["minute"]!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (value.count + 1)
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! BleDownloadTableViewCell
        
        if(indexPath.row == 0){
            cell.dateLable.text = "Date"
            cell.timeLable.text = "Time"
            cell.modeLable.text = "Mode"
            cell.valueLable.text = dataFunction.setUnit(DeviceStatus: UserGetSetData.Device_Status)
            cell.valueLable.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
            cell.modeLable.isHidden = false
            cell.modeImageView.isHidden = true
        }
        else{
            let pointer = indexPath.row - 1
            
            cell.dateLable.text = setDay(year:  year[pointer], month: month[pointer], day: day[pointer])
            cell.timeLable.text = setTime(hour: hour[pointer], minute: minute[pointer])
            cell.modeLable.text = dataFunction.setDataStatus(DeviceStatus: UserGetSetData.Device_Status,Status: Int(status[pointer]))
            if(UserGetSetData.Device_Status != 2){
                cell.valueLable.text = "\(value[pointer])"
            }
            else{
                cell.valueLable.text = "\(value[pointer]/10).\(value[pointer]%10)"
            }
            cell.valueLable.textColor = dataFunction.setColor(value: Int(value[pointer]),DeviceStatus: UserGetSetData.Device_Status,Status: Int(status[pointer]))
            cell.modeLable.isHidden = true
            cell.modeImageView.isHidden = false
            cell.modeImageView.image = dataFunction.setDataStatusImage(DeviceStatus: UserGetSetData.Device_Status,Status: Int(status[pointer]))
        }
        
        return cell
    }
    
    @objc func backAction(){
        Notification.sharedInstance.removeAllNotification()
        let storyboard = UIStoryboard(name: "Main",bundle:nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TabMain")
        self.present(controller, animated: true, completion: nil)

    }
    
    func setDay(year: UInt64, month: UInt64,day: UInt64)->String{
        var result = ""
        result = "20\(dataFunction.addZero(input: year))-\(dataFunction.addZero(input: month))-\(dataFunction.addZero(input: day))"
        return result
    }
    
    func setTime(hour: UInt64, minute: UInt64)->String{
        var result = ""
        result = "\(dataFunction.addZero(input: hour)):\(dataFunction.addZero(input: minute))"
        return result
    }
    
    @objc func setMode(){
        let checker = userDefults.value(forKey: "autoSync") as! String?
        if checker! == "true"{
            http()
        }
        else{
            EZLoadingActivity.show("Loading...", disableUI: true)
            coreData(cloud: false)
        }

    }
    
    func coreData(cloud: Bool){
        
        loadDbData()
        
        
        for i in 0 ..< value.count{
        
            let sec = setSec(position: i)
            
            if(checkTimeError(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec)){
                continue
            }

            
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                if(UserGetSetData.Device_Status == 0){
                    glucose = GlucoseModel(context: appDelegate.persistentContainer.viewContext)
                    glucose.userId = UserGetSetData.Id
                    glucose.petName = UserGetSetData.PetName
                    glucose.bloodStatus = Int64(status[i])
                    glucose.bloodValue = Int64(value[i])
                    glucose.createDate = "\(self.setUploadDate(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec))"
                    glucose.max = Int64(dataFunction.setMax(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    glucose.min = Int64(dataFunction.setMin(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    glucose.changed = 0
                    
                    if(cloud){
                        glucose.sended = Int64(self.json["data"].string!)! - Int64(value.count) + Int64(i)
                    }
                    else{
                        glucose.sended = 0
                    }
                    
                    
                    glucose.id = countList[0].bloodCount + Int64(i)
                    
                }
                
                else if(UserGetSetData.Device_Status == 1){
                    pressure = PressureModel(context: appDelegate.persistentContainer.viewContext)
                    pressure.userId = UserGetSetData.Id
                    pressure.bloodStatus = Int64(status[i])
                    pressure.bloodValue = Int64(value[i])
                    pressure.createDate = "\(self.setUploadDate(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec))"
                    pressure.max = Int64(dataFunction.setMax(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    pressure.min = Int64(dataFunction.setMin(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    pressure.changed = 0
                    
                    if(cloud){
                        pressure.sended = Int64(self.json["data"].string!)! - Int64(value.count) + Int64(i)
                    }
                    else{
                        pressure.sended = 0
                    }
                    
                    pressure.id = countList[0].pressCount + Int64(i)
                    
                }
                
                else if(UserGetSetData.Device_Status == 2){
                    temperatureModel = TemperatureModel(context: appDelegate.persistentContainer.viewContext)
                    temperatureModel.userId = UserGetSetData.Id
                    temperatureModel.bloodStatus = Int64(status[i])
                    temperatureModel.bloodValue = Int64(value[i])
                    temperatureModel.createDate = "\(self.setUploadDate(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec))"
                    temperatureModel.max = Int64(dataFunction.setMax(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    temperatureModel.min = Int64(dataFunction.setMin(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))
                    temperatureModel.changed = 0
                    
                    if(cloud){
                        temperatureModel.sended = Int64(self.json["data"].string!)! - Int64(value.count) + Int64(i)
                    }
                    else{
                        temperatureModel.sended = 0
                    }
                    
                    temperatureModel.id = countList[0].temperatureCount + Int64(i)
                    
                }
                
                note = NoteModel(context: appDelegate.persistentContainer.viewContext)
                note.userId = UserGetSetData.Id
                note.petName = UserGetSetData.PetName
                
                note.noteAction = Int64(value[i])
                note.noteContent = UserGetSetData.PetName
                note.createDate = "\(self.setUploadDate(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec))"
                
                note.noteVoice = nil
                note.noteImage = nil
                if(UserGetSetData.Device_Status == 0){
                    note.noteType = "glucose"
                    note.bloodId = countList[0].bloodCount + Int64(i)
                    note.pressureId = -1
                    note.temperatureId = -1
                    
                }
                else if(UserGetSetData.Device_Status == 1){
                    note.noteType = "pressure"
                    note.pressureId = countList[0].pressCount + Int64(i)
                    note.bloodId = -1
                    note.temperatureId = -1
                }
                else if(UserGetSetData.Device_Status == 2){
                    note.noteType = "temperature"
                    note.temperatureId = countList[0].temperatureCount + Int64(i)
                    note.bloodId = -1
                    note.pressureId = -1
                }
                
                
                appDelegate.saveContext()
            }
            
        }
        
        if(UserGetSetData.Device_Status == 0){
            countList[0].bloodCount = countList[0].bloodCount + Int64(value.count)
        }
        else if(UserGetSetData.Device_Status == 1){
            countList[0].pressCount = countList[0].pressCount + Int64(value.count)
        }
        else if(UserGetSetData.Device_Status == 2){
            countList[0].temperatureCount = countList[0].temperatureCount + Int64(value.count)
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            if(UserGetSetData.Device_Status == 0){
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
            }
            else  if(UserGetSetData.Device_Status == 1){
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPressure"), object: nil)
            }
            else  if(UserGetSetData.Device_Status == 2){
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTemperature"), object: nil)
            }
            
            EZLoadingActivity.Settings.SuccessText = "Upload Success"
            EZLoadingActivity.hide(true, animated: false)
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabMain")
            self.present(controller, animated: true, completion: nil)

        }
    }
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    countList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }

    
    
    func http(){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellHttpService()
            
            let queue = DispatchQueue(label:"DataUpload",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 150
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.httpConnect && i < timeOut)
                
                self.checkDataUpload()
                
                
            }
            
        } else {
            coreData(cloud: false)
            userDefults.set("true", forKey: "autoSyncNoInternet")
            let delayInSeconds = 2.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "Upload Success"
                EZLoadingActivity.hide(false, animated: false)
            }
            
            
        }
    }

    func cellHttpService() {
        
        var httpFunc = ""
        switch UserGetSetData.Device_Status{
        case 0 :
            httpFunc = "AddBloodData"
        case 1 :
            httpFunc = "AddBloodPressureData"
        case 2 :
            httpFunc = "AddTemperatureData"
        default:
            break
        }
        let data:String = setHttpVars()
        
        HttpServices.sharedInstance.postConnect(httpFunc, httpString: data) { (status, result) in
            
            self.json = HttpServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.httpConnect = status
            self.httpResult = self.json["status"].string!
            
        }
    }

    func checkDataUpload(){
        if(self.httpConnect){
            print("YES")
            if(self.httpResult == "true"){
                coreData(cloud: true)
            }
            else{
                EZLoadingActivity.Settings.FailText = "Upload Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.httpConnect = false
    }
    
    func setHttpVars()->String{
        return "{'userid':'\(UserGetSetData.Id)','data':\(setDataHttp())}"
    }
    
    func setDataHttp()->String{
        var result = "["
        
        for i in 0 ..< value.count{
            
            let sec = setSec(position: i)
            
            result = result + "{'value':'\(value[i])','status':'\(status[i])','measure_date':'\(setUploadDate(year: year[i], month: month[i], day: day[i], hour: hour[i], minute: minute[i], sec: sec))','min':'\(dataFunction.setMin(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))','max':'\(dataFunction.setMax(DeviceStatus: UserGetSetData.Device_Status, Status: Int(status[i])))'}"
            if(i != value.count-1){
                result = result + ","
            }
        }
        
        result = result + "]"
        
        return result
    }
    
    func setUploadDate(year: UInt64, month: UInt64,day: UInt64,hour: UInt64,minute: UInt64,sec: Int)->String{
        var result = setDay(year:  year, month: month, day: day) + " "
        result = result + setTime(hour: hour, minute: minute)
        
        //sec add
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        var date = formatter.date(from: result)
        var dateComponent = DateComponents()
        dateComponent.second = sec
        print(date)
        date = Calendar.current.date(byAdding: dateComponent, to: date!)
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return formatter.string(from: date!)
    }
    
    //設定相同時間秒數
    func setSec(position: Int)->Int{
        var  result = 0
        for i in 0 ..< position{
            let nowTime = "\(year[position])\(month[position])\(day[position])\(hour[position])\(minute[position])"
            let checkTime = "\(year[i])\(month[i])\(day[i])\(hour[i])\(minute[i])"
            if(nowTime == checkTime){
                result += 5
            }
        }
        return result
    }
    
    //檢查時間是否為合法值
    func checkTimeError(year: UInt64, month: UInt64,day: UInt64,hour: UInt64,minute: UInt64,sec: Int) ->Bool {
        var result = setDay(year:  year, month: month, day: day) + " "
        result = result + setTime(hour: hour, minute: minute)
        
        //sec add
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date = formatter.date(from: result)
        
        if(date == nil){
            return true
        }
        else{
            return false
        }
        
    }

    

}
