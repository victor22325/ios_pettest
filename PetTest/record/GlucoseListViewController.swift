//
//  RecordListViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import CoreData
import DatePickerDialog
import EZLoadingActivity
import SwiftyJSON

class GlucoseListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var fromUIButton: UIButton!
    @IBOutlet weak var toUIButton: UIButton!
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var modeUILabel: UILabel!
    
    let dataFunction = DataFunction()
    var glucose:[GlucoseModel] = []
    var glucoseAll:[GlucoseModel] = []
    
    var note: NoteModel!
    var noteList:[NoteModel] = []
    
    var countList: [CountModel] = []
    
    var fetchResultController: NSFetchedResultsController<GlucoseModel>!
    var fetchNoteResultController: NSFetchedResultsController<NoteModel>!
    var fetchCountResultController: NSFetchedResultsController<CountModel>!
    
    var startTime:[String] = ["1950-01-01"," 00:00:01"]
    var endTime:[String] = ["2050-01-01"," 23:59:59"]
    
    var httpConnect: Bool = false
    var httpResult: String = ""
    var json: JSON = []
    
    var idArray:[Int64] = []
    var dateArray:[String] = []
    var statusArray:[Int64] = []
    var valueArray:[Int64] = []
    var maxArray:[Int64] = []
    var minArray:[Int64] = []
    var glucoseDownload: GlucoseModel!
    
    let app = UIApplication.shared.delegate as! AppDelegate
    var viewContent: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInit()
        loadData()
        dataSeletUser()
        dataSeletPet()
        dataRange()
        viewContent = app.persistentContainer.viewContext
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let reloadImage = UIImage(named: "button_reload")?.withRenderingMode(.alwaysOriginal)
        let reloadBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        reloadBtn.setImage(reloadImage, for: UIControl.State.normal)
        reloadBtn.addTarget(self, action: #selector(syncAction), for: UIControl.Event.touchUpInside)
        reloadBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let reloadUIbutton = UIBarButtonItem(customView: reloadBtn)
        
        let deleteImage = UIImage(named: "button_delete")?.withRenderingMode(.alwaysOriginal)
        let deleteBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        deleteBtn.setImage(deleteImage, for: UIControl.State.normal)
        deleteBtn.addTarget(self, action: #selector(deleteData), for: UIControl.Event.touchUpInside)
        deleteBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let deleteUIbutton = UIBarButtonItem(customView: deleteBtn)
//        navigationItem.setRightBarButtonItems([deleteUIbutton,reloadUIbutton], animated: false)
        
        modeUILabel.text = dataFunction.setUnit(DeviceStatus: UserGetSetData.Device_Status)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func datePickerTapped(sender: UIButton) {
        if(sender == fromUIButton || sender == toUIButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .date) { (date) in
                if let dateCheck = date {
                    var input = ""
                    if(sender == self.fromUIButton){
                        input = "start"
                    }
                    else if(sender == self.toUIButton){
                        input = "end"
                    }
                    self.setTimeRenge(data: dateCheck, input: input)
                    
                }
            }
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return glucose.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! GlucoseListTableViewCell
        
        let pointer = indexPath.row
        let glucoseData = glucose[pointer]
        
        cell.dateLable.text = "\(glucoseData.createDate!.substring(with: 0..<10))"
        cell.timeLable.text = "\(glucoseData.createDate!.substring(with: 11..<16))"
        cell.modeImageView.image = dataFunction.setDataStatusImage(DeviceStatus: UserGetSetData.Device_Status,Status: Int(glucoseData.bloodStatus))
        cell.valueLable.text = "\(glucoseData.bloodValue)"
        cell.valueLable.textColor = dataFunction.setColor(value: Int(glucoseData.bloodValue),DeviceStatus: UserGetSetData.Device_Status,Status: Int(glucoseData.bloodStatus),max: Int(glucoseData.max),min: Int(glucoseData.min))
        cell.minLable.text = "\(glucoseData.min)"
        cell.maxLable.text = "\(glucoseData.max)"
        
        cell.backgroundColor = dataFunction.setCellColor(value: Int(glucoseData.bloodValue),DeviceStatus: UserGetSetData.Device_Status,Status: Int(glucoseData.bloodStatus),max: Int(glucoseData.max),min: Int(glucoseData.min))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print(indexPath)
        var result:String = "Your glucose is normal"
        
        if (glucose[indexPath.row].max < glucose[indexPath.row].bloodValue){
            result = "Your glucose is too high"
        }
        else if(glucose[indexPath.row].min > glucose[indexPath.row].bloodValue){
            result = "Your glucose is too low"
        }
        let alert = UIAlertController(title: "Notice", message: result, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //Edit Button
        let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Set range", handler: { (action, indexPath) -> Void in
            let alertController = UIAlertController(title: "Set glucsoe range",message: "",preferredStyle: .alert)
            
            alertController.addTextField {
                (textField: UITextField!) -> Void in
                
                textField.placeholder = "glucose max"
                textField.keyboardType = UIKeyboardType.decimalPad
            }
            alertController.addTextField {
                (textField: UITextField!) -> Void in
                
                textField.placeholder = "glucose min"
                textField.keyboardType = UIKeyboardType.decimalPad
            }

            let cancelAction = UIAlertAction(title: "CANCEL",style: .cancel,handler: nil)
            
            let okAction = UIAlertAction(title: "SAVE",style: UIAlertAction.Style.default) {
                (action: UIAlertAction!) -> Void in
                let maxUITextField = (alertController.textFields?.first)!as UITextField
                let minUITextField = (alertController.textFields?.last)!as UITextField

                let check = self.checkRange(max: maxUITextField.text!,min: minUITextField.text!)
                if(check == "Range changed successfully"){
                    EZLoadingActivity.show("Loading...", disableUI: true)
                    
                    let glucoseData = self.glucose[indexPath.row]
                    glucoseData.max = Int64(maxUITextField.text!)!
                    glucoseData.min = Int64(minUITextField.text!)!
                    glucoseData.changed = 1
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                        EZLoadingActivity.Settings.SuccessText = check
                        EZLoadingActivity.hide(true, animated: false)
                        
                        self.loadData()
                        self.dataSeletUser()
                        self.dataSeletPet()
                        self.dataRange()
                        self.dataTableView.reloadData()
                    }
                }
                else{
                    EZLoadingActivity.show("Loading...", disableUI: true)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                        EZLoadingActivity.Settings.FailText = check
                        EZLoadingActivity.hide(false, animated: false)
                    }
                }
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController,animated: true,completion: nil)
            
        })
        
        //delete button
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete",handler: { (action, indexPath) -> Void in
            
            let glucose = self.glucose[indexPath.row]
            self.viewContent.delete(glucose)
            self.app.saveContext()
            self.loadData()
            self.dataSeletUser()
            self.dataSeletPet()
            self.dataTableView.reloadData()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
        })
        
        
        shareAction.backgroundColor = UIColor(red: 68.0/255.0, green: 108.0/255.0, blue: 179.0/255.0, alpha: 1.0)
        deleteAction.backgroundColor = UIColor(red: 210.0/255.0, green: 77.0/255.0, blue: 87.0/255.0, alpha: 1.0)
        
        return [deleteAction, shareAction]
        
    }

    
    func loadData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<GlucoseModel> = GlucoseModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    glucose = fetchedObjects
                    glucoseAll = glucose
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    func loadNoteData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<NoteModel> = NoteModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchNoteResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchNoteResultController.delegate = self
            
            do{
                try fetchNoteResultController.performFetch()
                if let fetchedObjects = fetchNoteResultController.fetchedObjects{
                    noteList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    func loadCountData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        DispatchQueue.main.async {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                self.fetchCountResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
                self.fetchCountResultController.delegate = self
                
                do{
                    try self.fetchCountResultController.performFetch()
                    if let fetchedObjects = self.fetchCountResultController.fetchedObjects{
                        self.countList = fetchedObjects
                    }
                }
                catch{
                    print(error)
                }
            }
        }
    }

    
    
    //取時間區間
    func dataRange(){
        let startPoint = startTime[0] + startTime[1]
        let endPoint = endTime[0] + endTime[1]
        //刪除上半段
        glucose = glucose.filter { stringToTimestamp(timeString: $0.createDate!) > stringToTimestamp(timeString: startPoint) }

        //刪除下半段
        glucose = glucose.filter { stringToTimestamp(timeString: $0.createDate!) < stringToTimestamp(timeString: endPoint) }

    }
    
    //取得指定使用者資料
    func dataSeletUser(){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.userId == UserGetSetData.Id}
        glucoseAll = glucose
    }
    
    //取得指定寵物資料
    func dataSeletPet(){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.petName == UserGetSetData.PetName}
        glucoseAll = glucose
    }
    
    //取對應Note Data
    func selectNoteData(){
        var noteTemp:[NoteModel] = []
        for i in  0 ..< glucose.count{
            for j in 0 ..< noteList.count{
                if(noteList[j].bloodId == glucose[i].id){
                    noteTemp.append(noteList[j])
                    break
                }
            }
        }
        noteList = noteTemp
    }
    
    func setTimeRenge(data: Date,input: String){
        let dateFormatterInput = DateFormatter()
        dateFormatterInput.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let dateFormatterOutput = DateFormatter()
        dateFormatterOutput.dateFormat = "yyyy-MM-dd"
        
        let timeCheck = stringToTimestamp(timeString: dateFormatterInput.string(from: data))
        let startPoint = stringToTimestamp(timeString: startTime[0] + startTime[1])
        let endPoint = stringToTimestamp(timeString: endTime[0] + endTime[1])
        
        
        switch input {
        case "start":
            if(endPoint < timeCheck){
                self.toUIButton.setTitle("\(dateFormatter.string(from: data))",for: .normal)
                endTime[0] = "\(dateFormatterOutput.string(from: data))"
            }
            else{
                self.fromUIButton.setTitle("\(dateFormatter.string(from: data))",for: .normal)
                startTime[0] = "\(dateFormatterOutput.string(from: data))"
            }
        case "end":
            if(startPoint > timeCheck){
                self.fromUIButton.setTitle("\(dateFormatter.string(from: data))",for: .normal)
                startTime[0] = "\(dateFormatterOutput.string(from: data))"
            }
            else{
                self.toUIButton.setTitle("\(dateFormatter.string(from: data))",for: .normal)
                endTime[0] = "\(dateFormatterOutput.string(from: data))"
            }
            
        default:
            break
        }
        
        self.loadData()
        self.dataSeletUser()
        self.dataSeletPet()
        self.dataRange()
        self.dataTableView.reloadData()
    }
    
    func checkRange(max: String,min: String)->String{
        var result = ""
        if(max == "" || min == ""){
            result = "Please fill in all forms"
        }
        else{
            let maxInt = Int(max)
            let minInt = Int(min)
            if(maxInt! < minInt!){
                result = "Min must less than max"
            }
            else if(maxInt == minInt){
                result = "Min can not be equal to max"
            }
            else {
                result = "Range changed successfully"
            }
        }
        return result
    }
    
    
    @objc func syncAction(){
        http()
    }
    
    
    
    @objc func deleteData(){
        let alert = UIAlertController(title: "Please select data you want to delete", message: "", preferredStyle: .alert)
        let foreheadAction = UIAlertAction(title: "Cloud", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.deleteCloudData()
        }
        let ambientAction = UIAlertAction(title: "Local", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.deleteLocalData()
        }
        alert.addAction(foreheadAction)
        alert.addAction(ambientAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //刪除本機資料
    func deleteLocalData(){
        let alert = UIAlertController(title: "Delete Data", message: "Are you sure you want to delete all local data", preferredStyle: .alert)
        let foreheadAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            (action: UIAlertAction!) -> Void in
            
        }
        let ambientAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.loadData()
            self.dataSeletUser()
            self.dataSeletPet()
            self.loadNoteData()
            self.selectNoteData()
            
            for i in 0 ..< self.glucose.count{
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    let context = appDelegate.persistentContainer.viewContext
                    context.delete(self.glucose[i])
                    appDelegate.saveContext()
                }
            }
            
            for i in 0 ..< self.noteList.count{
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    let context = appDelegate.persistentContainer.viewContext
                    context.delete(self.noteList[i])
                    appDelegate.saveContext()
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
            self.loadData()
            self.dataSeletUser()
            self.dataSeletPet()
            self.dataRange()
            self.dataTableView.reloadData()

        }
        alert.addAction(foreheadAction)
        alert.addAction(ambientAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //刪除雲端資料
    func deleteCloudData(){
        let alert = UIAlertController(title: "Delete Data", message: "Are you sure you want to delete all local data", preferredStyle: .alert)
        let foreheadAction = UIAlertAction(title: "NO", style: UIAlertAction.Style.cancel) {
            (action: UIAlertAction!) -> Void in
            
        }
        let ambientAction = UIAlertAction(title: "YES", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            EZLoadingActivity.show("Loading...", disableUI: true)
            if Reachability.isConnectedToNetwork() == true {
                self.cellHttpDeleteService()
                
                let queue = DispatchQueue(label:"DataUpload",qos: DispatchQoS.userInitiated)
                
                queue.async {
                    var i = 0
                    let timeOut = 150
                    repeat{
                        Thread.sleep(forTimeInterval: 1)
                        i = i+1
                    }while(!self.httpConnect && i < timeOut)
                    
                    self.checkDataDelete()
                }
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                    EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                    EZLoadingActivity.hide(false, animated: false)
                }
            }
        }
        alert.addAction(foreheadAction)
        alert.addAction(ambientAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //字串轉Unix Time
    func stringToTimestamp(timeString: String)->Int64{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        let date = dfmatter.date(from: "\(timeString)")
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int64 = Int64(dateStamp)
        return dateSt
    }
    
    //http post
    func http(){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellHttpService()
            
            let queue = DispatchQueue(label:"DataUpload",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 150
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.httpConnect && i < timeOut)
                
                self.checkDataUpload()
                
                
            }
            
        } else {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
            
            
        }
    }
    
    //download Sync Update data
    func cellHttpService() {
        
        let httpFunc = setHttpFunc()
        
        if(httpFunc == "sync"){
            EZLoadingActivity.show("Loading...", disableUI: true)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                EZLoadingActivity.Settings.SuccessText = "sync Success"
                EZLoadingActivity.hide(true, animated: false)
            }

        }
        else{
            let data:String = setHttpVars()
            
            HttpServices.sharedInstance.postConnect(httpFunc, httpString: data) { (status, result) in
                
                self.json = HttpServices.sharedInstance.getAppCall(result!)
                print(status)
                print(self.json)
                self.httpConnect = status
                self.httpResult = self.json["status"].string!
                
            }
        }
    }
    
    //delete data
    func cellHttpDeleteService() {
        
        let httpFunc = "DeleteBloodData"
        
        let data:String = "{'userid':'\(UserGetSetData.Id)'}"
            
        HttpServices.sharedInstance.postConnect(httpFunc, httpString: data) { (status, result) in
            self.json = HttpServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.httpConnect = status
            self.httpResult = self.json["status"].string!
                
        }
        
    }

    //設定要使用的 httpFunc
    func setHttpFunc()->String{
        var result = "sync"
        
        var checkSended = false
        var checkChanged = false
        
        for i  in 0 ..< glucoseAll.count{
            if(glucoseAll[i].sended == 0){
                checkSended = true
            }
            else if (glucoseAll[i].changed != 0){
                checkChanged = true
            }
        }
        if(glucoseAll.count == 0){
            result = "downloadBloodDataPet"
        }
        else{
            if(checkSended) {
                result = "SyncBloodDataPet"
            }
            else if (checkChanged){
                result = "UpdateBloodDataChange"
            }
        }
        
        print(result)
        return result
    }
    
    //finish download Sync Update data
    func checkDataUpload(){
        if(self.httpConnect){
            print("YES")
            if(self.httpResult == "true"){
                let message = self.json["message"].string!
                if(message == "sync success"){
                    setSended(pointer: Int(self.json["data"].string!)!)
                    setChanged()
                    setDonwloadData()
                    loadCountData()
                    coreData()
                    self.loadData()
                    self.dataSeletUser()
                    self.dataSeletPet()
                    self.dataRange()
                    self.dataTableView.reloadData()
                }
                else if(message == "download Success" ){
                    setDonwloadData()
                    loadCountData()
                    coreData()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0 + Double(idArray.count)/100) {
                        self.loadData()
                        self.dataSeletUser()
                        self.dataSeletPet()
                        self.dataRange()
                        self.dataTableView.reloadData()
                    }
                }
                else{
                    setChanged()
                    EZLoadingActivity.Settings.SuccessText = "Sync Success"
                    EZLoadingActivity.hide(true, animated: false)
                }
                
            }
            else{
                EZLoadingActivity.Settings.FailText = "Sync Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.httpConnect = false
    }
    
    //finish Delete Data
    func checkDataDelete(){
        if(self.httpConnect){
            print("YES")
            if(self.httpResult == "true"){
                EZLoadingActivity.Settings.SuccessText = "Delete Success"
                EZLoadingActivity.hide(true, animated: false)
            }
            else{
                EZLoadingActivity.Settings.FailText = "Delete Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.httpConnect = false
    }
    
    
    func setHttpVars()->String{
        return "{'userid':'\(UserGetSetData.Id)','petname':'\(UserGetSetData.PetName)','data':\(setDataHttp(mode: "data")),'change':\(setDataHttp(mode: "change"))}"
    }
    
    //設定http 資料（data change）
    func setDataHttp(mode: String)->String{
        var result = "["
        var dataCount = 0
        
        for i in 0 ..< glucoseAll.count{
            let glucoseData = glucoseAll[i]
            
            if(glucoseData.changed != 0 && mode == "change"){
                result = result + "{'sended':'\(glucoseData.sended)' ,'value':'\(glucoseData.bloodValue)','status':'\(glucoseData.bloodStatus)','measure_date':'\(glucoseData.createDate!)','min':'\(glucoseData.min)','max':'\(glucoseData.max)'},"
                dataCount = dataCount + 1
            }
            
            if(glucoseData.sended == 0 && mode == "data"){
                result = result + "{'value':'\(glucoseData.bloodValue)','status':'\(glucoseData.bloodStatus)','measure_date':'\(glucoseData.createDate!)','min':'\(glucoseData.min)','max':'\(glucoseData.max)'},"
                dataCount = dataCount + 1
            }
        }
        
        if(dataCount != 0){
            //去除最後一個逗號 從尾巴往前一位
            let index = result.index(result.endIndex, offsetBy: -1)
            result = result.substring(to: index)
        }
        
        result = result + "]"
        print(result)
        return result
    }
    
    //format download data
    func setDonwloadData(){
        idArray = []
        dateArray = []
        statusArray = []
        valueArray = []
        maxArray = []
        minArray = []
        
        let downloadData = self.json["data"]
        
        for i  in 0 ..< downloadData.count{
            idArray.append(Int64(downloadData[i]["id"].string!)!)
            dateArray.append(downloadData[i]["measuredate"].string!)
            statusArray.append(Int64(downloadData[i]["bloodstatus"].string!)!)
            valueArray.append(Int64(downloadData[i]["bloodvalue"].string!)!)
            maxArray.append(Int64(downloadData[i]["max"].string!)!)
            minArray.append(Int64(downloadData[i]["min"].string!)!)
            
        }

    }
    
    //存入下載資料至coredata
    func coreData(){
        
        for i in 0 ..< dateArray.count{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(idArray.count)/100) {
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    self.glucoseDownload = GlucoseModel(context: appDelegate.persistentContainer.viewContext)
                    self.glucoseDownload.userId = UserGetSetData.Id
                    self.glucoseDownload.petName = UserGetSetData.PetName
                    self.glucoseDownload.bloodStatus = self.statusArray[i]
                    self.glucoseDownload.bloodValue = self.valueArray[i]
                    self.glucoseDownload.createDate = "\(self.dateArray[i])"
                    self.glucoseDownload.max = self.maxArray[i]
                    self.glucoseDownload.min = self.minArray[i]
                    self.glucoseDownload.changed = 0
                    self.glucoseDownload.sended = self.idArray[i]
                    self.glucoseDownload.id = self.countList[0].bloodCount + Int64(i)
                    
                    
                    self.note = NoteModel(context: appDelegate.persistentContainer.viewContext)
                    self.note.userId = UserGetSetData.Id
                    self.note.petName = UserGetSetData.PetName
                    self.note.noteAction = self.valueArray[i]
                    self.note.noteContent = UserGetSetData.PetName
                    self.note.createDate = "\(self.dateArray[i])"
                    self.note.noteVoice = nil
                    self.note.noteImage = nil
                    self.note.noteType = "glucose"
                    self.note.bloodId = self.countList[0].bloodCount + Int64(i)
                    self.note.pressureId = -1
                    self.note.temperatureId = -1
                    
                    appDelegate.saveContext()
                }
            }
        }
        
        
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0 + Double(idArray.count)/100) {
            
            self.countList[0].bloodCount = self.countList[0].bloodCount +  Int64(self.dateArray.count)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
            EZLoadingActivity.Settings.SuccessText = "sync Success"
            EZLoadingActivity.hide(true, animated: false)
        }
    }
    
    //傳送完畢 更新資料庫sended 同步database
    func setSended(pointer: Int){
        var uploadNumber = 0
        for i  in 0 ..< glucoseAll.count{
            if(glucoseAll[i].sended == 0){
                uploadNumber = uploadNumber + 1
            }
        }
        var nowPointer = pointer - uploadNumber
        for i  in 0 ..< glucoseAll.count{
            if(glucoseAll[i].sended == 0){
                glucoseAll[i].sended = Int64(nowPointer)
                nowPointer = nowPointer + 1
            }
        }

    }
    
    //傳送完畢 更新資料庫changed
    func setChanged(){
        for i  in 0 ..< glucoseAll.count{
            glucoseAll[i].changed = 0
        }
    }
    
}
