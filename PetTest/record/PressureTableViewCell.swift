//
//  PressureTableViewCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/5.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class PressureTableViewCell: UITableViewCell {
    
    @IBOutlet  var dateLable: UILabel!
    @IBOutlet  var timeLable: UILabel!
    @IBOutlet  var pulLable: UILabel!
    @IBOutlet  var sysLable: UILabel!
    @IBOutlet  var diaLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
