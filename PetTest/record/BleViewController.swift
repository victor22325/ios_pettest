//
//  bleViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/16.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
//import RxBluetoothKit
//import RxSwift
import CoreBluetooth
import EZLoadingActivity

class BleViewController: UIViewController {
    
    @IBOutlet weak var downloadUIButton: UIButton!
    @IBOutlet weak var syncUIButton: UIButton!
    @IBOutlet weak var disconnectUIButton: UIButton!
    @IBOutlet weak var syncUILabel: UILabel!
    @IBOutlet weak var deviceNameLabel: UILabel!
    
//    private var connectedPeripheral: Peripheral?
//    private let disposeBag = DisposeBag()
//
//    fileprivate var servicesList: [Service] = []
//    fileprivate var service: Service!
//
//    fileprivate var characteristicsList: [Characteristic] = []
//    fileprivate var notify: Characteristic!
//    fileprivate var write: Characteristic!
    
//    let bleFunction: BleFunction = BleFunction(DeviceStatus: UserGetSetData.Device_Status)
    var notifyResult = ""
    
    var temperatureModeSelect = false
    var temperatureStatus = 1

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Ble 相關參數初始化
        
        print("bleFunction manager connect")
//        guard BleFunction.scannedPeripheral != nil else { return }
//        title = "Connecting"
//        BleFunction.manager.connect(BleFunction.scannedPeripheral.peripheral)
//            .subscribe(onNext: { [weak self] in
//                guard let `self` = self else { return }
//                self.title = "Connected"
//                self.connectedPeripheral = $0
//                self.monitorDisconnection(for: $0)
//                self.downloadServices(for: $0)
//                }, onError: { [weak self] error in
//
//            })
//            .addDisposableTo(disposeBag)
//        startBle()
        
//        setVisable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionSend(sender: UIButton){
        //下載資料
//        if(sender == downloadUIButton){
//            if(UserGetSetData.Device_Status == 2 && !temperatureModeSelect){
//                temperatureModeSelect = true
//                temperatureDownloadSelect()
//            }
//            else{
//                EZLoadingActivity.show("Loading...", disableUI: true)
//                self.notifyResult = ""
//                self.writeValueForCharacteristic(hexadecimalString:  BleFunction.downloadCmd, characteristic: write)
//
//                let queue = DispatchQueue(label:"check",qos: DispatchQoS.userInitiated)
//                queue.async {
//                    var i = 0
//                    var time = 2.0
//                    let timeOut = 150
//                    var check = false
//                    var checkNull = false
//                    if(UserGetSetData.Device_Status == 2){
//                        time = 4.0
//                        check = true
//                    }
//                    repeat{
//                        let asyncNotifyResult =  self.notifyResult
//                        Thread.sleep(forTimeInterval: time)
//                        i = i+2
//                        if(asyncNotifyResult == self.notifyResult){
//                            check = true
//                        }
//                        if(self.notifyResult == ""){
//                            checkNull = true
//                        }
//
//                    }while((!check && i < timeOut) && (!checkNull))
//                    if(checkNull){
//                        EZLoadingActivity.Settings.FailText = "No data"
//                        EZLoadingActivity.hide(false, animated: false)
//
//                    }
//                    else{
//                        if(check){
//                            //ble data 載入成功 傳送指定格式Data至指定頁面
//
//                            EZLoadingActivity.Settings.SuccessText = "downloadCmd Success"
//                            EZLoadingActivity.hide(true, animated: false)
//                            switch UserGetSetData.Device_Status{
//                            case 0:
//                                BleFunction.downloadResult = self.bleFunction.glucoseDataFormat(NotifyResult: self.notifyResult)
//                                let storyboard = UIStoryboard(name: "Record",bundle:nil)
//                                let controller = storyboard.instantiateViewController(withIdentifier: "showBleDownload")
//                                self.present(controller, animated: true, completion: nil)
//
//                            case 1:
//                                BleFunction.downloadResult = self.bleFunction.pressureDataFormat(NotifyResult: self.notifyResult)
//                                let storyboard = UIStoryboard(name: "Record",bundle:nil)
//                                let controller = storyboard.instantiateViewController(withIdentifier: "showBleDownload")
//                                self.present(controller, animated: true, completion: nil)
//                            case 2:
//                                self.temperatureModeSelect = false
//                                BleFunction.downloadResult = self.bleFunction.temperatureDataFormat(NotifyResult: self.notifyResult,bleStatus: self.temperatureStatus)
//                                let storyboard = UIStoryboard(name: "Record",bundle:nil)
//                                let controller = storyboard.instantiateViewController(withIdentifier: "showBleDownload")
//                                self.present(controller, animated: true, completion: nil)
//
//                            default:
//                                break
//                            }
//
//                        }
//                        else{
//                            EZLoadingActivity.Settings.FailText = "downloadCmd fail"
//                            EZLoadingActivity.hide(false, animated: false)
//                        }
//                    }
//                }
//            }
//
//        }
        //同步時間
//        else if(sender == syncUIButton){
//            EZLoadingActivity.show("Loading...", disableUI: true)
//            self.notifyResult = ""
//            if(UserGetSetData.Device_Status == 0){
//                self.writeValueForCharacteristic(hexadecimalString: bleFunction.glucoseTime(), characteristic: write)
//            }
//            else if(UserGetSetData.Device_Status == 1){
//                self.writeValueForCharacteristic(hexadecimalString: bleFunction.pressureDay(), characteristic: write)
//                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4.0) {
//                    self.writeValueForCharacteristic(hexadecimalString: self.bleFunction.pressureTime(), characteristic: self.write)
//                }
//            }
//
//            let queue = DispatchQueue(label:"check",qos: DispatchQoS.userInitiated)
//            queue.async {
//                var i = 0
//                var time = 1.0
//                let timeOut = 10
//                var check = false
//                if(UserGetSetData.Device_Status != 0){
//                    time = 4.0
//                    check = true
//                }
//                repeat{
//                    Thread.sleep(forTimeInterval: time)
//                    i = i+1
//                    if(self.bleFunction.glucoseCheckOK(NotifyResult: self.notifyResult)){
//                        check = true
//                    }
//
//                }while(!check && i < timeOut)
//                if(check){
//                    EZLoadingActivity.Settings.SuccessText = "Sync Success"
//                    EZLoadingActivity.hide(true, animated: false)
//
//                }
//                else{
//                    EZLoadingActivity.Settings.FailText = "Sync fail"
//                    EZLoadingActivity.hide(false, animated: false)
//                }
//
//            }
//
//        }
//        //中止連線
//        else if(sender == disconnectUIButton){
//            setDisconnect()
//        }
    
    }
    
    //偵測Meter連線狀況
//    private func monitorDisconnection(for peripheral: Peripheral) {
//        print("BleViewController monitorDisconnection")
//        BleFunction.manager.monitorDisconnection(for: peripheral)
//            .subscribe(onNext: { [weak self] (peripheral) in
//                EZLoadingActivity.Settings.FailText = "Sync fail"
//                EZLoadingActivity.hide(false, animated: false)
//
//                let alert = UIAlertController(title: "Disconnected!", message: "Meter Disconnected", preferredStyle: .alert)
//                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
//                    (action: UIAlertAction!) -> Void in
//                    self?.setDisconnect()
//                }
//                alert.addAction(okAction)
//                self?.present(alert, animated: true, completion: nil)
//
//            }).addDisposableTo(disposeBag)
//    }
    
    //抓取目前Ble裝置 全部 service
//    private func downloadServices(for peripheral: Peripheral) {
//        print("BleViewController downloadServices")
//        peripheral.discoverServices(nil)
//            .subscribe({ services in
//                self.servicesList = services
//            }).addDisposableTo(disposeBag)
//    }

    //驗證是否取得正確services (正確轉至getCharacteristics)
//    private func startBle(){
//        let queue = DispatchQueue(label:"startBle",qos: DispatchQoS.userInitiated)
//
//        queue.async {
//            var i = 0
//            var timeOut = 10
//            var check = false
//            repeat{
//                Thread.sleep(forTimeInterval: 1)
//                i = i+1
//                if(self.servicesList.count>0){
//                    check = true
//                }
//            }while(!check && i < timeOut)
//
//            if(check){
//                self.selectServices()
//
//                i = 0
//                timeOut = 5
//                check = false
//
//                repeat{
//                    Thread.sleep(forTimeInterval: 1)
//                    i = i+1
//                    if((self.service) != nil){
//                        check = true
//                    }
//                }while(!check && i < timeOut)
//                if(check){
//                    self.getCharacteristics(for: self.service)
//                }
//                else{
//                    self.errorShow()
//                }
//            }
//            else{
//                self.errorShow()
//            }
//        }
//
//    }

    //取得指定的service
//    private func selectServices() {
//        for i in 0 ..< self.servicesList.count {
//            if(self.servicesList[i].uuid.uuidString == BleFunction.serviceUUID){
//                print(self.servicesList[i].uuid.uuidString)
//                self.service = self.servicesList[i]
//            }
//        }
//    }

    //驗證是否取得正確characteristic (正確轉至startNotify)
//    private func getCharacteristics(for service: Service) {
//        print("BleViewController getCharacteristics")
//        service.discoverCharacteristics(nil)
//            .subscribe({ characteristics in
//                self.characteristicsList = characteristics
//            }).addDisposableTo(disposeBag)
//
//        var i = 0
//        var timeOut = 10
//        var check = false
//        repeat{
//            Thread.sleep(forTimeInterval: 1)
//            i = i+1
//            if(self.servicesList.count>0){
//                check = true
//            }
//        }while(!check && i < timeOut)
//        if(check){
//            self.selectCharacteriscs()
//            i = 0
//            timeOut = 5
//            check = false
//
//            repeat{
//                Thread.sleep(forTimeInterval: 1)
//                i = i+1
//                if(((self.notify != nil) && (self.write != nil))){
//                    check = true
//                }
//            }while(!check && i < timeOut)
//            if(check){
//                startNotify()
//                BleFunction.connectFlag = true
//
//                //blood pressure init
//                if(UserGetSetData.Device_Status == 1){
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pressureMonitorBle"), object: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pressureBleConnect"), object: nil)
//
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
//                        self.writeValueForCharacteristic(hexadecimalString: self.bleFunction.pressureInit(), characteristic: self.write)
//                        EZLoadingActivity.Settings.SuccessText = "Connect Success"
//                        EZLoadingActivity.hide(true, animated: false)
//                        self.deviceNameLabel.text = "Clink Blood"
//                    }
//                }
//                else if(UserGetSetData.Device_Status == 2){
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "temperatureMonitorBle"), object: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "temperatureBleConnect"), object: nil)
//
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
//                        EZLoadingActivity.Settings.SuccessText = "Connect Success"
//                        EZLoadingActivity.hide(true, animated: false)
//                        self.deviceNameLabel.text = "Thermometer"
//                    }
//                }
//                else{
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "glucoseMonitorBle"), object: nil)
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "glucoseBleConnect"), object: nil)
//
//                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
//                        self.notifyResult = ""
//                        self.writeValueForCharacteristic(hexadecimalString: self.bleFunction.glucoseDeviceName(), characteristic: self.write)
//                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
//
//                            EZLoadingActivity.Settings.SuccessText = "Connect Success"
//                            EZLoadingActivity.hide(true, animated: false)
//                            self.deviceNameLabel.text = self.bleFunction.glucoseCheckDeviceName(NotifyResult: self.notifyResult)
//                        }
//                    }
//
//                }
//
//            }
//            else{
//                errorShow()
//            }
//
//        }
//        else{
//            errorShow()
//        }
//    }

    //取得指定的characteristic
//    private func selectCharacteriscs(){
//        for i in 0 ..< self.characteristicsList.count {
//            if(self.characteristicsList[i].uuid.uuidString == BleFunction.notifyUUID){
//                print(self.servicesList[i].uuid.uuidString)
//                self.notify = self.characteristicsList[i]
//            }
//            else if(self.characteristicsList[i].uuid.uuidString == BleFunction.writeUUID){
//                print(self.servicesList[i].uuid.uuidString)
//                self.write = self.characteristicsList[i]
//            }
//
//        }
//    }

    //指定characteristic ble notify
//    private func startNotify() {
//        print("BleViewController startNotify")
//        self.notify.setNotifyValue(true).subscribe(onNext: { [weak self] _ in
//
//        }).addDisposableTo(disposeBag)
//
//        self.notify.monitorValueUpdate().subscribe(onNext: { [weak self] _ in
//            print(self?.notify.value?.hexadecimalString ?? "")
//            self?.notifyResult = (self?.notifyResult)!+"\(self?.notify.value?.hexadecimalString ?? "")-"
//        }).addDisposableTo(disposeBag)
//
//    }

    //ble write
//    private func writeValueForCharacteristic(hexadecimalString: String,characteristic: Characteristic) {
//        let hexadecimalData: Data = Data.fromHexString(string: hexadecimalString)
//        let type: CBCharacteristicWriteType = characteristic.properties.contains(.write) ? .withResponse : .withoutResponse
//        characteristic.writeValue(hexadecimalData as Data, type: type)
//            .subscribe({ [weak self] _ in
//
//            }).disposed(by: disposeBag)
//    }
//
//    private func errorShow(){
//        EZLoadingActivity.Settings.FailText = ""
//        EZLoadingActivity.hide(false, animated: false)
//
//        let alert = UIAlertController(title: "Connect Fail!", message: "Connection is unstable or meter is not correct", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//            (action: UIAlertAction!) -> Void in
//            self.setDisconnect()
//        }
//        alert.addAction(okAction)
//        self.present(alert, animated: true, completion: nil)
//
//    }

    private func setDisconnect(){
        print("BleViewController setDisconnect")
//        BleFunction.manager.cancelPeripheralConnection(BleFunction.scannedPeripheral.peripheral)
//            .subscribe(onNext: { [weak self] (peripheral) in
//
//            }).addDisposableTo(disposeBag)
//        BleFunction.connectFlag = false
//        self.dismiss(animated: true, completion: nil)
    }
    
    //設定額溫槍UI顯示
//    func setVisable(){
//        if UserGetSetData.Device_Status == 2{
//            syncUIButton.isHidden  = true
//            syncUILabel.isHidden = true
//        }
//    }
//
//    //選擇額溫槍模式
//    func temperatureDownloadSelect(){
//        let alert = UIAlertController(title: "Please select measure mode", message: "", preferredStyle: .alert)
//        let foreheadAction = UIAlertAction(title: "Forehead", style: UIAlertAction.Style.default) {
//            (action: UIAlertAction!) -> Void in
//            self.temperatureStatus = 2
//            self.actionSend(sender: self.downloadUIButton)
//        }
//        let ambientAction = UIAlertAction(title: "Ambient", style: UIAlertAction.Style.default) {
//            (action: UIAlertAction!) -> Void in
//            self.temperatureStatus = 1
//            self.actionSend(sender: self.downloadUIButton)
//        }
//        alert.addAction(foreheadAction)
//        alert.addAction(ambientAction)
//        self.present(alert, animated: true, completion: nil)
//    }
//
//
//
//}

// MARK: - Presentr Delegate

//extension BleViewController: PresentrDelegate {
//
//    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
//        print("Dismissing View Controller")
//        return !keyboardShowing
//    }
//
//}

// MARK: - UITextField Delegate

//extension BleViewController: UITextFieldDelegate {
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//
}
