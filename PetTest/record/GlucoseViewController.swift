//
//  GlucoseViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/25.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
import Charts
import CoreData
//import RxBluetoothKit
//import RxSwift
import EZLoadingActivity

class GlucoseViewController: UIViewController,NSFetchedResultsControllerDelegate{

    @IBOutlet weak var preMealUIButton: UIButton!
    @IBOutlet weak var postMealUIButton: UIButton!
    @IBOutlet weak var allDateUIButton: UIButton!
    @IBOutlet weak var oneMonthUIButton: UIButton!
    @IBOutlet weak var threeMonthUIButton: UIButton!
    
    @IBOutlet weak var unitUILabel: UILabel!
    @IBOutlet weak var timePeriodUILabel: UILabel!
    
    //function call
    let optionBar = OptionBar()
    let dataFunction = DataFunction()
    
    //DB data
    var fetchResultController: NSFetchedResultsController<GlucoseModel>!
    var glucose:[GlucoseModel] = []
    
    //line chart
    @IBOutlet weak var lineChartView: LineChartView!
    weak var axisFormatDelegate: IAxisValueFormatter?
    
    var postMealTime = ["2017-05-26 14:36:00", "2017-05-26 14:36:05"]
    var preMealTime = ["2017-05-26 14:36:10", "2017-05-26 17:37:00"]
    var generalTime = ["2017-05-26 14:37:05", "2017-05-26 20:37:10"]
    
    var postMealData = [1.0,4.0]
    var preMealData = [3.0,1.0]
    var generalData = [2.0,3.0]
    
    var nowMode = "All"
    
    //pie chart
    @IBOutlet weak var preMealUILabel: UILabel!
    @IBOutlet weak var postMealUILabel: UILabel!
    @IBOutlet weak var preMealChartView: PieChartView!
    @IBOutlet weak var postMealChartView: PieChartView!
    
    var pieArray: [String] =  ["Above", "Within", "Below"]
    var preMealPieData = [10.0, 4.0, 6.0]
    var postMealPieData = [15.0, 40.0, 16.0]
    
//    var optionsMenu: CAPSOptionsMenu?
    
//    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewInit();
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAction), name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(monitorDisconnection), name: NSNotification.Name(rawValue: "glucoseMonitorBle"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setImageBleConnect), name: NSNotification.Name(rawValue: "glucoseBleConnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setImageBleDisconnect), name: NSNotification.Name(rawValue: "glucoseBleDisConnect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeGlucoseViewController"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(menuBarSelectorReceived), name: NSNotification.Name(rawValue: "menuBarSelector"), object: nil)
//        optionsMenu = optionBar.addOptionsMenu(ViewController: self)
        
        loadDbData()
        dataSeletUser()
        dataSeletPet()
        
        setDbData(period: false,periodtime: 0)
        
        axisFormatDelegate = self
        
        
        updateChartWithData(mode: setSpecies(currentSpecies: UserGetSetData.PetSpecies),postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
        setSpeciesButten(currentSpecies: UserGetSetData.PetSpecies)
        
        setChart(dataPoints: pieArray, preMealData: preMealPieData,postMealData: postMealPieData)
        
        setTimePeriod()
    }
    
    func setSpecies(currentSpecies: String)->String{
        var result = "All"
        
        if(currentSpecies == "Canine"){
            result = "postMeal"
        }
        else if(currentSpecies == "Feline"){
            result = "preMeal"
        }
        
        return result
    }
    
    func setSpeciesButten(currentSpecies: String){
        if(currentSpecies == "Canine"){
            nowMode = "postMeal"
            setClick(sender: postMealUIButton)
            setPieVisable(preMealVisable: false,postMealVisable: true)
            updateChartWithData(mode: "postMeal",postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            postMealUIButton.setTitle(UserGetSetData.PetName, for: .normal)
            postMealUIButton.isHidden = true
            setChartsChange()
        }
        else if(currentSpecies == "Feline"){
            nowMode = "preMeal"
            setClick(sender: preMealUIButton)
            setPieVisable(preMealVisable: true,postMealVisable: false)
            updateChartWithData(mode: "preMeal",postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            preMealUIButton.setTitle(UserGetSetData.PetName, for: .normal)
            postMealUIButton.isHidden = true
            setChartsChange()
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        monitorDisconnection()
    }
    
    //set logo
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        setNavigationItem(bleImageString: "bluetooth_gray")
        
        unitUILabel.text = dataFunction.setUnit(DeviceStatus: UserGetSetData.Device_Status)
        
        //line init
        lineChartView.backgroundColor = UIColor.white
        lineChartView.chartDescription?.enabled = false
        lineChartView.xAxis.labelPosition = .bottom
        lineChartView.xAxis.labelCount = 3
        lineChartView.noDataText = "NO DATA"
        lineChartView.noDataFont = NSUIFont(name: "HelveticaNeue-Bold", size: 40.0)!
        
        //pie init
        preMealChartView.usePercentValuesEnabled = true
        preMealChartView.drawHoleEnabled = false
        preMealChartView.chartDescription?.enabled = false
        preMealChartView.noDataText = "NO DATA"
        preMealChartView.noDataFont = NSUIFont(name: "HelveticaNeue-Bold", size: 20.0)!
        
        postMealChartView.usePercentValuesEnabled = true
        postMealChartView.drawHoleEnabled = false
        postMealChartView.chartDescription?.enabled = false
        postMealChartView.noDataText = "NO DATA"
        postMealChartView.noDataFont = NSUIFont(name: "HelveticaNeue-Bold", size: 20.0)!
        
    }
    
    @IBAction func actionSend(sender: UIButton){

        if(sender == preMealUIButton){
            nowMode = "preMeal"
            setClick(sender: preMealUIButton)
            setPieVisable(preMealVisable: true,postMealVisable: false)
            updateChartWithData(mode: "preMeal",postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            
            setChartsChange()
        }

        else if(sender == postMealUIButton){
            nowMode = "postMeal"
            setClick(sender: postMealUIButton)
            setPieVisable(preMealVisable: false,postMealVisable: true)
            updateChartWithData(mode: "postMeal",postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            
            setChartsChange()
        }
        else if(sender == allDateUIButton){
            setClick(sender: allDateUIButton)
            
            setDbData(period: false,periodtime: 0)
            
            setTimePeriod()
            
            updateChartWithData(mode: nowMode,postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            setChart(dataPoints: pieArray, preMealData: preMealPieData,postMealData: postMealPieData)

            setChartsChange()
        }
        else if(sender == oneMonthUIButton){
            setClick(sender: oneMonthUIButton)
            
            setDbData(period: true,periodtime: -1)
            
            let timeStamp = TimeData.sharedInstance.timestampShift(shiftMonth: 0)
            let timeStampSet = TimeData.sharedInstance.timestampShift(shiftMonth: -1)
            
            setTimeRange(startTime: timeStampSet, endTime: timeStamp)
            
            self.lineChartView.xAxis.axisMinimum = Double(timeStampSet)
            self.lineChartView.xAxis.axisMaximum = Double(timeStamp)
            
            updateChartWithData(mode: nowMode,postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            setChart(dataPoints: pieArray, preMealData: preMealPieData,postMealData: postMealPieData)
            
            setChartsChange()
        }
        else if(sender == threeMonthUIButton){
            setClick(sender: threeMonthUIButton)
            
            setDbData(period: true,periodtime: -3)
            
            let timeStamp = TimeData.sharedInstance.timestampShift(shiftMonth: 0)
            let timeStampSet = TimeData.sharedInstance.timestampShift(shiftMonth: -3)
            
            setTimeRange(startTime: timeStampSet, endTime: timeStamp)
            
            self.lineChartView.xAxis.axisMinimum = Double(timeStampSet)
            self.lineChartView.xAxis.axisMaximum = Double(timeStamp)
            
            updateChartWithData(mode: nowMode,postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
            setChart(dataPoints: pieArray, preMealData: preMealPieData,postMealData: postMealPieData)
            
            setChartsChange()
        }
    }
    
    //設定 timePeriodUILabel 文字
    func setTimeRange(startTime: Int64,endTime: Int64){
        let dateStart = Date(timeIntervalSince1970: TimeInterval(startTime))
        let dateEnd = Date(timeIntervalSince1970: TimeInterval(endTime))
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        let startString = dateFormatter.string(from: dateStart)
        let endString = dateFormatter.string(from: dateEnd)
        
        timePeriodUILabel.text = "\(startString) - \(endString)"
    }
    
    //設定圖表監聽
    func setChartsChange(){
        self.lineChartView.notifyDataSetChanged()
        self.lineChartView.invalidateIntrinsicContentSize()
        self.preMealChartView.notifyDataSetChanged()
        self.preMealChartView.invalidateIntrinsicContentSize()
        self.postMealChartView.notifyDataSetChanged()
        self.postMealChartView.invalidateIntrinsicContentSize()

    }
    
    //set button UI click
    func setClick(sender: UIButton){
        let red = UIColor(red: 193/255, green: 36/255, blue: 60/255, alpha: 1)
        let gray = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
        
        
        if(sender == preMealUIButton){
            preMealUIButton.backgroundColor = red
            postMealUIButton.backgroundColor = gray
        }
            
        else if(sender == postMealUIButton){
            preMealUIButton.backgroundColor = gray
            postMealUIButton.backgroundColor = red
        }
        else if(sender == allDateUIButton){
            allDateUIButton.backgroundColor = red
            oneMonthUIButton.backgroundColor = gray
            threeMonthUIButton.backgroundColor = gray
        }
        else if(sender == oneMonthUIButton){
            allDateUIButton.backgroundColor = gray
            oneMonthUIButton.backgroundColor = red
            threeMonthUIButton.backgroundColor = gray
        }
        else if(sender == threeMonthUIButton){
            allDateUIButton.backgroundColor = gray
            oneMonthUIButton.backgroundColor = gray
            threeMonthUIButton.backgroundColor = red
        }

    }
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<GlucoseModel> = GlucoseModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    glucose = fetchedObjects
                }
//                for glu in glucose {
//                    print("glucose bloodStatus:" + String(glu.bloodStatus) + " ,bloodValue:" + String(glu.bloodValue) + " ,date:" + String(glu.createDate!))
//                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //設定圖表數值 period 可開啟區間功能 （periodtime 要取得月份 須為負數）
    func setDbData(period: Bool,periodtime: Int64){
        //init array
        self.postMealTime = []
        self.preMealTime = []
        self.generalTime = []
        self.postMealData = []
        self.preMealData = []
        self.generalData = []
        self.postMealPieData = [0,0,0]
        self.preMealPieData = [0,0,0]
        
        var startTime:Int64 = 0
        var endTime:Int64 = 0
        
        if(period){
            startTime = TimeData.sharedInstance.timestampShift(shiftMonth: periodtime)
            endTime = TimeData.sharedInstance.timestampShift(shiftMonth: 0)
        }
        
        for i in 0 ..< self.glucose.count{
            let glucoseData = self.glucose[i]
            
            
            if(period){
                //特定區間
                if(inPeriod(startTime: startTime,endTime: endTime,dataTime: TimeData.sharedInstance.stringToTimestamp(timeString: glucoseData.createDate!))){
                    switch glucoseData.bloodStatus {
                    case Int64(PetSpecies.canine):
                        postMealTime.append(glucoseData.createDate!)
                        postMealData.append(Double(glucoseData.bloodValue))
                        if(glucoseData.bloodValue > glucoseData.max){
                            postMealPieData[0] = postMealPieData[0] + 1
                        }
                        else if(glucoseData.bloodValue < glucoseData.min){
                            postMealPieData[2] = postMealPieData[2] + 1
                        }
                        else{
                            postMealPieData[1] = postMealPieData[1] + 1
                        }
                    case Int64(PetSpecies.feline):
                        preMealTime.append(glucoseData.createDate!)
                        preMealData.append(Double(glucoseData.bloodValue))
                    
                        if(glucoseData.bloodValue > glucoseData.max){
                            preMealPieData[0] = preMealPieData[0] + 1
                        }
                        else if(glucoseData.bloodValue < glucoseData.min){
                            preMealPieData[2] = preMealPieData[2] + 1
                        }
                        else{
                            preMealPieData[1] = preMealPieData[1] + 1
                        }
                    case Int64(PetSpecies.other):
                        generalTime.append(glucoseData.createDate!)
                        generalData.append(Double(glucoseData.bloodValue))
                    default:
                        break
                    }
                }
            }
            else{
                switch glucoseData.bloodStatus {
                case Int64(PetSpecies.canine):
                    postMealTime.append(glucoseData.createDate!)
                    postMealData.append(Double(glucoseData.bloodValue))
                    if(glucoseData.bloodValue > glucoseData.max){
                        postMealPieData[0] = postMealPieData[0] + 1
                    }
                    else if(glucoseData.bloodValue < glucoseData.min){
                        postMealPieData[2] = postMealPieData[2] + 1
                    }
                    else{
                        postMealPieData[1] = postMealPieData[1] + 1
                    }
                case Int64(PetSpecies.feline):
                    preMealTime.append(glucoseData.createDate!)
                    preMealData.append(Double(glucoseData.bloodValue))
                    
                    if(glucoseData.bloodValue > glucoseData.max){
                        preMealPieData[0] = preMealPieData[0] + 1
                    }
                    else if(glucoseData.bloodValue < glucoseData.min){
                        preMealPieData[2] = preMealPieData[2] + 1
                    }
                    else{
                        preMealPieData[1] = preMealPieData[1] + 1
                    }
                case Int64(PetSpecies.other):
                    generalTime.append(glucoseData.createDate!)
                    generalData.append(Double(glucoseData.bloodValue))
                default:
                    break
                }
            }
        }
    }
    
    //取得指定使用者資料
    func dataSeletUser(){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.userId == UserGetSetData.Id}
    }
    
    //取得指定寵物資料
    func dataSeletPet(){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.petName == UserGetSetData.PetName}
    }
    
    
    //檢查使否在 startTime ～ endTime
    func inPeriod(startTime: Int64,endTime: Int64,dataTime: Int64)->Bool{
        if(dataTime > endTime){
            return false
        }
        else if (dataTime < startTime){
            return false
        }
        else{
            return true
        }
    }
    
    
    //line chart
    func updateChartWithData(mode: String,postMealTime: [String],preMealTime: [String],generalTime: [String],postMealData: [Double],preMealData: [Double],generalData: [Double]) {
        
        
        var postDataEntries: [ChartDataEntry] = []
        var preDataEntries: [ChartDataEntry] = []
        var generalDataEntries: [ChartDataEntry] = []
        
        for i in 0 ..< postMealData.count {
            let dateSt:Int64 = TimeData.sharedInstance.stringToTimestamp(timeString: postMealTime[i])
            
            let dataEntry = ChartDataEntry(x: Double(dateSt), y: postMealData[i])
            postDataEntries.append(dataEntry)
        }
        
        for i in 0 ..< preMealData.count {
            let dateSt:Int64 = TimeData.sharedInstance.stringToTimestamp(timeString: preMealTime[i])
            
            let dataEntry = ChartDataEntry(x: Double(dateSt), y: preMealData[i])
            preDataEntries.append(dataEntry)
        }
        
        for i in 0 ..< generalData.count {
            let dateSt:Int64 = TimeData.sharedInstance.stringToTimestamp(timeString: generalTime[i])
            
            let dataEntry = ChartDataEntry(x: Double(dateSt), y: generalData[i])
            generalDataEntries.append(dataEntry)
        }
        
        let postLineChartDataSet = LineChartDataSet(entries: postDataEntries, label: "Dog")
        
        let preLineChartDataSet = LineChartDataSet(entries: preDataEntries, label: "Cat")
        
        let generalLineChartDataSet = LineChartDataSet(entries: generalDataEntries, label: "General")
        
        var lineChartDataSetArray: [LineChartDataSet] = [LineChartDataSet]()
        var noData = true
        
        if(mode == "All"){
            if(postMealData.count>0){
                lineChartDataSetArray.append(postLineChartDataSet)
            }
            if(preMealData.count>0){
                lineChartDataSetArray.append(preLineChartDataSet)
            }
            if(generalData.count>0){
                lineChartDataSetArray.append(generalLineChartDataSet)
            }
            
            if(preMealData.count>0 || postMealData.count>0 || generalData.count>0){
                let lineChartData = LineChartData(dataSets: lineChartDataSetArray)
                lineChartView.data = lineChartData
                noData = false
            }
        }
        else if(mode == "preMeal"){
            if(preMealData.count>0){
                lineChartDataSetArray.append(preLineChartDataSet)
                let lineChartData = LineChartData(dataSets: lineChartDataSetArray)
                lineChartView.data = lineChartData
                noData = false
            }

        }

        else if(mode == "postMeal"){
            if(postMealData.count>0){
                lineChartDataSetArray.append(postLineChartDataSet)
                let lineChartData = LineChartData(dataSets: lineChartDataSetArray)
                lineChartView.data = lineChartData
                noData = false
            }
        }
        else if (mode == "general"){
            if(generalData.count>0){
                lineChartDataSetArray.append(generalLineChartDataSet)
                let lineChartData = LineChartData(dataSets: lineChartDataSetArray)
                lineChartView.data = lineChartData
                noData = false
            }

        }
        
        if(noData){
            lineChartView.data = nil
        }
        

        let xaxis = lineChartView.xAxis
        xaxis.valueFormatter = axisFormatDelegate
        
        preLineChartDataSet.circleRadius = 3.0
        preLineChartDataSet.circleColors =  [UIColor(red: 219/255, green: 68/255, blue: 55/255, alpha: 1)]
        preLineChartDataSet.colors =  [UIColor(red: 219/255, green: 68/255, blue: 55/255, alpha: 1)]
        
        
        postLineChartDataSet.circleRadius = 3.0
        postLineChartDataSet.circleColors =  [UIColor(red: 25/255, green: 118/255, blue: 210/255, alpha: 1)]
        postLineChartDataSet.colors =  [UIColor(red: 25/255, green: 118/255, blue: 210/255, alpha: 1)]
        
        generalLineChartDataSet.circleRadius = 3.0
        generalLineChartDataSet.circleColors =  [UIColor(red: 104/255, green: 159/255, blue: 56/255, alpha: 1)]
        generalLineChartDataSet.colors =  [UIColor(red: 104/255, green: 159/255, blue: 56/255, alpha: 1)]
    }
    
    //pie chart
    func setChart(dataPoints: [String], preMealData: [Double], postMealData: [Double]) {
        let pieColor:[UIColor] = [ UIColor(red: 219/255, green: 68/255, blue: 55/255, alpha: 1),UIColor(red: 104/255, green: 159/255, blue: 56/255, alpha: 1),UIColor(red: 253/255, green: 216/255, blue: 53/255, alpha: 1)]
        
        var preMealDataCount:Int64 = 0
        var postMealDataCount:Int64 = 0
        
        var preMealDataEntries: [PieChartDataEntry] = []
        var postMealDataEntries: [PieChartDataEntry] = []
        
        
        for i in 0..<dataPoints.count {
            let preMealDataEntry = PieChartDataEntry(value: preMealData[i], label: dataPoints[i], data: nil)
            preMealDataEntries.append(preMealDataEntry)
            
            let postMealDataEntry = PieChartDataEntry(value: postMealData[i], label: dataPoints[i], data: nil)
            postMealDataEntries.append(postMealDataEntry)
            
            preMealDataCount = preMealDataCount + Int64(preMealData[i])
            postMealDataCount = postMealDataCount + Int64(postMealData[i])
            
        }
        
        let preMealChartDataSet = PieChartDataSet(entries: preMealDataEntries, label: "")
        let preMealChartData = PieChartData(dataSet: preMealChartDataSet)
        
        let postMealChartDataSet = PieChartDataSet(entries: postMealDataEntries, label: "")
        let postMealChartData = PieChartData(dataSet: postMealChartDataSet)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        preMealChartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        postMealChartData.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        
        
        
        if(preMealDataCount > 0){
            preMealChartView.data = preMealChartData
        }
        else{
            preMealChartView.data = nil
        }
        if(postMealDataCount > 0){
            postMealChartView.data = postMealChartData
        }
        else{
             postMealChartView.data = nil
        }
        
        preMealChartDataSet.colors = pieColor
        postMealChartDataSet.colors = pieColor
        
    }

    //設定要顯示之圓餅圖 及 按鈕
    func setPieVisable(preMealVisable: Bool,postMealVisable: Bool){
        preMealUILabel.isHidden = !preMealVisable
        preMealChartView.isHidden = !preMealVisable
        postMealUILabel.isHidden = !postMealVisable
        postMealChartView.isHidden = !postMealVisable
        preMealUIButton.isHidden = !preMealVisable
        postMealUIButton.isHidden = !postMealVisable
    }
    

    
    func setTimePeriod(){
        let dateSt:Int64 = getTimestamp(mode: "startTime")
        let dateStMax:Int64 = getTimestamp(mode: "endTime")
        
        self.lineChartView.xAxis.axisMinimum = Double(dateSt)
        self.lineChartView.xAxis.axisMaximum = Double(dateStMax)
        
        setTimeRange(startTime: dateSt, endTime: dateStMax)
    }
    
    //設定時間
    func getTimestamp(mode: String)->Int64{
        var result:Int64 = 0
        switch mode {
        case "startTime":
            if(glucose.count>0){
                if(glucose.count == 1){
                    result = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[0].createDate!) - 125
                }
                else{
                    let start = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[0].createDate!)
                    let end = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[glucose.count-1].createDate!)
                    
                    result = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[0].createDate!)
                    
                    if((end-start)<250){
                        result = result - 125
                    }
                }
            }
            else{
                result = TimeData.sharedInstance.timestampShift(shiftMonth: -1)
            }
            break
        case "endTime":
            if(glucose.count>0){
                if(glucose.count == 1){
                     result = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[0].createDate!) + 125
                }
                else{
                    let start = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[0].createDate!)
                    let end = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[glucose.count-1].createDate!)
                    
                    result = TimeData.sharedInstance.stringToTimestamp(timeString: self.glucose[glucose.count-1].createDate!)
                    
                    if((end-start)<250){
                        result = result + 125
                    }
                }
            }
            else{
                result = TimeData.sharedInstance.timestampShift(shiftMonth: 0)
            }
            break
        default:
            break
        }
        
        return result
    }
    
    //重新載入圖表
    @objc func reloadAction(){
        print("reloadAction")
        setSpeciesButten(currentSpecies: UserGetSetData.PetSpecies)
        setClick(sender: allDateUIButton)
        
        loadDbData()
        dataSeletUser()
        dataSeletPet()
        setDbData(period: false,periodtime: 0)
        
        setTimePeriod()
        
        updateChartWithData(mode: setSpecies(currentSpecies: UserGetSetData.PetSpecies),postMealTime: postMealTime,preMealTime: preMealTime,generalTime: generalTime, postMealData: postMealData,preMealData: preMealData,generalData: generalData)
        
        setChart(dataPoints: pieArray, preMealData: preMealPieData,postMealData: postMealPieData)
        
        setChartsChange()
    }
    
    //設定導航列
    func setNavigationItem(bleImageString: String){
        
//        let reloadImage = UIImage(named: "button_reload")?.withRenderingMode(.alwaysOriginal)
//        let reloadBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
//        reloadBtn.setImage(reloadImage, for: UIControl.State.normal)
//        reloadBtn.addTarget(self, action: #selector(reloadAction), for: UIControl.Event.touchUpInside)
//        reloadBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
//        let reloadUIbutton = UIBarButtonItem(customView: reloadBtn)

//        let bleImage = UIImage(named: "\(bleImageString)")?.withRenderingMode(.alwaysOriginal)
//        let bleBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
//        bleBtn.setImage(bleImage, for: UIControl.State.normal)
//        bleBtn.addTarget(self, action: #selector(bleAction), for: UIControl.Event.touchUpInside)
//        bleBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
//        let bleUIbutton = UIBarButtonItem(customView: bleBtn)
        
        let recordAddImage = UIImage(named: "record_add")?.withRenderingMode(.alwaysOriginal)
        let recrodAddBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        recrodAddBtn.setImage(recordAddImage, for: UIControl.State.normal)
        recrodAddBtn.addTarget(self, action: #selector(recordAddAction), for: UIControl.Event.touchUpInside)
        recrodAddBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let recordAddUIbutton = UIBarButtonItem(customView: recrodAddBtn)
        
        navigationItem.setLeftBarButtonItems([recordAddUIbutton], animated: false)
        
        let gridImage = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        let gridBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        gridBtn.setImage(gridImage, for: UIControl.State.normal)
        gridBtn.addTarget(self, action: #selector(gridAction), for: UIControl.Event.touchUpInside)
        gridBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let gridUIbutton = UIBarButtonItem(customView: gridBtn)
        
        navigationItem.setRightBarButtonItems([gridUIbutton], animated: false)
    }
    
    @objc func menuBarSelectorReceived(notification: NSNotification) {
        let selector = notification.object as! String
        switch selector {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break
        
        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break
 
        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break
            
        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break
            
        default:
            print(selector + " no handle")
            break
        }
    }
    
    @objc func selectPetAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.fluid(percentage: 0.80)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = false
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base",bundle:nil)
        let popupViewController = storyboard.instantiateViewController(withIdentifier: "selectPetViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
        
    }
    
    @objc func recordAddAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 1.00)
            let height = ModalSize.fluid(percentage: 1.00)
            let customType = PresentationType.custom(width: width, height: height, center: .center)

            let presenter = Presentr(presentationType: customType)

            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Record", bundle: nil)
        let recordAddViewController = storyboard.instantiateViewController(withIdentifier: "RecordAddViewController")

        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: recordAddViewController, animated: false, completion: nil)
        
//        let storyboard = UIStoryboard(name: "Record", bundle: nil)
//        let addRecrodViewController = storyboard.instantiateViewController(withIdentifier: "AddRecordViewController")
//        self.present(addRecrodViewController, animated: false, completion: nil)
    }
    
    @objc func gridAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.35)
            let height = ModalSize.fluid(percentage: 0.38)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 250, y: 70))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base", bundle: nil)
        let gridViewController = storyboard.instantiateViewController(withIdentifier: "menuTableViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: gridViewController, animated: false, completion: nil)
    }
    
    //藍牙模組
    @objc func bleAction(){
//        if(BleFunction.connectFlag == true){
//            EZLoadingActivity.show("Loading...", disableUI: true)
//
//            let presenter: Presentr = {
//                let width = ModalSize.fluid(percentage: 0.90)
//                let height = ModalSize.fluid(percentage: 0.50)
//                let customType = PresentationType.custom(width: width, height: height, center: .center)
//
//                let presenter = Presentr(presentationType: customType)
//
//                presenter.transitionType = TransitionType.coverHorizontalFromRight
//                presenter.dismissOnSwipe = true
//                return presenter
//            }()
//            let storyboard = UIStoryboard(name: "Record",bundle:nil)
//            let popupViewController: BleViewController = {
//                let popupViewController = storyboard.instantiateViewController(withIdentifier: "bleViewController")
//                return popupViewController as! BleViewController
//            }()
//
//            presenter.transitionType = nil
//            presenter.dismissTransitionType = nil
//            presenter.keyboardTranslationType = .compress
//            self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
//
//        }
//        else{
//            let presenter: Presentr = {
//                let width = ModalSize.fluid(percentage: 0.90)
//                let height = ModalSize.fluid(percentage: 0.70)
//                let customType = PresentationType.custom(width: width, height: height, center: .center)
//
//                let presenter = Presentr(presentationType: customType)
//
//                presenter.transitionType = TransitionType.coverHorizontalFromRight
//                presenter.dismissOnSwipe = true
//                return presenter
//            }()
//            let storyboard = UIStoryboard(name: "Record",bundle:nil)
//            let popupViewController: ScanBleViewController = {
//                let popupViewController = storyboard.instantiateViewController(withIdentifier: "ScanBleViewController")
//                return popupViewController as! ScanBleViewController
//            }()
//
//            presenter.transitionType = nil
//            presenter.dismissTransitionType = nil
//            presenter.keyboardTranslationType = .compress
//            self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
//        }
    }
    
    //偵測ble Meter連線狀況
    @objc func monitorDisconnection() {
        print("GlucoseViewController monitorDisconnection")
//        guard BleFunction.scannedPeripheral != nil else { return }
//
//        BleFunction.manager.monitorDisconnection(for: BleFunction.scannedPeripheral.peripheral)
//            .subscribe(onNext: { [weak self] (peripheral) in
//                self?.setDisconnect()
//            }).addDisposableTo(disposeBag)
    }
    
    //ble disconnect
    func setDisconnect(){
        print("GlucoseViewController setDisconnect")
//        BleFunction.manager.cancelPeripheralConnection(BleFunction.scannedPeripheral.peripheral)
//            .subscribe(onNext: { [weak self] (peripheral) in
//
//            }).addDisposableTo(disposeBag)
//        BleFunction.connectFlag = false
//        setImageBleDisconnect()
    }
    
    @objc func setImageBleConnect(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            self.setNavigationItem(bleImageString: "bluetooth_blue")
        }
       
    }
    
    @objc func setImageBleDisconnect(){
        setNavigationItem(bleImageString: "bluetooth_gray")
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
}

struct menuBarNotification {
    static let message = "message"
}

// MARK: axisFormatDelegate
extension GlucoseViewController: IAxisValueFormatter {
    
    // x軸數值format
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
}
