//
//  VerifySNViewController.swift
//  PetTest
//
//  Created by Victor on 2019/7/24.
//  Copyright © 2019 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import CoreData

class VerifySNViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    let userDefults = UserDefaults.standard
    
    @IBOutlet weak var verifySNTextFeild: UITextField!
    @IBOutlet weak var EnterBtn: UIButton!
    
    let accountText = "asdf@b.c"
    let passwordText = "mickey7656"
    let serailNumber = "17D1100202924"
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    
    var deviceSelect :Int = 0
    
    var count: CountModel!
    var countList: [CountModel] = []
    var fetchResultController: NSFetchedResultsController<CountModel>!
    
    
    var pet:[PetModel] = []
    var petFetchResultController: NSFetchedResultsController<PetModel>!
    
    override func viewDidLoad() {
        super .viewDidLoad()
        
        getVerifySNValue()
        initialNavigation()
        settingInit()
        databaseInit()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    private func initialNavigation() {
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    private func settingInit(){
        if userDefults.value(forKey: "autoLogin") != nil {
            
        }
        else{
            userDefults.set("false", forKey: "autoLogin")
            userDefults.set("false", forKey: "autoSync")
            userDefults.set("false", forKey: "autoSyncNoInternet")
            userDefults.set("true", forKey: "waterReminder")
            userDefults.set("true", forKey: "medicineReminder")
            userDefults.set("true", forKey: "clinicReminder")
            
        }
    }
    
    func databaseInit(){
        loadDbData()
        
        if(countList.count == 0){
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                count = CountModel(context: appDelegate.persistentContainer.viewContext)
                count.bloodCount = 0
                count.pressCount = 0
                count.temperatureCount = 0
                count.reminderCount = 0
                appDelegate.saveContext()
            }
        }
    }
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    countList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    @IBAction func verifyEnterAction(sender: UIButton) {
        if sender == EnterBtn {
            
            EZLoadingActivity.show("Loading...", disableUI: true)
            
            if(checkout() != "OK"){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    EZLoadingActivity.Settings.FailText = "\(self.checkout())"
                    EZLoadingActivity.hide(false, animated: false)
                }
                return
            }
            
            if Reachability.isConnectedToNetwork() == true {
                
                self.cellSoapService()
                
                let queue = DispatchQueue(label:"Verify SN",qos: DispatchQoS.userInitiated)
                
                queue.async {
                    var i = 0
                    let timeOut = 20
                    repeat{
                        Thread.sleep(forTimeInterval: 1)
                        i = i+1
                    }while(!self.soapConnect && i < timeOut)
                    
                    self.checkLogIn()
                }
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                    EZLoadingActivity.hide(false, animated: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                        self.showOfflineAlert()
                    }
                }
                
            }
        }
    }
    
    private func checkout() -> String{
        let result = "OK";
//        if (verifySNTextFeild.text?.isEmpty)! {
//            result = "Serial Number is required"
//        } else {
//            if verifySNTextFeild.text != serailNumber  {
//                result = "Wrong serial Nubmer!"
//            }
//        }
        return result
    }
    
    private func showOfflineAlert() {
        let offlineAlertController = UIAlertController(title: nil, message: "Offline login model", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.soapConnect = false
            self.soapResult = "true"
            self.offlineLogin()
        }
        let noAction = UIAlertAction(title: "No", style: .cancel) { (_) in
            offlineAlertController.dismiss(animated: false, completion: nil)
        }
        offlineAlertController.addAction(yesAction)
//        offlineAlertController.addAction(noAction)
        present(offlineAlertController, animated: true, completion: nil)
    }
    
    private func offlineLogin() {
        let offlineJson: JSON = [
            "status" : "true",
            "Pet_Name" : "aa",
            "Pet_Species" : "Canine",
            "Pet_Diabetes_Year" : "",
            "GlucoseUnit" : "1",
            "country" : "Your Conutry",
            "id" : "261",
            "email" : "asdf@b.c",
            "firstname" : "allasp",
            "weightUnit" : "0",
            "PreMeal_Lower" : "50",
            "region" : "Your State",
            "Pet_Diabetes_Type" : "na",
            "PostMeal_Upper" : "111",
            "App_Use" : "0",
            "Pet_Birth" : "",
            "DiabetesType" : "na",
            "Pet_App_Use" : "1",
            "Pet_Breed" : "",
            "message" : "login success",
            "Pet_Gender" : "",
            "gender" : "",
            "username" : "soxp",
            "weight" : "50",
            "city" : "Your City",
            "PostMeal_Lower" : "50",
            "PreMeal_Upper" : "111"
        ]
        print(offlineJson)
        self.setPersonal(json: offlineJson)
        self.setPetData(json: offlineJson)
        self.performSegue(withIdentifier: "verifySuccess", sender: self)
    }
 
    private func cellSoapService() {
        
        let soapFunc = "loginPetSelect"
        let data:NSDictionary = [
            "appId": "1",
            "password": "\(passwordText)",
            "username": "\(accountText)"
        ]
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.soapConnect = status
            self.soapResult = self.json["status"].stringValue
        }
    }
    
    private func checkLogIn(){
        if(self.soapConnect){
            print("YES")
            if(self.soapResult == "true") {
                DispatchQueue.main.async {
                    self.setVerifySNValue(serialNumber: self.verifySNTextFeild.text!, device: "Glucose")
                    self.setPersonal(json: self.json)
                    EZLoadingActivity.Settings.SuccessText = "Verify Success"
                    EZLoadingActivity.hide(true, animated: false)
                    self.setPetData(json: self.json)
                    self.performSegue(withIdentifier: "verifySuccess", sender: self)
                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "Verify Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                self.showOfflineAlert()
            }
        }
        self.soapConnect = false
    }
    
    private func setVerifySNValue(serialNumber: String, device: String){
        userDefults.set(serialNumber, forKey: "serialNumber")
        userDefults.set(device, forKey: "device")
    }
    
    private func setPersonal(json: JSON){
        UserGetSetData.Id = json["id"].stringValue
        UserGetSetData.FristName = json["firstname"].stringValue
        UserGetSetData.LastName = json["username"].stringValue
        UserGetSetData.Email = json["email"].stringValue
        UserGetSetData.Password =  self.passwordText
        
        UserGetSetData.Country = json["country"].stringValue
        UserGetSetData.Region = json["region"].stringValue
        UserGetSetData.City = json["city"].stringValue
        
        UserGetSetData.Gender = json["gender"].stringValue
        UserGetSetData.WeightUnit = json["weightUnit"].stringValue
        UserGetSetData.Weight = json["weight"].stringValue
        
        UserGetSetData.GlucoseUnit = json["GlucoseUnit"].stringValue
        UserGetSetData.DiabetesType = json["DiabetesType"].stringValue
        UserGetSetData.PreMealUpper = json["PreMeal_Upper"].stringValue
        UserGetSetData.PreMealLower = json["PreMeal_Lower"].stringValue
        UserGetSetData.PostMealUpper = json["PostMeal_Upper"].stringValue
        UserGetSetData.PostMealLower = json["PostMeal_Lower"].stringValue
        
        UserGetSetData.PetName = json["Pet_Name"].stringValue
        UserGetSetData.PetSpecies = json["Pet_Species"].stringValue
        UserGetSetData.PetBreed = json["Pet_Breed"].stringValue
        UserGetSetData.PetWeight = json["weight"].stringValue
        UserGetSetData.PetDiabetesType = json["Pet_Diabetes_Type"].stringValue
        UserGetSetData.PetDiabetesYear = json["Pet_Diabetes_Year"].stringValue
        UserGetSetData.PetGender = json["Pet_Gender"].stringValue
        UserGetSetData.PetBirth = json["Pet_Birth"].stringValue
        
        UserGetSetData.PetAppUse = json["Pet_App_Use"].stringValue
        UserGetSetData.AppUSE = json["App_Use"].stringValue
        
        UserGetSetData.Device_Status = self.deviceSelect
        UserGetSetData.Login_Status = true
    }
    
    private func getVerifySNValue(){
        if userDefults.value(forKey: "serialNumber") != nil {
            self.verifySNTextFeild.text = userDefults.value(forKey: "serialNumber") as! String?
            self.deviceSelect = setDeviceSelect(Device: "Glucose")
        }
        self.verifyEnterAction(sender: EnterBtn)
    }
    
    private func setDeviceSelect(Device: String)->Int{
        var result = 0
        
        switch Device {
        case "Glucose":
            result = 0
        case "BloodPressure":
            result = 1
        case "Temperature":
            result = 2
        default:
            break
        }
        return result
    }
    
    private func setPetData(json: JSON){
        let fethRequest :NSFetchRequest<PetModel> = PetModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "petName",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        DispatchQueue.main.async {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                self.petFetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
                self.petFetchResultController.delegate = self
                
                do{
                    try self.petFetchResultController.performFetch()
                    if let fetchedObjects = self.petFetchResultController.fetchedObjects{
                        self.pet = fetchedObjects
                    }
                }
                catch{
                    print(error)
                }
            }
            self.pet = self.pet.filter{ $0.userId == UserGetSetData.Id}
            if self.pet.count == 0 {
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    let petItem = PetModel(context: appDelegate.persistentContainer.viewContext)
                    petItem.userId = json["id"].stringValue
                    petItem.petName = json["Pet_Name"].stringValue
                    petItem.petSpecies = json["Pet_Species"].stringValue
                    petItem.petBreed = json["Pet_Breed"].stringValue
                    petItem.petWeight = Float(Int64(json["weight"].stringValue)!)
                    petItem.petDiabetesType = json["Pet_Diabetes_Type"].stringValue
                    petItem.petDiabetesYear = json["Pet_Diabetes_Year"].stringValue
                    petItem.petGender = json["Pet_Gender"].stringValue
                    petItem.petBirth = json["Pet_Birth"].stringValue
                    petItem.petGlucoseLower = Int64(json["PreMeal_Lower"].stringValue)!
                    petItem.petGlucoseUpper = Int64(json["PreMeal_Upper"].stringValue)!
                    petItem.status = 1
                    appDelegate.saveContext()
                }
            } else {
                self.setCurrentPet()
            }
        }
    }
    
    private func setCurrentPet(){
        self.dataSeletPet()
        let petItem  = pet[pet.count-1]
        
        UserGetSetData.PetName = petItem.petName!
        UserGetSetData.PetSpecies = petItem.petSpecies!
        UserGetSetData.PetBreed = petItem.petBreed!
        UserGetSetData.Weight = "\(petItem.petWeight)"
        UserGetSetData.PetWeight = "\(petItem.petWeight)"
        UserGetSetData.PetDiabetesType =  petItem.petDiabetesType!
        UserGetSetData.PetDiabetesYear = petItem.petDiabetesYear!
        UserGetSetData.PetGender = petItem.petGender!
        UserGetSetData.PetBirth = petItem.petBirth!
        UserGetSetData.PreMealUpper = "\(petItem.petGlucoseUpper)"
        UserGetSetData.PreMealLower = "\(petItem.petGlucoseLower)"
        UserGetSetData.PostMealUpper = "\(petItem.petGlucoseUpper)"
        UserGetSetData.PostMealLower = "\(petItem.petGlucoseLower)"
    }
    
    private func dataSeletPet(){
        //只留有指定使用者資料
        pet = pet.filter{ $0.status == 1}
    }
}
