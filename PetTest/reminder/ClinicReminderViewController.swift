//
//  ClinicReminderViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/20.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import DatePickerDialog
import UserNotifications
import CoreData

class ClinicReminderViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var selectDayButton: UIButton!
    @IBOutlet weak var selectTimeButton: UIButton!
    
    @IBOutlet weak var beforeTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextField!
    
    //DB
    var reminder: ReminderModel!
    var countList: [CountModel] = []
    var fetchResultController: NSFetchedResultsController<CountModel>!
    
    let reminderType = "clinic"
    
    let before = ["5 min","10 min","15 min","30 min","45 min","60 min"]
    
    let type = ["notification","notification + sound","none"]
    
    
    
    var noteDate:Date = Date()
    let userDefults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(Reminder.reminderItem != nil){
            let reminderItem = Reminder.reminderItem
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: (reminderItem?.createDate)!)
            
            noteDate = date!
            contentTextField.text = reminderItem?.content
            typeTextField.text = reminderItem?.remindType
            beforeTextField.text = reminderItem?.before
        }
        
        viewInit()
    }

    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        //nabigation Button
        let saveImage = UIImage(named: "button_ok")?.withRenderingMode(.alwaysOriginal)
        let saveBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        saveBtn.setImage(saveImage, for: UIControl.State.normal)
        saveBtn.addTarget(self, action: #selector(saveAction), for: UIControl.Event.touchUpInside)
        saveBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let saveUIbutton = UIBarButtonItem(customView: saveBtn)
        
        let deleteImage = UIImage(named: "button_delete")?.withRenderingMode(.alwaysOriginal)
        let deleteBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        deleteBtn.setImage(deleteImage, for: UIControl.State.normal)
        deleteBtn.addTarget(self, action: #selector(deleteData), for: UIControl.Event.touchUpInside)
        deleteBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let deleteUIbutton = UIBarButtonItem(customView: deleteBtn)
        
        var barButtonItem: [UIBarButtonItem]  = [saveUIbutton]
        
        if(Reminder.reminderItem != nil){
            barButtonItem = [saveUIbutton,deleteUIbutton]
        }
        
        navigationItem.setRightBarButtonItems(barButtonItem, animated: false)
        
        
        //init time
        selectDayButton.setTitle(TimeData.sharedInstance.setDayLabelDatabase(data: noteDate), for: .normal)
        selectTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: noteDate), for: .normal)
        
        //init spinner
        let beforePickerView = UIPickerView()
        beforePickerView.delegate = self
        beforePickerView.tag = 1
        beforeTextField.inputView = beforePickerView
        
        let typePickerView = UIPickerView()
        typePickerView.delegate = self
        typePickerView.tag = 2
        typeTextField.inputView = typePickerView

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func actionSend(sender: UIButton){
        if(sender == selectDayButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .date) { (date) in
                if let dateCheck = date {
                    self.selectDayButton.setTitle(TimeData.sharedInstance.setDayLabelDatabase(data: dateCheck), for: .normal)
                    self.noteDate = TimeData.sharedInstance.setDay(data: self.noteDate,dateChange: dateCheck)
                }
            }

        }
        else if(sender == selectTimeButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .time) { (date) in
                if let dateCheck = date {
                    self.selectTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                    self.noteDate = TimeData.sharedInstance.setTime(data: self.noteDate,dateChange: dateCheck)
                }
            }

        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return before.count
        }
        if pickerView.tag == 2 {
            return type.count
        }
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            beforeTextField.text =  before[0]
            return before[row]
        }
        if pickerView.tag == 2 {
            typeTextField.text =  type[0]
            return type[row]
        }
        
        return nil
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            beforeTextField.text =  before[row]
        }
        if pickerView.tag == 2 {
            typeTextField.text =  type[row]
        }
    }

    

    
    @objc func saveAction(){
        let contentString = contentTextField.text!
        let alertType = typeTextField.text!
        let timeString = TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
        let beforeArray = beforeTextField.text!.components(separatedBy: " ")
        var identifier = ""
        
        let time = Int(TimeData.sharedInstance.stringToTimestamp(timeString: timeString)) - Int(TimeData.sharedInstance.timestampShift(date: Date())) - TimeData.sharedInstance.getTimeInterval(time: Int(beforeArray[0])!,type: beforeArray[1])
        
        if(Reminder.reminderItem != nil){
            Reminder.reminderItem?.createDate = TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
            Reminder.reminderItem?.content = contentTextField.text!
            Reminder.reminderItem?.remindType = typeTextField.text!
            Reminder.reminderItem?.before = beforeTextField.text!
            Reminder.reminderItem?.startDate =  TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
            
            identifier = "\((Reminder.reminderItem?.notificationId)!)"
        }
        else{
            loadDbData()
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                reminder = ReminderModel(context: appDelegate.persistentContainer.viewContext)
                reminder.id = countList[0].reminderCount
                reminder.type = "clinic"
                reminder.createDate = TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
                reminder.content = contentTextField.text!
                reminder.remindType = typeTextField.text!
                reminder.before =  beforeTextField.text!
                reminder.startDate =  TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
                reminder.notificationId = "\(countList[0].reminderCount).0"
                
                countList[0].reminderCount = countList[0].reminderCount + 1
                appDelegate.saveContext()
            }
            identifier = "\(countList[0].reminderCount - 1).0"
        }
        
        print(time)
        
        Notification.sharedInstance.removeNotification(identifier: identifier)
        
        if(time > 0 && typeTextField.text != "none" && userDefults.value(forKey: "clinicReminder") as! String? == "true"){
            Notification.sharedInstance.addNotification(type: reminderType, identifier: identifier, contentString: contentString,alertType: alertType, timeInterval: TimeInterval(time))
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadReminder"), object: nil)
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func deleteData(){
        if(Reminder.reminderItem != nil){
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                context.delete(Reminder.reminderItem!)
                appDelegate.saveContext()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadReminder"), object: nil)
            }
            Notification.sharedInstance.removeNotification(identifier: "\(String(describing: Reminder.reminderItem?.notificationId))")
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    countList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }


}
