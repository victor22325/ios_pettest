//
//  MedicineTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/23.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class MedicineTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadMedicine), name: NSNotification.Name(rawValue: "reloadMedicineTable"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeMedicineTableViewController"), object: nil)
        
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Reminder.medicineTime.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! MedicineTableViewCell
        
        cell.timeLable.text = Reminder.medicineTime[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        Reminder.medicineTime.remove(at: indexPath.row)
        reloadMedicine()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSelectHeight"), object: nil)
    }
    
    @objc func reloadMedicine(){
        self.tableView.reloadData()
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
}
