//
//  WaterReminderViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/21.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import DatePickerDialog
import UserNotifications
import CoreData
import EZLoadingActivity
import IHKeyboardAvoiding

class WaterReminderViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var startTimeButton: UIButton!
    @IBOutlet weak var endTimeButton: UIButton!
    
    @IBOutlet weak var beforeTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var contentTextField: UITextField!
    
    @IBOutlet weak var avoidingView: UIView!
    
    //DB
    var reminder: ReminderModel!
    var countList: [CountModel] = []
    var fetchResultController: NSFetchedResultsController<CountModel>!
    
    let reminderType = "water"
    
    let before = ["10 min","30 min","60 min","120 min","180 min"]
    
    let type = ["notification","notification + sound","none"]
    
    
    var noteDate:Date = Date()
    let userDefults = UserDefaults.standard
    var dateArray:[Date] = []
    var startTime = "08:30"
    var endTime = "17:00"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(Reminder.reminderItem != nil){
            let reminderItem = Reminder.reminderItem
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: (reminderItem?.createDate)!)
            
            noteDate = date!
            contentTextField.text = reminderItem?.content
            typeTextField.text = reminderItem?.remindType
            beforeTextField.text = reminderItem?.before
            
            let timesplite = (Reminder.reminderItem?.startDate?.components(separatedBy: "-"))!
            
            startTimeButton.setTitle(timesplite[0], for: .normal)
            endTimeButton.setTitle(timesplite[1], for: .normal)
            startTime = timesplite[0]
            endTime = timesplite[1]
        }
        
        viewInit()
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        //nabigation Button
        let saveImage = UIImage(named: "button_ok")?.withRenderingMode(.alwaysOriginal)
        let saveBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        saveBtn.setImage(saveImage, for: UIControl.State.normal)
        saveBtn.addTarget(self, action: #selector(saveAction), for: UIControl.Event.touchUpInside)
        saveBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let saveUIbutton = UIBarButtonItem(customView: saveBtn)
        
        let deleteImage = UIImage(named: "button_delete")?.withRenderingMode(.alwaysOriginal)
        let deleteBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        deleteBtn.setImage(deleteImage, for: UIControl.State.normal)
        deleteBtn.addTarget(self, action: #selector(deleteData), for: UIControl.Event.touchUpInside)
        deleteBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let deleteUIbutton = UIBarButtonItem(customView: deleteBtn)
        
        var barButtonItem: [UIBarButtonItem]  = [saveUIbutton]
        
        if(Reminder.reminderItem != nil){
            barButtonItem = [saveUIbutton,deleteUIbutton]
        }
        
        navigationItem.setRightBarButtonItems(barButtonItem, animated: false)
        
        
        //init spinner
        let beforePickerView = UIPickerView()
        beforePickerView.delegate = self
        beforePickerView.tag = 1
        beforeTextField.inputView = beforePickerView
        
        let typePickerView = UIPickerView()
        typePickerView.delegate = self
        typePickerView.tag = 2
        typeTextField.inputView = typePickerView
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //設定鍵盤擋住輸入視窗事件
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        KeyboardAvoiding.avoidingView = textField
        return true
    }
    
    @IBAction func actionSend(sender: UIButton){
        if(sender == startTimeButton || sender == endTimeButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .time) { (date) in
                if let dateCheck = date {
                    if(sender == self.startTimeButton){
                        
                        if(self.checkTimeRange(startTime: TimeData.sharedInstance.setTimeLabel(data: dateCheck),endTime: self.endTime)){
                            self.startTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                            self.startTime = TimeData.sharedInstance.setTimeLabel(data: dateCheck)
                        }
                        else{
                            self.startTimeButton.setTitle(self.endTime, for: .normal)
                            self.startTime = self.endTime
                            self.endTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                            self.endTime = TimeData.sharedInstance.setTimeLabel(data: dateCheck)
                        }
                    }
                    else{
                        
                        if(self.checkTimeRange(startTime: self.startTime,endTime: TimeData.sharedInstance.setTimeLabel(data: dateCheck))){
                            self.endTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                            self.endTime = TimeData.sharedInstance.setTimeLabel(data: dateCheck)
                        }
                        else{
                            self.endTimeButton.setTitle(self.startTime, for: .normal)
                            self.endTime = self.startTime
                            self.startTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                            self.startTime = TimeData.sharedInstance.setTimeLabel(data: dateCheck)
                        }

                    }
                }
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return before.count
        }
        if pickerView.tag == 2 {
            return type.count
        }
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            beforeTextField.text =  before[0]
            return before[row]
        }
        if pickerView.tag == 2 {
            typeTextField.text =  type[0]
            return type[row]
        }
        
        return nil
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            beforeTextField.text =  before[row]
        }
        if pickerView.tag == 2 {
            typeTextField.text =  type[row]
        }
    }
    
    
    
    
    @objc func saveAction(){
        let contentString = contentTextField.text!
        let alertType = typeTextField.text!
        let frequencyArray = beforeTextField.text!.components(separatedBy: " ")
        var identifier = ""
        dateArray = TimeData.sharedInstance.setTimeInterval(startTime: startTime,endTime: endTime,timeInterval: Int(frequencyArray[0])!)
        
        if(Reminder.reminderItem != nil){
            let identifierArray = (Reminder.reminderItem?.notificationId?.components(separatedBy: "-"))!
            
            for i in 0 ..< identifierArray.count{
                let delayInSeconds = Double(i) / 5.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    Notification.sharedInstance.removeNotification(identifier: identifierArray[i])
                }
            }
            
            Reminder.reminderItem?.createDate = TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
            Reminder.reminderItem?.content = contentTextField.text!
            Reminder.reminderItem?.remindType = typeTextField.text!
            Reminder.reminderItem?.before = beforeTextField.text!
            Reminder.reminderItem?.startDate = "\(startTime)-\(endTime)"
            Reminder.reminderItem?.notificationId = setIdentifier(id: (Reminder.reminderItem?.id)!,dateArray: dateArray)
            
            identifier = "\((Reminder.reminderItem?.id)!)"
        }
        else{
            loadDbData()
            
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                reminder = ReminderModel(context: appDelegate.persistentContainer.viewContext)
                reminder.id = countList[0].reminderCount
                reminder.type = "water"
                reminder.createDate = TimeData.sharedInstance.setDayTimeFormat(data: noteDate)
                reminder.content = contentTextField.text!
                reminder.remindType = typeTextField.text!
                reminder.before =  beforeTextField.text!
                reminder.startDate = "\(startTime)-\(endTime)"
                reminder.notificationId = setIdentifier(id: countList[0].reminderCount,dateArray: dateArray)
                
                countList[0].reminderCount = countList[0].reminderCount + 1
                appDelegate.saveContext()
            }
            identifier = "\(countList[0].reminderCount - 1)"
        }
        
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if(typeTextField.text != "none" && userDefults.value(forKey: "waterReminder") as! String? == "true"){
            for i in 0 ..< dateArray.count{
                
                let delayInSeconds = Double(i) / 5.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                     Notification.sharedInstance.addNotificationRepeat(type: self.reminderType, identifier: "\(identifier).\(i)", contentString: contentString,alertType: alertType,date: self.dateArray[i])
                }
            }
        }
        
        var delayInSeconds = 0.0
        if(typeTextField.text != "none"){
            delayInSeconds = Double(dateArray.count) / 5.0
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            EZLoadingActivity.Settings.SuccessText = "Success"
            EZLoadingActivity.hide(true, animated: false)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadReminder"), object: nil)
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func deleteData(){
        if(Reminder.reminderItem != nil){
            EZLoadingActivity.show("Loading...", disableUI: true)
            let identifierArray = (Reminder.reminderItem?.notificationId?.components(separatedBy: "-"))!
            
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                context.delete(Reminder.reminderItem!)
                appDelegate.saveContext()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadReminder"), object: nil)
            }
            
            for i in 0 ..< identifierArray.count{
                let delayInSeconds = Double(i) / 5.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    print(identifierArray[i])
                    Notification.sharedInstance.removeNotification(identifier: identifierArray[i])
                }
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(identifierArray.count/5)) {
                EZLoadingActivity.Settings.SuccessText = "Success"
                EZLoadingActivity.hide(true, animated: false)
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    countList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    func setIdentifier(id: Int64,dateArray: [Date])->String{
        var resultString = ""
        
        for i in 0 ..< dateArray.count{
            resultString = resultString + "\(id).\(i)"
            if(i != dateArray.count-1){
                resultString = resultString + "-"
            }
        }
        return resultString
    }
    
    func checkTimeRange(startTime: String,endTime: String)->Bool{
        let startTimeArray = startTime.components(separatedBy: ":")
        let endTimeArray = endTime.components(separatedBy: ":")
        let startTimeInt:Int = Int(startTimeArray[0])!*60 + Int(startTimeArray[1])!
        let endTimeInt:Int = Int(endTimeArray[0])!*60 + Int(endTimeArray[1])!
        
        let checkTime = endTimeInt - startTimeInt
        
        if(checkTime > 0){
            return true
        }
        else{
            return false
        }
    
    }
    
}
