//
//  ReminderViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/20.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import CoreData
import Presentr

class ReminderViewController: UIViewController,NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var reminderTableView: UITableView!
    
//    let optionBar = OptionBar()
//    var optionsMenu: CAPSOptionsMenu?
    
    var reminder:[ReminderModel] = []
    var fetchResultController: NSFetchedResultsController<ReminderModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        viewInit()
//        optionsMenu = optionBar.addOptionsMenu(ViewController: self)
        
        loadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadReminder), name: NSNotification.Name(rawValue: "reloadReminder"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeReminderViewController"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(menuBarSelectorReceived), name: NSNotification.Name(rawValue: "menuBarSelector"), object: nil)
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let gridImage = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        let gridBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        gridBtn.setImage(gridImage, for: UIControl.State.normal)
        gridBtn.addTarget(self, action: #selector(gridAction), for: UIControl.Event.touchUpInside)
        gridBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let gridUIbutton = UIBarButtonItem(customView: gridBtn)
        
        navigationItem.setRightBarButtonItems([gridUIbutton], animated: false)
        
        reminderTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return reminder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! ReminderTableViewCell
        
        
        let reminderData = reminder[indexPath.row]
        
        cell.ReminderImageView.image = setImage(type: reminderData.type!)
        cell.ReminderContentLabel.text = reminderData.content
        cell.ReminderTimeLabel.text = setTimeString(type: reminderData.type!, time: reminderData.startDate!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        Reminder.reminderItem = reminder[indexPath.row]
        if(reminder[indexPath.row].type == "clinic"){
            self.performSegue(withIdentifier: "editClinicReminder", sender: self)
        }
        else if(reminder[indexPath.row].type == "medicine"){
            Reminder.medicineTime = (reminder[indexPath.row].startDate?.components(separatedBy: "-"))!
            
            if(!Reminder.medicineTime[0].contains(":")){
                Reminder.medicineTime = []
            }
            
            self.performSegue(withIdentifier: "editMedicineReminder", sender: self)
        }
        else if(reminder[indexPath.row].type == "water"){
            self.performSegue(withIdentifier: "editWaterReminder", sender: self)
        }
    }
    
    @IBAction func actionSend(sender: UIButton){
        if(sender == addButton){
            let alert = UIAlertController(title: "Select the reminder", message: "", preferredStyle: .alert)
            _ = UIAlertAction(title: "Water reminder", style: UIAlertAction.Style.default) {
                (action: UIAlertAction!) -> Void in
                Reminder.reminderItem = nil
                self.performSegue(withIdentifier: "editWaterReminder", sender: self)
            }
            let medinineAction = UIAlertAction(title: "Isulin/midicine Reminder", style: UIAlertAction.Style.default) {
                (action: UIAlertAction!) -> Void in
                Reminder.reminderItem = nil
                Reminder.medicineTime = []
                self.performSegue(withIdentifier: "editMedicineReminder", sender: self)
            }
            let clinicAction = UIAlertAction(title: "Veterinarian Reminder", style: UIAlertAction.Style.default) {
                (action: UIAlertAction!) -> Void in
                Reminder.reminderItem = nil
                self.performSegue(withIdentifier: "editClinicReminder", sender: self)
            }
            //取消喝水提醒
            //alert.addAction(waterAction)
            alert.addAction(medinineAction)
            alert.addAction(clinicAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<ReminderModel> = ReminderModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "id",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    reminder = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //設定reminder type 對應的 image
    func setImage(type: String)->UIImage{
        switch type {
        case "clinic":
            return  UIImage(named: "doctor")!
        case "medicine":
            return  UIImage(named: "medicine")!
        case "water":
            return  UIImage(named: "water")!
        default:
            return  UIImage(named: "doctor")!
        }
    }
    
    func setTimeString(type: String,time: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        
        let date = dateFormatter.date(from: time)
        switch type {
        case "clinic":
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
            return dateFormatter.string(from: date!)
        case "water":
            let timeSplit = time.components(separatedBy: "-")
            let stringFormatter = DateFormatter()
            
            stringFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
            
            dateFormatter.dateFormat = "HH:mm"
            stringFormatter.dateFormat = "hh:mm a"
           
            let startTime = stringFormatter.string(from: dateFormatter.date(from: timeSplit[0])!)
            let endTime = stringFormatter.string(from: dateFormatter.date(from: timeSplit[1])!)
            
            return "\(startTime) ~ \(endTime)"
        case "medicine":
            return "forever"
        default:
           return ""
        }
    
    }
    
    @objc func reloadReminder(){
        loadData()
        reminderTableView.reloadData()
    }
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func menuBarSelectorReceived(notification: NSNotification) {
        let selector = notification.object as! String
        switch selector {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break
            
        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break
            
        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break
            
        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break
            
        default:
            print(selector + " no handle")
            break
        }
    }
    
    @objc func gridAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.35)
            let height = ModalSize.fluid(percentage: 0.38)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 250, y: 70))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base", bundle: nil)
        let gridViewController = storyboard.instantiateViewController(withIdentifier: "menuTableViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: gridViewController, animated: false, completion: nil)
    }
}
