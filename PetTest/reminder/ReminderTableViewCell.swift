//
//  ReminderTableViewCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/21.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class ReminderTableViewCell: UITableViewCell {

    @IBOutlet weak var ReminderImageView: UIImageView!
    @IBOutlet weak var ReminderContentLabel: UILabel!
    @IBOutlet weak var ReminderTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
