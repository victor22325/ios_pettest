//
//  Globally.swift
//  advocate
//
//  Created by BMBMAC on 2017/4/27.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation


public struct UserGetSetData{
    static var Id=""
    static var FristName=""
    static var LastName=""
    static var Email=""
    static var Password=""
    
    static var Country=""
    static var Region=""
    static var City=""
    static var Zipcode=""
    
    static var Gender=""
    static var HeightUnit=""
    static var Height=""
    static var Heightinches=""
    static var WeightUnit=""
    static var Weight=""
    
    static var TemperatureUnit=""
    static var TemperatureType=""
    static var ForeheadUpper=""
    static var ForeheadLower=""
    static var AmbientUpper=""
    static var AmbientLower=""
    
    static var PressureUnit=""
    static var HypertensionType=""
    static var SystolicUpper=""
    static var SystolicLower=""
    static var DiastolicUpper=""
    static var DiastolicLower=""
    
    static var GlucoseUnit=""
    static var DiabetesType=""
    static var PreMealUpper=""
    static var PreMealLower=""
    static var PostMealUpper=""
    static var PostMealLower=""
    
    //pet 
    static var PetName = ""
    static var PetSpecies = ""
    static var PetBreed = ""
    static var PetWeight = ""
    static var PetDiabetesType = ""
    static var PetDiabetesYear = ""
    static var PetGender = ""
    static var PetBirth = ""
    static var PetGlucoseLower = ""
    static var PetGlucoseUpper = ""
    static var PetAppUse = ""
    static var AppUSE = ""
    
    var autoLogin=""
    var autoSync=""
    var backupToServer=""
    
    static var Device_Status = 0
    static var Login_Status = false
    static var User_logout = false
    
    var LoginStatus=false
    
}

public struct PetSpecies {
    static let other = 1
    static let canine = 2
    static let feline = 3
}


public struct Version{
    static var versionCode = "1"
    static var versionName = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    static var lastVersionName = ""
    static var lasetVersionCode = ""
    static var aboutProduct = "Product Name : Advocate PetTest"
    static var aboutVersion = "Version : v" + versionName
    static var aboutId = "2019 Advocate Corp All Rights Reserved"
    static var aboutWebsite = "Website : www.broadmaster-biotech.com"
    
}

public struct Comment{
    static var Id=""
    static var reloadFlag = false
}

public struct glucose{
    static var glucose: GlucoseModel? = nil
}






















