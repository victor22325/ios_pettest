//
//  CommentEditViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/11.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
import EZLoadingActivity
import SwiftyJSON

class CommentEditViewController: UIViewController {

    @IBOutlet weak var updateUIButton: UIButton!
    @IBOutlet weak var cancelUIButton: UIButton!
    @IBOutlet weak var editUITextField: UITextField!
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionButton(sender: UIButton) {
        if sender == updateUIButton {
            startSOAP()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
        }
        else if sender == cancelUIButton {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func startSOAP(){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellSoapService()
            
            let queue = DispatchQueue(label:"commentEdit",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 10
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.soapConnect && i < timeOut)
                
                self.checkLogIn()
            }
            
        } else {
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
            
            
        }
        
    }
    
    func cellSoapService() {
        
        let soapFunc = "updateComment"
        let data:NSDictionary = [
            "content": "\(editUITextField.text!)",
            "commentid": "\(Comment.Id)"
        ]
        
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.soapConnect = status
            self.soapResult = self.json["status"].string!
            
        }
    }
    
    func checkLogIn(){
        if(self.soapConnect){
            print("YES")
            if(self.soapResult == "true"){
                Comment.reloadFlag = true
            }
            else{
                EZLoadingActivity.Settings.FailText = "Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
        let delayInSeconds = 5.0
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
            self.dismiss(animated: true, completion: nil)
        }
        
    }

}

// MARK: - Presentr Delegate

extension CommentEditViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension CommentEditViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}



