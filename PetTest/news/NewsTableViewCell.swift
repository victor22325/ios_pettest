//
//  NewsTableViewCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/9.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet  var titleLable: UILabel!
    @IBOutlet  var contentLable: UILabel!
    @IBOutlet  var cellImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
