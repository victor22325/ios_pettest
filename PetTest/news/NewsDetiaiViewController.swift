//
//  NewsDetiaiViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/10.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import Presentr
import IHKeyboardAvoiding

class NewsDetiaiViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var TitleImageView: UIImageView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var ContentTextView: UITextView!
    @IBOutlet weak var CommentTableView: UITableView!
    @IBOutlet weak var CommentTextField: UITextField!
    @IBOutlet weak var CommentButton: UIButton!
    
    @IBOutlet var avoidingView: UIView!
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var jsonList: JSON = []
    var jsonComment: JSON = []


    var news: News!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        KeyboardAvoiding.avoidingView = self.avoidingView
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeNewsDetiaiViewController"), object: nil)

        TitleImageView.image = UIImage(data: news.image!)!
        TitleLabel.text = news.name
        ContentTextView.text = news.content
        
        let soapFunc = "GetCommentList"
        let data:NSDictionary = [
            "itemid" : "\(news.id)"
        ]

        startSOAP(soapFunc: soapFunc,data: data)
    }
    
    @objc func tap(gesture: UITapGestureRecognizer) {
        CommentTextField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return jsonList["data"].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! NewsTableViewCell
        
        //Configure the cell...
        cell.titleLable.text = self.jsonList["data"][indexPath.row]["username"].string!
        cell.contentLable.text = self.jsonList["data"][indexPath.row]["content"].string!
        cell.cellImage.image = UIImage(named: "person.jpg")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //Edit Button
        let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: " Edit ", handler: { (action, indexPath) -> Void in
            if(self.jsonList["data"][indexPath.row]["userid"].string == UserGetSetData.Id){
                Comment.Id = self.jsonList["data"][indexPath.row]["commentid"].string!
                let presenter: Presentr = {
                    let width = ModalSize.fluid(percentage: 0.90)
                    let height = ModalSize.custom(size: 100)
                    let customType = PresentationType.custom(width: width, height: height, center: .center)
                    
                    let presenter = Presentr(presentationType: customType)
                    
                    presenter.transitionType = TransitionType.coverHorizontalFromRight
                    presenter.dismissOnSwipe = true
                    return presenter
                }()
                let storyboard = UIStoryboard(name: "News",bundle:nil)
                let popupViewController: CommentEditViewController = {
                    let popupViewController = storyboard.instantiateViewController(withIdentifier: "CommentEditViewController")
                    return popupViewController as! CommentEditViewController
                }()
                
                presenter.transitionType = nil
                presenter.dismissTransitionType = nil
                presenter.keyboardTranslationType = .compress
                self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            }
        })
        
        //Delete button
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete",handler: { (action, indexPath) -> Void in
            if(self.jsonList["data"][indexPath.row]["userid"].string == UserGetSetData.Id){
                Comment.Id = self.jsonList["data"][indexPath.row]["commentid"].string!
            
                let soapFunc = "deleteComment"
                let data:NSDictionary = [
                    "commentid": "\(Comment.Id)"
                ]
            
                self.startSOAP(soapFunc: soapFunc,data: data)
            
                self.loadList()
            }
            
        })
        
        
        shareAction.backgroundColor = UIColor(red: 68.0/255.0, green: 108.0/255.0, blue: 179.0/255.0, alpha: 1.0)
        deleteAction.backgroundColor = UIColor(red: 210.0/255.0, green: 77.0/255.0, blue: 87.0/255.0, alpha: 1.0)
        
        return [deleteAction, shareAction]

    }

    
    @IBAction func actionButton(sender: UIButton) {
        if sender == CommentButton {
            let soapFunc = "uploadComment"
            let data:NSDictionary = [
                "content" : "\(CommentTextField.text!)",
                "ip" : "",
                "item_id" : "\(news.id)",
                "parent_id" : "0",
                "userid" : "\(UserGetSetData.Id)"
            ]
            
            startSOAP(soapFunc: soapFunc,data: data)
            
            loadList()
        }
    }
    
    func startSOAP(soapFunc: String, data:NSDictionary){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellSoapService(soapFunc: soapFunc,data: data)
            
            let queue = DispatchQueue(label:"instructuin",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 15
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.soapConnect && i < timeOut)
                self.checkNewsComment(soapFunc: soapFunc)
            }
            
        } else {
            CommentTableView.isHidden = true
            CommentTextField.isHidden = true
            CommentButton.isHidden = true
            
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
    }
    
    func cellSoapService(soapFunc: String, data:NSDictionary) {
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            switch soapFunc{
                case "GetCommentList" :
                    self.jsonList = SOAPServices.sharedInstance.getAppCall(result!)
                    self.soapResult = self.jsonList["status"].string!
                
                default:
                    self.jsonComment = SOAPServices.sharedInstance.getAppCall(result!)
                    self.soapResult = self.jsonComment["status"].string!

                    break
            }
           
            print(status)
            self.soapConnect = status
            
        }
    }
    
    func checkNewsComment(soapFunc: String){
        if(self.soapConnect){
            if(self.soapResult == "true"){
                if(soapFunc == "GetCommentList"){
                    EZLoadingActivity.Settings.SuccessText = "Success"
                    EZLoadingActivity.hide(true, animated: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        self.CommentTableView.reloadData()
                    }
                }
                else{
                    Comment.reloadFlag = true
                }
            }
            else{
                EZLoadingActivity.Settings.SuccessText = "Success"
                EZLoadingActivity.hide(true, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    
    @objc func loadList(){
        let soapFuncReload = "GetCommentList"
        let dataReload:NSDictionary = [
            "itemid" : "\(news.id)"
        ]
        
        let queue = DispatchQueue(label:"Reload",qos: DispatchQoS.userInitiated)
        
        queue.async {
            repeat{
                Thread.sleep(forTimeInterval: 2)
            }while(!Comment.reloadFlag )
            Comment.reloadFlag = false
            self.startSOAP(soapFunc: soapFuncReload,data: dataReload)
        }        
        print("reload")
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }

}
