//
//  NewsTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/9.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import Presentr

class NewsTableViewController: UITableViewController {
    
    @IBOutlet weak var reloadUIButton: UIButton!
    
    //function call
//    let optionBar = OptionBar()
    
    var loading: Bool = false
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    var data:[Data] = []
    
    var news: News = News()
    var optionsMenu: CAPSOptionsMenu?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewInit()
        
//        optionsMenu = optionBar.addOptionsMenu(ViewController: self)
        NotificationCenter.default.addObserver(self, selector: #selector(menuBarSelectorReceived), name: NSNotification.Name(rawValue: "menuBarSelector"), object: nil)
        
        startSOAP()
  
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let reloadImage = UIImage(named: "button_reload")?.withRenderingMode(.alwaysOriginal)
        let reloadUIbutton = UIBarButtonItem(image: reloadImage, style: .plain, target: self, action: #selector(startSOAP))
        
        navigationItem.setLeftBarButton(reloadUIbutton, animated: true)
        
        let gridImage = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        let gridBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        gridBtn.setImage(gridImage, for: UIControl.State.normal)
        gridBtn.addTarget(self, action: #selector(gridAction), for: UIControl.Event.touchUpInside)
        gridBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let gridUIbutton = UIBarButtonItem(customView: gridBtn)
        
        navigationItem.setRightBarButtonItems([gridUIbutton], animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return json["data"].count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! NewsTableViewCell
        
        //Configure the cell...
        cell.titleLable.text = self.json["data"][indexPath.row]["name"].string!
        cell.contentLable.text = self.json["data"][indexPath.row]["summary"].string!
        cell.cellImage.image = UIImage(data: self.data[indexPath.row])!
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print(indexPath)
        news = News(id:self.json["data"][indexPath.row]["id"].string!,name:self.json["data"][indexPath.row]["name"].string!,content:self.json["data"][indexPath.row]["content"].string!,image:self.data[indexPath.row])
        performSegue(withIdentifier: "showNewsDetail", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showNewsDetail" {
            let destinationController = segue.destination as! NewsDetiaiViewController
            destinationController.news = news
        }
    }
    
    
    @objc func startSOAP(){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellSoapService()
            
            let queue = DispatchQueue(label:"instructuin",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 25
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.soapConnect && i < timeOut)
                self.loading = true
                self.checkNews()
            }
            
        } else {
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
    }
    
    
    
    func cellSoapService() {
        
        let soapFunc = "getNewsList"
        let data:NSDictionary = [
            "" : ""
        ]
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            self.soapConnect = status
            self.soapResult = self.json["status"].string!
            
        }
    }
    
    func checkNews(){
        if(self.soapConnect){
            if(self.soapResult == "true"){
                for i in 0 ..< json["data"].count {
                    let url = URL(string: self.json["data"][i]["thumb"].string!)
                    self.data.append(try! Data(contentsOf: url!))
                }
                
                EZLoadingActivity.Settings.SuccessText = "Success"
                EZLoadingActivity.hide(true, animated: false)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.tableView.reloadData()
                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    @objc func menuBarSelectorReceived(notification: NSNotification) {
        let selector = notification.object as! String
        switch selector {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break
            
        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break
            
        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break
            
        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break
            
        default:
            print(selector + " no handle")
            break
        }
    }

    @objc func gridAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.35)
            let height = ModalSize.fluid(percentage: 0.38)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 250, y: 70))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base", bundle: nil)
        let gridViewController = storyboard.instantiateViewController(withIdentifier: "menuTableViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: gridViewController, animated: false, completion: nil)
    }
}
