//
//  News.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/9.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation

class News{
    var id=""
    var name=""
    var content=""
    var image: Data? = nil
    
    init(){
        
    }

    init(id:String,name:String,content:String,image:Data){
        self.id = id
        self.name = name
        self.content = content
        self.image = image
        
    }
}
