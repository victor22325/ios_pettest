//
//  SettingViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
import UserNotifications
import CoreData
import EZLoadingActivity

class SettingViewController: UIViewController, NSFetchedResultsControllerDelegate{

    @IBOutlet weak var medicineUISwitch: UISwitch!
    @IBOutlet weak var clinicUISwitch: UISwitch!
    @IBOutlet weak var autoSyncUISwitch: UISwitch!
    @IBOutlet weak var autoLoginUISwitch: UISwitch!
    
    let userDefults = UserDefaults.standard
    
    //reminder
    var reminder:[ReminderModel] = []
    var fetchResultController: NSFetchedResultsController<ReminderModel>!
    
    //function call
//    let optionBar = OptionBar()
    
    var optionsMenu: CAPSOptionsMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(menuBarSelectorReceived), name: NSNotification.Name(rawValue: "menuBarSelector"), object: nil)
        
        viewInit()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewInit(){
        //放logo到navigition
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
//        optionsMenu = optionBar.addOptionsMenu(ViewController: self)
        let gridImage = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        let gridBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        gridBtn.setImage(gridImage, for: UIControl.State.normal)
        gridBtn.addTarget(self, action: #selector(gridAction), for: UIControl.Event.touchUpInside)
        gridBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let gridUIbutton = UIBarButtonItem(customView: gridBtn)
        
        navigationItem.setRightBarButtonItems([gridUIbutton], animated: false)

        getSwitch(sender: medicineUISwitch,userSetting: "medicineReminder")
        getSwitch(sender: clinicUISwitch,userSetting: "clinicReminder")
        getSwitch(sender: autoSyncUISwitch,userSetting: "autoSync")
        getSwitch(sender: autoLoginUISwitch,userSetting: "autoLogin")
        
        autoSyncUISwitch.isEnabled = false
        autoLoginUISwitch.isEnabled = false
    }
    
    @IBAction func actionSend(sender: UISwitch){
        if(sender == medicineUISwitch){
            setSwitch(sender: medicineUISwitch,userSetting: "medicineReminder")
            
            if(userDefults.value(forKey: "medicineReminder") as! String? == "true"){
                setReminderOn(userSetting: "medicineReminder")
            }
            else{
                setReminderOff(userSetting: "medicineReminder")
            }
        }
        else if(sender == clinicUISwitch){
            setSwitch(sender: clinicUISwitch,userSetting: "clinicReminder")
            
            if(userDefults.value(forKey: "clinicReminder") as! String? == "true"){
                setReminderOn(userSetting: "clinicReminder")
            }
            else{
                setReminderOff(userSetting: "clinicReminder")
            }
        }
        else if(sender == autoSyncUISwitch){
            setSwitch(sender: autoSyncUISwitch,userSetting: "autoSync")
        }
        else if(sender == autoLoginUISwitch){
            setSwitch(sender: autoLoginUISwitch,userSetting: "autoLogin")
        }
    }
    
    //點擊Switch功能
    func setSwitch(sender: UISwitch,userSetting: String){
        if sender.isOn{
            userDefults.set("true", forKey: "\(userSetting)")
        }
        else{
            userDefults.set("false", forKey: "\(userSetting)")
        }
        
        print("\(userSetting) \(String(describing: userDefults.value(forKey: userSetting) as! String?))")
    }
    
    //初始化Switch
    func getSwitch(sender: UISwitch,userSetting: String){
        let checker = userDefults.value(forKey: "\(userSetting)") as! String?
        if checker! == "true"{
             sender.setOn(true, animated:true)
        }
        else{
             sender.setOn(false, animated:true)
        }
    }
    
    //reminder switch On
    func setReminderOn(userSetting: String){
        print("setOn")
        loadData()
        reminder = setReminderData(userSetting: userSetting,Reminder: reminder)
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        var total: Int = 0
        
        for i in 0 ..< reminder.count{
            var delayInSeconds = Double(total) / 5.0
            
            let reminderItem = self.reminder[i]
                
            if(userSetting == "waterReminder"){
                let identifierArray = (reminderItem.notificationId?.components(separatedBy: "-"))!
                let frequencyArray = reminderItem.before?.components(separatedBy: " ")
                let timeArray = reminderItem.startDate?.components(separatedBy: "-")
                
                let dateArray:[Date] = TimeData.sharedInstance.setTimeInterval(startTime: timeArray![0],endTime: timeArray![1],timeInterval: Int(frequencyArray![0])!)
                
                for j in 0 ..< identifierArray.count{
                    delayInSeconds = Double(total) / 5.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        Notification.sharedInstance.addNotificationRepeat(type: "water", identifier: "\(reminderItem.id).\(j)", contentString: reminderItem.content!,alertType:  reminderItem.remindType!,date: dateArray[j])
                    }
                    total = total + 1
                }
            }
            else if(userSetting == "medicineReminder"){
                let identifierArray = (reminderItem.notificationId?.components(separatedBy: "-"))!
                let beforeArray = reminderItem.before?.components(separatedBy: " ")
                let medicineTime = (reminderItem.startDate?.components(separatedBy: "-"))!
                
                let dateArray:[Date] = TimeData.sharedInstance.setTimeInterval(selectTimeArray: medicineTime,timeBefore: Int(beforeArray![0])!)
                
                for j in 0 ..< identifierArray.count{
                    delayInSeconds = Double(total) / 5.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        Notification.sharedInstance.addNotificationRepeat(type: "medicine", identifier: "\(reminderItem.id).\(j)", contentString: reminderItem.content!,alertType:  reminderItem.remindType!,date: dateArray[j])
                    }
                    total = total + 1
                }
            }
            else if(userSetting == "clinicReminder"){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    let beforeArray = reminderItem.before?.components(separatedBy: " ")
                    let time = Int(TimeData.sharedInstance.stringToTimestamp(timeString: reminderItem.createDate!)) - TimeData.sharedInstance.timestampShift(date: Date()) - TimeData.sharedInstance.getTimeInterval(time: Int((beforeArray?[0])!)!,type: (beforeArray?[1])!)
                    if(time > 0 && reminderItem.remindType != "none"){
                        Notification.sharedInstance.addNotification(type: "clinic", identifier: reminderItem.notificationId!, contentString: reminderItem.content!,alertType: reminderItem.remindType!, timeInterval: TimeInterval(time))
                    }
                }
                total = total + 1
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(total/5)) {
            EZLoadingActivity.Settings.SuccessText = "Success"
            EZLoadingActivity.hide(true, animated: false)
        }
    }
    
    //reminder switch Off
    func setReminderOff(userSetting: String){
        print("setOff")
        loadData()
        reminder = setReminderData(userSetting: userSetting,Reminder: reminder)
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        var total: Int = 0
        
        for i in 0 ..< reminder.count{
            var delayInSeconds = Double(total) / 5.0
            let reminderItem = self.reminder[i]
            
            if(userSetting == "waterReminder"){
                let identifierArray = (reminderItem.notificationId?.components(separatedBy: "-"))!
                for j in 0 ..< identifierArray.count{
                    delayInSeconds = Double(total) / 5.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        print(identifierArray[j])
                        Notification.sharedInstance.removeNotification(identifier: identifierArray[j])
                    }
                    total = total + 1
                }
            }
            else if(userSetting == "medicineReminder"){
                let identifierArray = (reminderItem.notificationId?.components(separatedBy: "-"))!
                for j in 0 ..< identifierArray.count{
                    delayInSeconds = Double(total) / 5.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        print(identifierArray[j])
                        Notification.sharedInstance.removeNotification(identifier: identifierArray[j])
                    }
                }
                total = total + 1
            }
            else if(userSetting == "clinicReminder"){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    print(reminderItem.notificationId!)
                    Notification.sharedInstance.removeNotification(identifier: "\(reminderItem.notificationId!)")
                }
                total = total + 1
            }
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(total/5)) {
            self.removeAllNotification(water: (self.userDefults.value(forKey: "waterReminder") as! String?)!,medinine: (self.userDefults.value(forKey: "medicineReminder") as! String?)!,clinic: (self.userDefults.value(forKey: "clinicReminder") as! String?)!)
            EZLoadingActivity.Settings.SuccessText = "Success"
            EZLoadingActivity.hide(true, animated: false)
        }
    
    }
    

    func loadData(){
        //從資料儲存區讀取
        let fethRequest: NSFetchRequest<ReminderModel> = ReminderModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "id",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    reminder = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //指定Reminder CoreData
    func setReminderData(userSetting: String,Reminder: [ReminderModel])->[ReminderModel]{
        var tempReminder:[ReminderModel] = []
        var type = ""
        if(userSetting == "waterReminder"){
            type = "water"
        }
        else if(userSetting == "medicineReminder"){
            type = "medicine"
        }
        else if(userSetting == "clinicReminder"){
            type = "clinic"
        }
        
        for i in 0 ..< Reminder.count{
            if(Reminder[i].type == type){
                tempReminder.append(Reminder[i])
            }
        }
        
        return tempReminder
    }
    
    //排除不明原因 Notification 卡背景
    
    func removeAllNotification(water: String,medinine: String,clinic: String){
        if(water == "false" && medinine == "false" && clinic == "false"){
            print("removeAll")
            Notification.sharedInstance.removeAllNotification()
        }
    
    }
    
    @objc func menuBarSelectorReceived(notification: NSNotification) {
        let selector = notification.object as! String
        switch selector {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break
            
        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break
            
        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break
            
        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break
            
        default:
            print(selector + " no handle")
            break
        }
    }
    
    @objc func gridAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.35)
            let height = ModalSize.fluid(percentage: 0.38)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 250, y: 70))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base", bundle: nil)
        let gridViewController = storyboard.instantiateViewController(withIdentifier: "menuTableViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: gridViewController, animated: false, completion: nil)
    }
    
}
