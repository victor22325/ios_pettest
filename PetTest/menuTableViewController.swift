//
//  menuTableViewController.swift
//  PetTest
//
//  Created by Victor on 2019/7/16.
//  Copyright © 2019 BMBMAC. All rights reserved.
//

import UIKit
import Presentr

class menuTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var menuBarTableView: UITableView!
    var menuBarList = [String]()
    
    override func viewDidLoad() {
        
//        menuBarList.append("Profile")
        menuBarList.append("Select Pet")
        menuBarList.append("Instruction")
        menuBarList.append("About")
        menuBarList.append("Version")
        menuBarList.append("Logout")
        menuBarList.append("Exit")
        
        self.menuBarTableView.delegate = self
        self.menuBarTableView.dataSource = self
        
        self.menuBarTableView.tableHeaderView?.frame = CGRect.zero
        self.menuBarTableView.tableFooterView?.frame = CGRect.zero
        self.menuBarTableView.bounces = false

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(menuBarList[indexPath.row])
        let selector = menuBarList[indexPath.row]
        switch selector {
        case "Select Pet":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.80)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = false
                return presenter
            }()
            
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: SelectPetViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "selectPetViewController")
                return popupViewController as! SelectPetViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break
        case "About":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.30)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: aboutViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "aboutViewController")
                return popupViewController as! aboutViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break
        case "Version":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.custom(size: 200)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: versionViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "versionViewController")
                return popupViewController as! versionViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break
        default:
            dismiss(animated: false, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuBarSelector"), object: selector)
            break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuBarList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuBarCell", for: indexPath)
        cell.textLabel?.text = menuBarList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
