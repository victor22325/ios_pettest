//
//  MonthSelectViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/27.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr

class MonthSelectViewController: UIViewController {
    
    @IBOutlet weak var monthYearPickerView: MonthYearPickerView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    var date: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM"
        
        date = dateFormatter.string(from: Date.init())
        
        monthYearPickerView.onDateSelected = { (month: Int, year: Int) in
            self.date = "\(year)/\(self.addZero(number: month))"
            NSLog(self.date)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(sender: UIButton) {
        if(sender == doneButton){
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM"
            Note.date = formatter.date(from: date)!
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetMonth"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetMonthLable"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandCalendarReload"), object: nil)
            dismiss(animated: true, completion: nil)
            print(date)
        }
        else if(sender == cancelButton){
            dismiss(animated: true, completion: nil)
        }
    }
    
    func addZero(number: Int)->String{
        var result = "\(number)"
        if(number < 10){
            result = "0\(number)"
        }
        return result
    }
    
}

// MARK: - Presentr Delegate

extension MonthSelectViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension MonthSelectViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
