//
//  NoteTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import JTAppleCalendar
import CoreData

class CalendarViewController: UIViewController,NSFetchedResultsControllerDelegate{
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    let formatter = DateFormatter()
    
    var hideTest = false
    
    let outsideMonthColor = UIColor(red: 214/255, green: 226/255, blue: 171/255, alpha: 1)
    let monthColor = UIColor.black
    let currentDateSelectedViewColor = UIColor(red: 165/255, green: 165/255, blue: 165/255, alpha: 1)
    
    var noteString:[String] = []
    var bloodString:[String] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setCalendarView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setCalendarMonth), name: NSNotification.Name(rawValue: "SetMonth"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCalendar), name: NSNotification.Name(rawValue: "reloadCalendar"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectToday), name: NSNotification.Name(rawValue: "selectToday"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(clearSelect), name: NSNotification.Name(rawValue: "clearSelect"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeCalendarViewController"), object: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setCalendarView(){
        //calendar init
        calendarView.scrollToDate(Note.date)
        
        //setup calendar spacing
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
    }
    
    @objc func setCalendarMonth(){
        calendarView.scrollToDate(Note.date)
    }
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CalendarCell else { return }
        if cellState.dateBelongsTo == .thisMonth{
            validCell.dateLable.textColor = monthColor
        }
        else{
            validCell.dateLable.textColor = outsideMonthColor
        }
        
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CalendarCell else { return }
        if cellState.isSelected{
            if cellState.dateBelongsTo == .thisMonth{
                validCell.seletedView.isHidden = false
            }
        }
        else{
            validCell.seletedView.isHidden = true
        }
        
    }
    
    func handleCellDataInit(view: JTAppleCell?, cellState: CellState,date: Date){
        guard let validCell = view as? CalendarCell else { return }
        
        if cellState.dateBelongsTo == .thisMonth{
            validCell.dataContainImageView.isHidden = false
            self.formatter.dateFormat = "yyyy-MM-dd"
            let noteCheck = checkDateString(dateString: noteString,checkDate: self.formatter.string(from: date))
            let bloodCheck = checkDateString(dateString: bloodString,checkDate: self.formatter.string(from: date))
            
            if(noteCheck && bloodCheck){
                validCell.dataContainImageView.image = UIImage(named: "icon_all")
            }
            else{
                if(noteCheck){
                    validCell.dataContainImageView.image = UIImage(named: "icon_pencil")
                }
                else if(bloodCheck){
                    validCell.dataContainImageView.image = UIImage(named: "icon_blood")
                }
                else{
                    validCell.dataContainImageView.image = UIImage(named: "icon_not_optional")
                }
            }
            
        }
        else{
            validCell.dataContainImageView.isHidden = true
        }
        
    }
    
    func checkDateString (dateString: [String],checkDate: String)->Bool{
        var result = false
        
        for i in 0 ..< dateString.count{
            if(dateString[i].substring(with: 0..<10) == checkDate){
                result = true
            }
        }
        
        return result
    }
    
    func setupViewOfCalendar(from visibleDates: DateSegmentInfo){
        let date = visibleDates.monthDates.first!.date
        Note.date = date
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetMonthLable"), object: nil)
    }
    
    func setDayItem(){
        noteString = []
        bloodString = []
        for i in 0 ..< Note.noteData.count{
            let noteItem = Note.noteData[i]
            if(noteItem.noteType == "notes"){
                noteString.append(noteItem.createDate!)
            }
            else{
                bloodString.append(noteItem.createDate!)
            }
        }
    }
    
    @objc func reloadCalendar(){
        setDayItem()
        calendarView.reloadData()
    }
    
    @objc func selectToday(){
        let now = Date()
        calendarView.selectDates([now])
        calendarView.scrollToDate(now)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.75) {
            self.calendarView.selectDates([now])
            self.calendarView.reloadData()
        }
    }
    
    @objc func clearSelect(){
        calendarView.deselectAllDates()
        calendarView.reloadData()
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
}

extension CalendarViewController: JTAppleCalendarViewDataSource{
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyyMMdd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        
        let startDate = formatter.date(from: "20000101")!
        let endDate = formatter.date(from: "20401231")!
        
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate)
        return parameters
    }
    
}
extension CalendarViewController: JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    //display cell
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CalendarCell
        cell.dateLable.text = cellState.text
        
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        handleCellDataInit(view: cell, cellState: cellState,date: date)
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        let gregorian = Calendar(identifier: .gregorian)
        let now = Date()
        var components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
        var componentsClick = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)

        components.year = componentsClick.year
        components.month = componentsClick.month
        components.day = componentsClick.day
        
        let dateSet = gregorian.date(from: components)!
        print(Note.clickDate)
        print(dateSet)
        print("clcick")
        
        
        Note.clickDate = dateSet
        

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshNote"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandTableReload"), object: nil)

        
    }
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        //scroll month
        setupViewOfCalendar(from: visibleDates)
    }
}

