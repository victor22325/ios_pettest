//
//  NoteViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr

class NoteViewController: UIViewController,UISearchBarDelegate{

    @IBOutlet weak var monthButton: UIButton!
    
    @IBOutlet weak var todayUIBotton: UIButton!
    @IBOutlet weak var addNoteUIBotton: UIButton!
    
    @IBOutlet weak var noteSearchBar: UISearchBar!
    @IBOutlet weak var noteCancelView: UIView!
    @IBOutlet weak var noteCancelButton: UIButton!
    
    @IBOutlet weak var nextMonthUIBotton: UIButton!
    @IBOutlet weak var lastMonthUIBotton: UIButton!
    
    
//    let optionBar = OptionBar()
//    var optionsMenu: CAPSOptionsMenu?
    
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewInit()
        
        Note.date = Date.init()
        Note.clickDate = Date()
        Note.clickExpand = true
            
//        optionsMenu = optionBar.addOptionsMenu(ViewController: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setDate), name: NSNotification.Name(rawValue: "SetMonthLable"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(editNote), name: NSNotification.Name(rawValue: "editNote"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeNoteViewController"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(menuBarSelectorReceived), name: NSNotification.Name(rawValue: "menuBarSelector"), object: nil)
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        let gridImage = UIImage(named: "grid_icon")?.withRenderingMode(.alwaysOriginal)
        let gridBtn: UIButton = UIButton(type: UIButton.ButtonType.custom)
        gridBtn.setImage(gridImage, for: UIControl.State.normal)
        gridBtn.addTarget(self, action: #selector(gridAction), for: UIControl.Event.touchUpInside)
        gridBtn.frame = CGRect(x: 0,y: 0,width: 23,height: 23)
        let gridUIbutton = UIBarButtonItem(customView: gridBtn)
        
        navigationItem.setRightBarButtonItems([gridUIbutton], animated: false)
        
        noteSearchBar.delegate = self
        setDate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        noteSearchBar.endEditing(true)
        Note.noteSearchString = noteSearchBar.text!
        print( Note.noteSearchString)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshNote"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandTableReload"), object: nil)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        Note.noteSearchString = ""
        noteCancelView.isHidden = false
    }
        
    @IBAction func actionSend(sender: UIButton){
        if(sender == nextMonthUIBotton){
            Note.date = Calendar.current.date(byAdding: .month, value: 1, to: Note.date)!
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetMonth"), object: nil)
            setDate()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandCalendarReload"), object: nil)
        }
        else if (sender == lastMonthUIBotton){
            Note.date = Calendar.current.date(byAdding: .month, value: -1, to: Note.date)!
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetMonth"), object: nil)
            setDate()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandCalendarReload"), object: nil)
        }
        else if(sender == addNoteUIBotton){
            Note.noteItem = nil
            performSegue(withIdentifier: "editNote", sender: self)
        }
        else if(sender == todayUIBotton){
            print("today")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectToday"), object: nil)
        }
        else if(sender == noteCancelButton){
            Note.noteSearchString = ""
            noteSearchBar.text = ""
            noteCancelView.isHidden = true
            noteSearchBar.endEditing(true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "clearSelect"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "expandCalendarReload"), object: nil)
        }
        else if(sender == monthButton){
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.80)
                let height = ModalSize.custom(size: 250)
                let customType = PresentationType.custom(width: width, height: height, center: .center)
                
                let presenter = Presentr(presentationType: customType)
                
                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Notes",bundle:nil)
            let popupViewController: MonthSelectViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "MonthSelectViewController")
                return popupViewController as! MonthSelectViewController
            }()
            
            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
        }
    }
    
    @objc func menuBarSelectorReceived(notification: NSNotification) {
        let selector = notification.object as! String
        switch selector {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break
            
        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break
            
        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break
            
        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break
            
        default:
            print(selector + " no handle")
            break
        }
    }
    
    @objc func gridAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.35)
            let height = ModalSize.fluid(percentage: 0.38)
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 250, y: 70))
            let customType = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base", bundle: nil)
        let gridViewController = storyboard.instantiateViewController(withIdentifier: "menuTableViewController")
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = false
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: gridViewController, animated: false, completion: nil)
    }
    
    @objc func setDate(){
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        formatter.dateFormat = "MMMM yyyy"
        monthButton.setTitle(self.formatter.string(from: Note.date), for: .normal)
    }
    
    
    @objc func editNote(){
        performSegue(withIdentifier: "editNote", sender: self)
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }

}
