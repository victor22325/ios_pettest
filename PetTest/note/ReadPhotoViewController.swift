//
//  ReadPhotoViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr

class ReadPhotoViewController: UIViewController  {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var rotateButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoImageView.image =  UIImage(data: Note.notePhoto! as Data)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(sender: UIButton) {
         photoImageView.transform = photoImageView.transform.rotated(by: CGFloat(M_PI_2))
    }
    
    
}

// MARK: - Presentr Delegate

extension ReadPhotoViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension ReadPhotoViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}
