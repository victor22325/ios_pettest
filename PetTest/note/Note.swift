//
//  Note.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import Foundation
import CoreData

class Note{
    static var date = Date.init()
    static var clickExpand = true
    static var clickDate = Date()
    
    static var noteItem: NoteModel? = nil
    static var notePhoto: NSData? = nil
    static var noteRecord: NSData? = nil
    static var noteRecordMode: String = "record"
    
    static var noteData:[NoteModel] = []
    
    static var noteSearchString: String = ""
    
    static var noteRegister = false
}
