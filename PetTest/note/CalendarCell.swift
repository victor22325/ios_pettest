//
//  CalendarCell.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {

    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var seletedView: UIView!
    @IBOutlet weak var dataContainImageView: UIImageView!

}
