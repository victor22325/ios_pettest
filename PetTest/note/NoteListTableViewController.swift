//
//  NoteListTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import CoreData

class NoteListTableViewController: UITableViewController,NSFetchedResultsControllerDelegate{
    
    var note:[NoteModel] = []
    var noteAll:[NoteModel] = []
    var fetchResultController: NSFetchedResultsController<NoteModel>!
    
    let dataFunction = DataFunction()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        dataSeletUser()
        //dataSeletPet()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCalendar"), object: nil)
        
        
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadNote), name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshNote), name: NSNotification.Name(rawValue: "refreshNote"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeNoteListTableViewController"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return note.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! NoteListTableViewCell
        
        let pointer = indexPath.row
        let noteData = note[pointer]
        
        if(noteData.noteType == "notes"){
            cell.bloodStatusLabel.text = ""
            cell.bloodValueLabel.text = ""
            
            if(noteData.noteImage != nil){
                cell.NoteImageView.isHidden = false
                cell.NoteImageView.image = UIImage(data: noteData.noteImage as! Data)
            }
            else{
                cell.NoteImageView.isHidden = true
            }
        }
        else{
            let noteDeviceStatus = dataFunction.setDeviceStatus(statusString: noteData.noteType!)
            let noteStatus = dataFunction.setStatus(statusString: noteData.noteContent!)
            let max = dataFunction.setMax(DeviceStatus: noteDeviceStatus,Status: noteStatus)
            let min = dataFunction.setMin(DeviceStatus: noteDeviceStatus,Status: noteStatus)
            
            cell.bloodStatusLabel.text = "\(dataFunction.setUnit(DeviceStatus: noteDeviceStatus))"
            if(noteData.noteType == "temperature"){
                cell.bloodValueLabel.text = "\(noteData.noteAction/10).\(noteData.noteAction%10)"
            }
            else{
                cell.bloodValueLabel.text = "\(noteData.noteAction)"
            }
            
            cell.NoteImageView.isHidden = true
            cell.bloodValueLabel.textColor = dataFunction.setColor(value: Int(noteData.noteAction),DeviceStatus: noteDeviceStatus,Status: noteStatus,max: max,min: min)
        }

        cell.NoteContentLabel.text = "\(noteData.noteContent!)"
        cell.createDateLabel.text = "\(setTime(dateString: noteData.createDate!))"
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        if(note[indexPath.row].noteType == "notes"){
            Note.noteItem = note[indexPath.row]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editNote"), object: nil)
        }
    }
    
    
    func loadData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<NoteModel> = NoteModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    note = fetchedObjects
                    noteAll = note
                    Note.noteData = note
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    
    
    
    //取時間區間
    func dataRange(){
        note = noteAll
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startPoint = dateFormatter.string(from: Note.clickDate) + " 00:00:01"
        let endPoint = dateFormatter.string(from: Note.clickDate) + " 23:59:59"
        //刪除上半段
        note = note.filter { stringToTimestamp(timeString: $0.createDate!) > stringToTimestamp(timeString: startPoint) }
        
        //刪除下半段
        note = note.filter { stringToTimestamp(timeString: $0.createDate!) < stringToTimestamp(timeString: endPoint) }
        
    }
    
    
    
    //取指定字串
    func dataSeletString(dataString: String){
        note = noteAll

        //只留有指定字串
        note = note.filter { ($0.noteContent?.containsIgnoringCase(find: dataString))!}
        
    }
    
    //取得指定使用者資料
    func dataSeletUser(){
        //只留有指定使用者資料
        note = note.filter{ $0.userId == UserGetSetData.Id}
        noteAll = note
        Note.noteData = note

    }
    
    //取得指定使用者資料
    func dataSeletPet(){
        //只留有指定使用者資料
        note = note.filter{ $0.petName == UserGetSetData.PetName || $0.noteType == "notes"}
        noteAll = note
        Note.noteData = note
        
    }
    
    @objc func reloadNote(){
        loadData()
        dataSeletUser()
        //dataSeletPet()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCalendar"), object: nil)
        dataRange()
        self.tableView.reloadData()
    }
    
    @objc func refreshNote(){
        print("refreshNote")
        if(Note.noteSearchString != ""){
            dataSeletString(dataString: Note.noteSearchString)
        }
        else{
            dataRange()
        }
        
        self.tableView.reloadData()
    }
    
    func setTime(dateString: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        let dateFormatterResult = DateFormatter()
        dateFormatterResult.dateFormat = "hh:mm a MMM/dd"
        dateFormatterResult.locale = NSLocale(localeIdentifier: "en_US") as Locale
        return dateFormatterResult.string(from: date!)
    }
    
    //字串轉Unix Time
    func stringToTimestamp(timeString: String)->Int64{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd HH:mm:ss"
        let date = dfmatter.date(from: "\(timeString)")
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int64 = Int64(dateStamp)
        return dateSt
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }

}
