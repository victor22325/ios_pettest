//
//  NoteTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class NoteTableViewController: UITableViewController {
    
    @IBOutlet weak var noteTableView: UITableView!
    @IBOutlet weak var tableListView: UIView!
    
    var selectedIndexPath : IndexPath?
    var testIndexPath : IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(expandTableReload), name: NSNotification.Name(rawValue: "expandTableReload"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(expandCalendarReload), name: NSNotification.Name(rawValue: "expandCalendarReload"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeNoteTableViewController"), object: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Note.clickExpand = !Note.clickExpand
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }
    
    @objc func expandTableReload(){
        print("expandTableReload")
        if(Note.clickExpand){
            tableListView.isHidden = false
            Note.clickExpand = !Note.clickExpand
            let indexPath:IndexPath = IndexPath(row: 0, section: 0)
            tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
    @objc func expandCalendarReload(){
        if(!Note.clickExpand){
            tableListView.isHidden = true
            Note.clickExpand = !Note.clickExpand
            let indexPath:IndexPath = IndexPath(row: 1, section: 0)
            tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(Note.clickExpand){
            if indexPath.row == 0 {
                return 242
            } else {
                return 0
            }
        }
        else{
            if indexPath.row == 1 {
                return 380
            } else {
                return 0
            }
        }
    }
    
    
}
