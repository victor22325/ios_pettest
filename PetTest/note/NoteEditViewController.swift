//
//  NoteEditViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/12.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import CoreData
import Presentr
import DatePickerDialog


class NoteEditViewController: UIViewController, UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    @IBOutlet weak var selectDayButton: UIButton!
    @IBOutlet weak var selectTimeButton: UIButton!
    
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var mealButton: UIButton!
    @IBOutlet weak var exerciseButton: UIButton!
    @IBOutlet weak var noteButton: UIButton!
    @IBOutlet weak var medicalButton: UIButton!
    
    @IBOutlet weak var mealView: UIView!
    @IBOutlet weak var exerciseView: UIView!
    @IBOutlet weak var noteView: UIView!
    @IBOutlet weak var medicalView: UIView!
    
    var note: NoteModel!
    
    var noteDate:Date!
    
    var noteType = "notes"
    var noteAction = 2
    
    var notePhoto: NSData? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewInit()
        
        //new or edit
        if(Note.noteItem != nil){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            noteDate = dateFormatter.date(from: Note.noteItem!.createDate!)
            noteType = Note.noteItem!.noteType!
            noteAction = Int(Note.noteItem!.noteAction)
            Note.noteRecord = Note.noteItem!.noteVoice as NSData?
            notePhoto = Note.noteItem!.noteImage as NSData?
            setClick(sender: noteAction)
        }
        else{
            noteDate = Note.clickDate
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewInit(){
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        noteTextView.delegate = self
        
        var currentDate = Date()
        
        if(Note.noteItem != nil){
            noteTextView.text = Note.noteItem!.noteContent!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            currentDate = dateFormatter.date(from: Note.noteItem!.createDate!)!
        }
        else{
            noteTextView.text = "Add your contents note ..."
            currentDate = Note.clickDate
            Note.noteRecord = nil
            noteTextView.textColor = UIColor.lightGray
            deleteButton.isHidden = true
        }
        
        
        //init time
        selectDayButton.setTitle(TimeData.sharedInstance.setDayLabel(data: currentDate), for: .normal)
        selectTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: currentDate), for: .normal)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Add your contents note ..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func actionSend(sender : UIButton){
        if(sender == mealButton){
            noteAction = 0
            setClick(sender : noteAction)
        }
        else if(sender == exerciseButton){
            noteAction = 1
            setClick(sender : noteAction)
        }
        else if(sender == noteButton){
            noteAction = 2
            setClick(sender : noteAction)
        }
        else if(sender == medicalButton){
            noteAction = 3
            setClick(sender : noteAction)
        }
        else if(sender == selectDayButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .date) { (date) in
                if let dateCheck = date {
                    self.selectDayButton.setTitle(TimeData.sharedInstance.setDayLabel(data: dateCheck), for: .normal)
                    self.noteDate = TimeData.sharedInstance.setDay(data: self.noteDate,dateChange: dateCheck)
                }
            }

        }
        else if(sender == selectTimeButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .time) { (date) in
                if let dateCheck = date {
                    self.selectTimeButton.setTitle(TimeData.sharedInstance.setTimeLabel(data: dateCheck), for: .normal)
                    self.noteDate = TimeData.sharedInstance.setTime(data: self.noteDate,dateChange: dateCheck)
                }
            }
        }
        else if(sender == recordButton){
            if(Note.noteRecord == nil){
                Note.noteRecordMode = "record"
                audioAlert()
            }
            else{
                let alert = UIAlertController(title: "Please select the action to photo", message: "", preferredStyle: .alert)
                let readAction = UIAlertAction(title: "Read", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    Note.noteRecordMode = "play"
                    self.audioAlert()
                }
                let newAction = UIAlertAction(title: "New", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    Note.noteRecordMode = "record"
                    self.audioAlert()
                }
                let DeleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    Note.noteRecord = nil
                }
                alert.addAction(readAction)
                alert.addAction(newAction)
                alert.addAction(DeleteAction)
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        else if(sender == imageButton){
            //photo 相關
            if(notePhoto == nil){
                photoAlert()
            }
            else{
                let alert = UIAlertController(title: "Please select the action to photo", message: "", preferredStyle: .alert)
                let readAction = UIAlertAction(title: "Read", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    Note.notePhoto = self.notePhoto
                    self.photoRead()
                }
                let newAction = UIAlertAction(title: "New", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    self.photoAlert()
                }
                let DeleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    self.notePhoto = nil
                }
                alert.addAction(readAction)
                alert.addAction(newAction)
                alert.addAction(DeleteAction)
                self.present(alert, animated: true, completion: nil)

            }
            
        }
        else if(sender == deleteButton){
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                context.delete(Note.noteItem!)
                appDelegate.saveContext()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
                _ = navigationController?.popViewController(animated: true)
            }

        }
        else if(sender == cancelButton){
            _ = navigationController?.popViewController(animated: true)
        }
        else if(sender == doneButton){
            if(Note.noteItem != nil){
                Note.noteItem!.userId = UserGetSetData.Id
                Note.noteItem!.noteType = noteType
                Note.noteItem!.noteAction = Int64(noteAction)
                if(noteTextView.text == "Add your contents note ..."){
                    Note.noteItem!.noteContent = ""
                }
                else{
                    Note.noteItem!.noteContent = noteTextView.text!
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                Note.noteItem!.createDate = dateFormatter.string(from: noteDate)
                Note.noteItem!.noteVoice = Note.noteRecord as Data?
                Note.noteItem!.noteImage = notePhoto as Data?

            }
            else{
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    note = NoteModel(context: appDelegate.persistentContainer.viewContext)
                    note.userId = UserGetSetData.Id
                    note.noteType = noteType
                    note.noteAction = Int64(noteAction)
                    if(noteTextView.text == "Add your contents note ..."){
                        note.noteContent = ""
                    }
                    else{
                        note.noteContent = noteTextView.text!
                    }
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    note.createDate = dateFormatter.string(from: noteDate)
                    
                    
                    note.noteVoice = Note.noteRecord as Data?
                    
                    note.noteImage = notePhoto as Data?
                    
                    note.bloodId = 0
                    note.pressureId = 0
                    note.temperatureId = 0
                    
                    appDelegate.saveContext()
                }

            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    //下方 Button color
    func setClick(sender : Int){
        let gray = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        let blue = UIColor(red: 88/255, green: 137/255, blue: 191/255, alpha: 1.0)
        
        if(sender == 0){
            mealView.backgroundColor = blue
            exerciseView.backgroundColor = gray
            noteView.backgroundColor = gray
            medicalView.backgroundColor = gray
        }
        else if(sender == 1){
            mealView.backgroundColor = gray
            exerciseView.backgroundColor = blue
            noteView.backgroundColor = gray
            medicalView.backgroundColor = gray
        }
        else if(sender == 2){
            mealView.backgroundColor = gray
            exerciseView.backgroundColor = gray
            noteView.backgroundColor = blue
            medicalView.backgroundColor = gray
        }
        else if(sender == 3){
            mealView.backgroundColor = gray
            exerciseView.backgroundColor = gray
            noteView.backgroundColor = gray
            medicalView.backgroundColor = blue
        }
    }
    
    //處理 photo回傳
    internal func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey: Any]){
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            let tempImage: UIImage? = selectedImage
            if let noteImage = tempImage{
                if let imageData = noteImage.pngData(){
                    notePhoto = NSData(data: imageData)
                }
            }

        }
        dismiss(animated: true, completion: nil)
    }
    
    //開啟錄音功能
    func audioAlert(){
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.custom(size: 350)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Notes",bundle:nil)
        let popupViewController: RecordAudioViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "RecordAudioViewController")
            return popupViewController as! RecordAudioViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    //新增一張圖片 （現有的 或 拍新的）
    func photoAlert(){
        let alert = UIAlertController(title: "Please select the action to add the photo", message: "This allows PetTest App to take a picutre of your pet's behavior or record. The purpose is able to attach photo to to do list.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style:  UIAlertAction.Style.cancel) {
            (action: UIAlertAction!) -> Void in
        }
        let selectAction = UIAlertAction(title: "Take Album", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.photoAdd(mode: "Select")
        }
        let newAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.photoAdd(mode: "New")
        }
        alert.addAction(cancelAction)
        alert.addAction(selectAction)
        alert.addAction(newAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //讀取 photo
    func photoRead(){
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.fluid(percentage: 0.80)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Notes",bundle:nil)
        let popupViewController: ReadPhotoViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "ReadPhotoViewController")
            return popupViewController as! ReadPhotoViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    //新增一張圖片 （現有的 或 拍新的）
    func photoAdd(mode: String){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            if(mode == "Select"){
                imagePicker.sourceType = .photoLibrary
            }
            else{
                imagePicker.sourceType = .camera
            }
            imagePicker.delegate = self
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
}
