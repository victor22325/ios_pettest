//
//  RecordAudioViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/6/13.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
import AVFoundation


class RecordAudioViewController: UIViewController {
    
    @IBOutlet weak var recordAudioButton: UIButton!
    @IBOutlet weak var recordAudioView: UIView!
    
    @IBOutlet weak var playAudioButton: UIButton!
    @IBOutlet weak var playAudioView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var saveAudioButton: UIButton!
    
    var totalTime:Double = 10.0
    var timer:Timer?
    
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    
    var isRecoding = false
    var isPlaying = false
    
    ////定义音频的编码参数，这部分比较重要，决定录制音频文件的格式、音质、容量大小等，建议采用AAC的编码方式
    let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0) as Float),//声音采样率
        AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC) as Int32),//编码格式
        AVNumberOfChannelsKey : NSNumber(value: 1 as Int32),//采集音轨
        AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.medium.rawValue) as Int32)]//音频质量
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setView(mode: Note.noteRecordMode)
        
        if(Note.noteRecordMode == "play"){
            saveAudioButton.setImage(UIImage(named: "icon-Exit")!, for: .normal)
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
            try audioRecorder = AVAudioRecorder(url: self.directoryURL()!,settings: recordSettings)//初始化实例
            
            audioRecorder.prepareToRecord()//准备录音
        } catch {
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(sender: UIButton) {
        if(sender == recordAudioButton){
            if(isRecoding){
                //停止录音
                
                audioRecorder.stop()
                let audioSession = AVAudioSession.sharedInstance()
                
                do {
                    try audioSession.setActive(false)
                    print("stop!!")
                    recordAudioButton.layer.removeAllAnimations()
                    isRecoding = false
                    recordAudioButton.setImage(UIImage(named: "micButtonImg")!, for: .normal)
                    setView(mode: "play")
                    
                }
                catch {
                }
            }
            else{
                //开始录音
                
                if !audioRecorder.isRecording {
                    let audioSession = AVAudioSession.sharedInstance()
                    do {
                        try audioSession.setActive(true)
                        audioRecorder.record()
                        print("record!")
                        animation()
                        isRecoding = true
                        recordAudioButton.setImage(UIImage(named: "recordBtn")!, for: .normal)
                    }
                    catch {
                    }
                }
            }
        }
        else if(sender == playAudioButton){
            
            if(isPlaying){
                //暂停播放
                if (!audioRecorder.isRecording){
                    do {
                        if(Note.noteRecordMode == "play"){
                            try audioPlayer = AVAudioPlayer(data: Note.noteRecord! as Data)
                        }
                        else{
                            try audioPlayer = AVAudioPlayer(contentsOf: audioRecorder.url)
                        }

                        audioPlayer.pause()
                        
                        print("pause!!")
                        isPlaying = false
                        playAudioButton.setImage(UIImage(named: "icon-Play")!, for: .normal)
                    }
                    catch {
                    }
                }
                
            }
            else{
                //开始播放
                if (!audioRecorder.isRecording){
                    
                    do {
                        if(Note.noteRecordMode == "play"){
                            try audioPlayer = AVAudioPlayer(data: Note.noteRecord! as Data)
                        }
                        else{
                            try audioPlayer = AVAudioPlayer(contentsOf: audioRecorder.url)
                        }
                        totalTime = (audioPlayer?.duration)!
                        timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector:#selector(setProgress), userInfo: nil, repeats: true)
                        audioPlayer.play()
                        print("play!!")
                        isPlaying = true
                        playAudioButton.setImage(UIImage(named: "icon-Stop")!, for: .normal)
                        
                    }
                    catch {
                    }
                }
            }
        }
        else if(sender == saveAudioButton){
            if(Note.noteRecordMode == "record"){
                do{
                    
                    Note.noteRecord =  try Data(contentsOf: audioRecorder.url) as NSData
                }
                catch{
                    
                }
            }
            dismiss(animated: true, completion: nil)
        }
    }
    
    func directoryURL() -> URL? {
        //定义并构建一个url来保存音频，音频文件名为ddMMyyyyHHmmss.caf
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmss"
        let recordingName = formatter.string(from: currentDateTime)+".caf"
        print(recordingName)
        
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundURL = documentDirectory.appendingPathComponent(recordingName)
        return soundURL
    }
    
    
    @objc func setProgress() {
        
        let formater = DateComponentsFormatter()
        formater.zeroFormattingBehavior = .pad
        formater.includesApproximationPhrase = false
        formater.includesTimeRemainingPhrase = false
        formater.allowedUnits = [.minute,.second]
        formater.calendar = Calendar.current
        
        timeLabel.text = formater.string(from: (audioPlayer?.currentTime)!)
        
        let progressCounter:Double=(audioPlayer?.currentTime)!
        
        progressView.progress=Float(progressCounter/totalTime)
        
        if(progressCounter > totalTime-0.1){
            isPlaying = false
            playAudioButton.setImage(UIImage(named: "icon-Play")!, for: .normal)
        }
    }
    
    func animation(){
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.fromValue = 0.88
        pulse.toValue = 1.12
        pulse.timingFunction = CAMediaTimingFunction(controlPoints: 0.42, 0.0, 0.58, 1.0)
        pulse.duration = 0.3
        pulse.beginTime = CACurrentMediaTime() + 0.15
        pulse.autoreverses = true
        pulse.repeatCount = Float.infinity
        recordAudioButton.layer.add(pulse, forKey: nil)
        
    }
    
    func setView(mode: String){
        if(mode == "record"){
            recordAudioView.isHidden = false
            recordAudioButton.isHidden = false
            playAudioView.isHidden = true
            timeLabel.isHidden = true
        }
        else{
            recordAudioView.isHidden = true
            recordAudioButton.isHidden = true
            playAudioView.isHidden = false
            timeLabel.isHidden = false

        }
    }
}

// MARK: - Presentr Delegate

extension RecordAudioViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension RecordAudioViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}
