//
//  ViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/3/2.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import CoreData
import AudioToolbox

class LoginViewController: UIViewController,NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var nologinButton: UIButton!
    
    @IBOutlet weak var nologinImage: UIImageView!
    
    var deviceSelect :Int = 0
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    
    let userDefults = UserDefaults.standard
    
    var count: CountModel!
    var countList: [CountModel] = []
    var fetchResultController: NSFetchedResultsController<CountModel>!
    
    
    var pet:[PetModel] = []
    var petFetchResultController: NSFetchedResultsController<PetModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //放logo到navigition
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        //檢查sqlite路徑用
        //print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last! as String)
        
        getLoginValue()
        
        if userDefults.value(forKey: "account") != nil {

        }
        else{
            nologinButton.isHidden = true
            nologinImage.isHidden = true
        }
        settingInit()
        databaseInit()
        CheckAutoLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionButton(sender: UIButton) {
        // Yes button clicked
        if sender == loginButton {
            
            EZLoadingActivity.show("Loading...", disableUI: true)
            
            if(checkout() != "OK"){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                    EZLoadingActivity.Settings.FailText = "\(self.checkout())"
                    EZLoadingActivity.hide(false, animated: false)
                }
                return
            }
            
            if Reachability.isConnectedToNetwork() == true {
     
                self.cellSoapService()
                
                let queue = DispatchQueue(label:"login",qos: DispatchQoS.userInitiated)
                
                queue.async {
                    var i = 0
                     let timeOut = 20
                    repeat{
                        Thread.sleep(forTimeInterval: 1)
                        i = i+1
                    }while(!self.soapConnect && i < timeOut)
                
                    self.checkLogIn()
                }

            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                    EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                    EZLoadingActivity.hide(false, animated: false)
                }

            
            }
            
        }
        else if sender == signupButton {
            print("signup")
        }
        
        else if sender == nologinButton {
            getUserValue()
            loadPetData()
            dataSeletUser()
            setCurrentPet()
            performSegue(withIdentifier: "loginSuccess", sender: self)
        }
    }
    
    //soap 設定
    func cellSoapService() {
        
        let soapFunc = "loginPetSelect"
        let data:NSDictionary = [
            "appId": "1",
            "password": "\(passwordTextField.text!)",
            "username": "\(accountTextField.text!)"
        ]
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.soapConnect = status
            self.soapResult = self.json["status"].stringValue
        }
    }
    
    //soap 回傳值驗證
    func checkLogIn(){
        if(self.soapConnect){
            print("YES")
            if(self.soapResult == "true") {
                DispatchQueue.main.async {
                    self.setLoginValue(account: self.accountTextField.text!,password: self.passwordTextField.text!,device: "Glucose")
                    self.setUserValue(json: self.json)
                    self.setPersonal(json: self.json)
                    EZLoadingActivity.Settings.SuccessText = "login Success"
                    EZLoadingActivity.hide(true, animated: false)
                    self.setPetData(json: self.json)
                    self.performSegue(withIdentifier: "loginSuccess", sender: self)
                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "login Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //設定 UserGetData物件
    func setPersonal(json: JSON){
        UserGetSetData.Id = json["id"].stringValue
        UserGetSetData.FristName = json["firstname"].stringValue
        UserGetSetData.LastName = json["username"].stringValue
        UserGetSetData.Email = json["email"].stringValue
        UserGetSetData.Password =  passwordTextField.text!
        
        UserGetSetData.Country = json["country"].stringValue
        UserGetSetData.Region = json["region"].stringValue
        UserGetSetData.City = json["city"].stringValue
        
        UserGetSetData.Gender = json["gender"].stringValue
        UserGetSetData.WeightUnit = json["weightUnit"].stringValue
        UserGetSetData.Weight = json["weight"].stringValue
        
        UserGetSetData.GlucoseUnit = json["GlucoseUnit"].stringValue
        UserGetSetData.DiabetesType = json["DiabetesType"].stringValue
        UserGetSetData.PreMealUpper = json["PreMeal_Upper"].stringValue
        UserGetSetData.PreMealLower = json["PreMeal_Lower"].stringValue
        UserGetSetData.PostMealUpper = json["PostMeal_Upper"].stringValue
        UserGetSetData.PostMealLower = json["PostMeal_Lower"].stringValue
        
        UserGetSetData.PetName = json["Pet_Name"].stringValue
        UserGetSetData.PetSpecies = json["Pet_Species"].stringValue
        UserGetSetData.PetBreed = json["Pet_Breed"].stringValue
        UserGetSetData.PetWeight = json["weight"].stringValue
        UserGetSetData.PetDiabetesType = json["Pet_Diabetes_Type"].stringValue
        UserGetSetData.PetDiabetesYear = json["Pet_Diabetes_Year"].stringValue
        UserGetSetData.PetGender = json["Pet_Gender"].stringValue
        UserGetSetData.PetBirth = json["Pet_Birth"].stringValue
        
        UserGetSetData.PetAppUse = json["Pet_App_Use"].stringValue
        UserGetSetData.AppUSE = json["App_Use"].stringValue
        
        UserGetSetData.Device_Status = self.deviceSelect
        UserGetSetData.Login_Status = true
    }

    
    //設定帳號密碼
    func setLoginValue(account : String, password : String,device: String){
        userDefults.set(account, forKey: "account")
        userDefults.set(password, forKey: "password")
        userDefults.set(device, forKey: "device")
    }
    func getLoginValue(){
        if userDefults.value(forKey: "account") != nil {
            accountTextField.text = userDefults.value(forKey: "account") as! String?
            passwordTextField.text = userDefults.value(forKey: "password") as! String?

        }
        
        self.deviceSelect = setDeviceSelect(Device: "Glucose")
    }
    
    //設定快速登入
    func setUserValue(json: JSON){
        userDefults.set(json.rawString(), forKey: "json")
    }
    
    func getUserValue(){
        if let jsonGet = userDefults.value(forKey: "json") {
            setPersonal(json: JSON(jsonGet as! String))
        } else {
            
        }
    }
    
    func setDeviceSelect(Device: String)->Int{
        var result = 0

        switch Device {
        case "Glucose":
            result = 0
        case "BloodPressure":
            result = 1
        case "Temperature":
            result = 2
        default:
            break
        }
        return result
    }
    
    
    //Setting 初始化
    func settingInit(){
        if userDefults.value(forKey: "autoLogin") != nil {
            
        }
        else{
            userDefults.set("false", forKey: "autoLogin")
            userDefults.set("false", forKey: "autoSync")
            userDefults.set("false", forKey: "autoSyncNoInternet")
            userDefults.set("true", forKey: "waterReminder")
            userDefults.set("true", forKey: "medicineReminder")
            userDefults.set("true", forKey: "clinicReminder")
            
        }
    }
    
    func databaseInit(){
        loadDbData()
        
        if(countList.count == 0){
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                count = CountModel(context: appDelegate.persistentContainer.viewContext)
                count.bloodCount = 0
                count.pressCount = 0
                count.temperatureCount = 0
                count.reminderCount = 0
                appDelegate.saveContext()
            }
        }
    }
    
    func loadDbData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<CountModel> = CountModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "bloodCount",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    countList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //自動登入
    func CheckAutoLogin(){
        if userDefults.value(forKey: "autoLogin") != nil {
            let checker = userDefults.value(forKey: "autoLogin") as! String?
            if(checker == "true" && !(UserGetSetData.User_logout)){
                actionButton(sender: loginButton)
            }
        }
    }
    
    //驗證輸入
    func checkout() -> String{
        var result = "OK";
        
        if(accountTextField.text?.isEmpty)! {
            result = "E-mail is required"
        }
        else if !(isValidEmail(testStr: accountTextField.text!)){
            result = "Not a valid email address"
        }
        else if(passwordTextField.text?.isEmpty)! {
            result = "Password is required"
        }
        
        return result
    }

    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //取得寵物資料
    func setPetData(json: JSON){
        let fethRequest :NSFetchRequest<PetModel> = PetModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "petName",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        DispatchQueue.main.async {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                self.petFetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
                self.petFetchResultController.delegate = self
                
                do{
                    try self.petFetchResultController.performFetch()
                    if let fetchedObjects = self.petFetchResultController.fetchedObjects{
                        self.pet = fetchedObjects
                    }
                }
                catch{
                    print(error)
                }
            }
            self.pet = self.pet.filter{ $0.userId == UserGetSetData.Id}
            if self.pet.count == 0 {
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    let petItem = PetModel(context: appDelegate.persistentContainer.viewContext)
                    petItem.userId = json["id"].stringValue
                    petItem.petName = json["Pet_Name"].stringValue
                    petItem.petSpecies = json["Pet_Species"].stringValue
                    petItem.petBreed = json["Pet_Breed"].stringValue
                    petItem.petWeight = Float(Int64(json["weight"].stringValue)!)
                    petItem.petDiabetesType = json["Pet_Diabetes_Type"].stringValue
                    petItem.petDiabetesYear = json["Pet_Diabetes_Year"].stringValue
                    petItem.petGender = json["Pet_Gender"].stringValue
                    petItem.petBirth = json["Pet_Birth"].stringValue
                    petItem.petGlucoseLower = Int64(json["PreMeal_Lower"].stringValue)!
                    petItem.petGlucoseUpper = Int64(json["PreMeal_Upper"].stringValue)!
                    petItem.status = 1
                    appDelegate.saveContext()
                }
            } else {
                self.setCurrentPet()
            }
        }
    }
    
    func setCurrentPet(){
        dataSeletPet()
        let petItem  = pet[pet.count-1]
        
        UserGetSetData.PetName = petItem.petName!
        UserGetSetData.PetSpecies = petItem.petSpecies!
        UserGetSetData.PetBreed = petItem.petBreed!
        UserGetSetData.Weight = "\(petItem.petWeight)"
        UserGetSetData.PetWeight = "\(petItem.petWeight)"
        UserGetSetData.PetDiabetesType =  petItem.petDiabetesType!
        UserGetSetData.PetDiabetesYear = petItem.petDiabetesYear!
        UserGetSetData.PetGender = petItem.petGender!
        UserGetSetData.PetBirth = petItem.petBirth!
        UserGetSetData.PreMealUpper = "\(petItem.petGlucoseUpper)"
        UserGetSetData.PreMealLower = "\(petItem.petGlucoseLower)"
        UserGetSetData.PostMealUpper = "\(petItem.petGlucoseUpper)"
        UserGetSetData.PostMealLower = "\(petItem.petGlucoseLower)"
    }
    
    func loadPetData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<PetModel> = PetModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "petName",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        DispatchQueue.main.async {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let context = appDelegate.persistentContainer.viewContext
                self.petFetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
                self.petFetchResultController.delegate = self
                
                do{
                    try self.petFetchResultController.performFetch()
                    if let fetchedObjects = self.petFetchResultController.fetchedObjects{
                        self.pet = fetchedObjects
                    }
                }
                catch{
                    print(error)
                }
            }
        }
    }
    
    //取得指定使用者資料
    func dataSeletUser(){
        //只留有指定使用者資料
        pet = pet.filter{ $0.userId == UserGetSetData.Id}
    }
    
    func dataSeletPet(){
        //只留有指定使用者資料
        pet = pet.filter{ $0.status == 1}
    }


}

