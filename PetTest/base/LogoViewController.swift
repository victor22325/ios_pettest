//
//  LogoViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/3/2.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

//用於無功能只需要放LOGO的View

import UIKit

class LogoViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
