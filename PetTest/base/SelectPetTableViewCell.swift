//
//  SelectPetTableViewCell.swift
//  HenrySchein
//
//  Created by BMBMAC on 2017/8/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class SelectPetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var petNameLabel: UILabel!
    @IBOutlet weak var petSpeciesLabel: UILabel!
    @IBOutlet weak var petBreedLabel: UILabel!
    @IBOutlet weak var petWeightLabel: UILabel!
    @IBOutlet weak var petBirthLabel: UILabel!
    @IBOutlet weak var petDiabetesLabel: UILabel!
    @IBOutlet weak var petSelectLabel: UILabel!
    
    @IBOutlet weak var petImageView: UIImageView!
    @IBOutlet weak var petGenderImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
