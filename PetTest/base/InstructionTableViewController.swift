//
//  InstructionTableViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/9.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import EZLoadingActivity
import SwiftyJSON
import MediaPlayer
import AVKit
import AudioToolbox

class InstructionTableViewController: UITableViewController {
    
    @IBOutlet weak var reloadUIButton: UIButton!
    
    var loading: Bool = false
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    var data:[Data] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back", style: .plain, target: self, action: #selector(backAction))
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    
        startSOAP()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return json["data"].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! InstructionTableViewCell
        
        //Configure the cell...
        cell.titleLable.text = self.json["data"][indexPath.row]["name"].string!
        cell.contentLable.text = self.json["data"][indexPath.row]["summary"].string!
        cell.cellImage.image = UIImage(data: self.data[indexPath.row])!

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if Reachability.isConnectedToNetwork() == true {
            let url = URL(string: self.json["data"][indexPath.row]["media"].string!)
            let player = AVPlayer(url: url! as URL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        else{
            alert(alertText: "No internet avaliable!")
        }
    }
    
    @IBAction func actionButton(sender: UIButton) {
        if sender == reloadUIButton {
            startSOAP()
        }
    }
    
    func startSOAP(){
        EZLoadingActivity.show("Loading...", disableUI: true)
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellSoapService()
            
            let queue = DispatchQueue(label:"instructuin",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 20
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.soapConnect && i < timeOut)
                self.loading = true
                self.checkInstruction()
            }
            
        } else {
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
    }
    
    
    func cellSoapService() {
        
        let soapFunc = "getInstructionList"
        let data:NSDictionary = [
            "" : ""
        ]
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            self.soapConnect = status
            self.soapResult = self.json["status"].string!
            
        }
    }
    
    func checkInstruction(){
        if(self.soapConnect){
            if(self.soapResult == "true"){
                for i in 0 ..< json["data"].count {
                    let url = URL(string: self.json["data"][i]["thumb"].string!)
                    self.data.append(try! Data(contentsOf: url!))
                }

                EZLoadingActivity.Settings.SuccessText = "Success"
                EZLoadingActivity.hide(true, animated: false)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.tableView.reloadData()
                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "Fail"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    @objc func backAction(){
        dismiss(animated: true, completion: nil)
    }
    
    func alert (alertText : String){
        let alertController = UIAlertController(title: "Warning", message: alertText, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }


}
