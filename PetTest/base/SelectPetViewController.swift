//
//  SelectPetViewController.swift
//  HenrySchein
//
//  Created by BMBMAC on 2017/8/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import Presentr
import CoreData
import EZLoadingActivity
import SwiftyJSON

class SelectPetViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate{
    
    @IBOutlet weak var selectPetTableView: UITableView!
    @IBOutlet weak var addPetButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    var message: String = ""
    
    var pet:[PetModel] = []
    var fetchResultController: NSFetchedResultsController<PetModel>!
    
    var glucose:[GlucoseModel] = []
    var fetchGlucoseResultController: NSFetchedResultsController<GlucoseModel>!
    
    var noteList:[NoteModel] = []
    var fetchNoteResultController: NSFetchedResultsController<NoteModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        dataSeletUser()
        
        if Reachability.isConnectedToNetwork() != true {
            uploadButton.isHidden = true
            downloadButton.isHidden = true
        }
        //hidden upload and download
        uploadButton.isHidden = true
        downloadButton.isHidden = true

        NotificationCenter.default.addObserver(self, selector: #selector(reloadPetList), name: NSNotification.Name(rawValue: "reloadPetList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeSelf), name: NSNotification.Name(rawValue: "removeSelectPetViewController"), object: nil)
    }
    
    func loadData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<PetModel> = PetModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "petName",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    pet = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //取得指定使用者資料
    func dataSeletUser(){
        //只留有指定使用者資料
        pet = pet.filter{ $0.userId == UserGetSetData.Id}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIndentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! SelectPetTableViewCell
        
        
        let petData = pet[indexPath.row]
        
        //cell.petImageView.image = UIImage(data: petData.petImage as! Data)
        cell.petNameLabel.text = petData.petName
        cell.petSpeciesLabel.text = petData.petSpecies
        cell.petBreedLabel.text = petData.petBreed
        cell.petWeightLabel.text = "\(petData.petWeight) \(setWeight(weightUnit: petData.petWeightUnit))"
        cell.petBirthLabel.text = petData.petBirth
        cell.petDiabetesLabel.text = setDiabetes(petData: petData)
        cell.petGenderImageView.image = setImage(gender: petData.petGender!)
        cell.petImageView.image = setPhoto(petData: petData)
        
        if(petData.status == 0){
            cell.petSelectLabel.isHidden = true
        }
        else{
            cell.petSelectLabel.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        
        for i in 0 ..< pet.count{
            if(i == indexPath.row){
                let petItem = pet[i]
                petItem.status = 1
                UserGetSetData.PetName = petItem.petName!
                UserGetSetData.PetSpecies = petItem.petSpecies!
                UserGetSetData.PetBreed = petItem.petBreed!
                UserGetSetData.Weight = "\(petItem.petWeight)"
                UserGetSetData.PetDiabetesType =  petItem.petDiabetesType!
                UserGetSetData.PetDiabetesYear = petItem.petDiabetesYear!
                UserGetSetData.PetGender = petItem.petGender!
                UserGetSetData.PetBirth = petItem.petBirth!
                UserGetSetData.PreMealUpper = "\(petItem.petGlucoseUpper)"
                UserGetSetData.PreMealLower = "\(petItem.petGlucoseLower)"
                UserGetSetData.PostMealUpper = "\(petItem.petGlucoseUpper)"
                UserGetSetData.PostMealLower = "\(petItem.petGlucoseLower)"
            }
            else{
                pet[i].status = 0
            }
        }
        selectPetTableView.reloadData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCalendar"), object: nil)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        //Edit Button
        let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: " Edit ", handler: { (action, indexPath) -> Void in
            Pet.petItem = self.pet[indexPath.row]
            self.performSegue(withIdentifier: "setPetInfomation", sender: self)
        })
        
        //Delete button
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete",handler: { (action, indexPath) -> Void in
            if(self.pet.count > 1 && self.pet[indexPath.row].status == 0){
                let petName = self.pet[indexPath.row].petName
                
                if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                    let petItem = self.pet[indexPath.row]
                    let context = appDelegate.persistentContainer.viewContext
                    context.delete(petItem)
                    appDelegate.saveContext()
                    self.reloadPetList()
                    _ = self.navigationController?.popViewController(animated: true)
   
                }
                
                self.loadGlucoseData()
                self.dataSeletGlucoseUser()
                self.dataSeletPet(petName: petName!)
                self.loadNoteData()
                self.selectNoteData()
                
                
                for i in 0 ..< self.glucose.count{
                    if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                        let context = appDelegate.persistentContainer.viewContext
                        context.delete(self.glucose[i])
                        appDelegate.saveContext()
                    }
                }
                
                for i in 0 ..< self.noteList.count{
                    if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                        let context = appDelegate.persistentContainer.viewContext
                        context.delete(self.noteList[i])
                        appDelegate.saveContext()
                    }
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
            }
            else if(self.pet[indexPath.row].status != 0){
                self.alert(alertText : "You can't delete current select pet!")
            }
            else{
                self.alert(alertText : "You need at least one pet information!")
            }

        })
        
        
        shareAction.backgroundColor = UIColor(red: 68.0/255.0, green: 108.0/255.0, blue: 179.0/255.0, alpha: 1.0)
        deleteAction.backgroundColor = UIColor(red: 210.0/255.0, green: 77.0/255.0, blue: 87.0/255.0, alpha: 1.0)
        
        return [deleteAction, shareAction]
        
    }
    
    func loadGlucoseData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<GlucoseModel> = GlucoseModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchGlucoseResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchGlucoseResultController.delegate = self
            
            do{
                try fetchGlucoseResultController.performFetch()
                if let fetchedObjects = fetchGlucoseResultController.fetchedObjects{
                    glucose = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //取得指定使用者資料
    func dataSeletGlucoseUser(){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.userId == UserGetSetData.Id}
    }
    
    //取得指定寵物資料
    func dataSeletPet(petName: String){
        //只留有指定使用者資料
        glucose = glucose.filter{ $0.petName == petName}
    }
    
    func loadNoteData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<NoteModel> = NoteModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "createDate",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchNoteResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchNoteResultController.delegate = self
            
            do{
                try fetchNoteResultController.performFetch()
                if let fetchedObjects = fetchNoteResultController.fetchedObjects{
                    noteList = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    //取對應Note Data
    func selectNoteData(){
        var noteTemp:[NoteModel] = []
        for i in  0 ..< glucose.count{
            for j in 0 ..< noteList.count{
                if(noteList[j].bloodId == glucose[i].id){
                    noteTemp.append(noteList[j])
                    break
                }
            }
        }
        noteList = noteTemp
    }


    
    
    @IBAction func actionButton(sender: UIButton) {
        if(sender == addPetButton){
            Pet.petItem = nil
            self.performSegue(withIdentifier: "setPetInfomation", sender: self)
        }
        else if(sender == uploadButton){
            uploadDataAlet()
        }
        else if(sender == downloadButton){
            soapData(mode: "download")
        }
        
    }
    
    func setPhoto(petData: PetModel)->UIImage{
        if(petData.petImage != nil){
            return UIImage(data: petData.petImage as! Data)!
        }
        else{
            if(petData.petSpecies == "Canine"){
                return UIImage(named: "dog_list")!
            }
            else{
                return UIImage(named: "cat_list")!
            }
        }
    }
    
    func setWeight(weightUnit: String?)->String{
        var result = "lb"
        if(weightUnit == "lb"){
            result = "lb"
        }
        else if(weightUnit == "kg"){
            result = "kg"
        }
        
        return result
    }
    
    func setDiabetes(petData: PetModel)->String{
        var result = "\(petData.petDiabetesType!)"

        if(petData.petDiabetesType == "type1" || petData.petDiabetesType == "type2"){
            result = result + " \(petData.petDiabetesYear!)year"
        }
        
        return result
        
    }
    
    func setImage(gender: String)->UIImage{
        switch gender {
        case "male":
            return  UIImage(named: "male")!
        case "Female":
            return  UIImage(named: "female")!
        default:
            return  UIImage(named: "male")!
        }
    }
    
    func uploadDataAlet(){
        let alert = UIAlertController(title: "Backup Data", message: "Are you sure to backup all local data to cloud(This action will remove all cloud data)", preferredStyle: .alert)
        let foreheadAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            
        }
        let ambientAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.soapData(mode: "upload")
            print(self.setUploadString())
        }
        alert.addAction(foreheadAction)
        alert.addAction(ambientAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func soapData(mode: String){
        EZLoadingActivity.show("Loading...", disableUI: true)
        if Reachability.isConnectedToNetwork() == true {
            
            self.cellSoapService(mode: mode)
            
            let queue = DispatchQueue(label:"downloadPet",qos: DispatchQoS.userInitiated)
            
            queue.async {
                var i = 0
                let timeOut = 25
                repeat{
                    Thread.sleep(forTimeInterval: 1)
                    i = i+1
                }while(!self.soapConnect && i < timeOut)
                
                self.checkPetData(mode: mode)
            }
        }
        else{
            let delayInSeconds = 1.0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                EZLoadingActivity.Settings.FailText = "No internet avaliable!"
                EZLoadingActivity.hide(false, animated: false)
            }
        }

    }
    
    func setUploadString()->String{
        var result:String = ""
        
        for i in  0 ..< pet.count{
            result = result + "\(UserGetSetData.Id)-\(pet[i].petName!)-\(pet[i].petSpecies!)-\(pet[i].petBreed!)-\(pet[i].petWeight)-\(pet[i].petDiabetesType!)-\(pet[i].petDiabetesYear!)-\(pet[i].petGender!)-\(pet[i].petBirth!)-\(pet[i].petGlucoseUpper)-\(pet[i].petGlucoseLower)-0-\(pet[i].status)-,"
        }
        
        return result
    }
    
    func cellSoapService(mode: String) {
        
        var soapFunc = "downloadPetList"
        
        if(mode == "download"){
            soapFunc = "downloadPetList"
        }
        else if(mode == "upload"){
            soapFunc = "editPetList"
        }
        
        var data:NSDictionary = [
            "id": "\(UserGetSetData.Id)"
        ]
        
        if(mode == "download"){
            data = [
                "id": "\(UserGetSetData.Id)"
            ]

        }
        else if(mode == "upload"){
            data = [
                "petuploadjson": "\(setUploadString())",
                "id": "\(UserGetSetData.Id)"
            ]
        }
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.soapConnect = status
            self.soapResult = self.json["status"].string!
            self.message = self.json["message"].string!
            
        }
    }
    
    func checkPetData(mode: String){
        if(self.soapConnect){
            print("YES")
            if(self.soapResult == "true"){
                if(mode == "download"){
                    if(json["data"][0][0] != nil){
                    setPetDownloadData(data:json["data"][0][0].string!)
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(json["data"][0][0].string!.components(separatedBy: ",").count)/10) {
                        EZLoadingActivity.Settings.SuccessText = "\(self.message)"
                        EZLoadingActivity.hide(true, animated: false)
                    }
                    }
                    else{
                        EZLoadingActivity.Settings.SuccessText = "No data"
                        EZLoadingActivity.hide(true, animated: false)
                    }

                }
                else if(mode == "upload"){
                    EZLoadingActivity.Settings.SuccessText = "\(self.message)"
                    EZLoadingActivity.hide(true, animated: false)

                }
            }
            else{
                EZLoadingActivity.Settings.FailText = "\(self.message)"
                EZLoadingActivity.hide(false, animated: false)
            }
        }
        else{
            EZLoadingActivity.Settings.FailText = "Please try argin!"
            EZLoadingActivity.hide(false, animated: false)
        }
        self.soapConnect = false
    }
    
    func setPetDownloadData(data:String){
    
        let getData = data.components(separatedBy: ",")
        for i in 0 ..< getData.count-1{
            let petData = getData[i]
            let petDataArray =  petData.components(separatedBy: "-")
            if(checkPetName(name:petDataArray[1])){
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(i)/10) {
                    if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                        let petItem = PetModel(context: appDelegate.persistentContainer.viewContext)
                        petItem.userId = petDataArray[0].trimmingCharacters(in: .whitespaces)
                        petItem.petName = petDataArray[1].trimmingCharacters(in: .whitespaces)
                        petItem.petSpecies = petDataArray[2].trimmingCharacters(in: .whitespaces)
                        petItem.petBreed = petDataArray[3].trimmingCharacters(in: .whitespaces)
                        petItem.petWeight = Float(Int64(petDataArray[4])!)
                        petItem.petDiabetesType = petDataArray[5].trimmingCharacters(in: .whitespaces)
                        petItem.petDiabetesYear = petDataArray[6].trimmingCharacters(in: .whitespaces)
                        petItem.petGender = petDataArray[7].trimmingCharacters(in: .whitespaces)
                        petItem.petBirth = petDataArray[8].trimmingCharacters(in: .whitespaces)
                        petItem.petGlucoseLower = Int64(petDataArray[10])!
                        petItem.petGlucoseUpper = Int64(petDataArray[9])!
                        petItem.status = 0
                        
                        appDelegate.saveContext()
                    }
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(getData.count)/10) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPetList"), object: nil)
        }
        
    }
    
    func checkPetName(name:String)->Bool{
        var result = true
        for i in 0 ..< pet.count{
            if(pet[i].petName == name){
                result = false
            }
        }
        
        return result
    }

    
    //彈出警示視窗
    func alert (alertText : String){
        let alertController = UIAlertController(title: "Warning", message: alertText, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func reloadPetList(){
        loadData()
        dataSeletUser()
        selectPetTableView.reloadData()
    }
    
    @objc func removeSelf(){
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    
}

// MARK: - Presentr Delegate

extension SelectPetViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension SelectPetViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}
