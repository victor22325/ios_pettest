
import UIKit
import Presentr

class MenuBarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var menuBarView: UIView!
    @IBOutlet weak var menuBarOption: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var selectPetView: UIView!
    @IBOutlet weak var instructionView: UIView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var versionView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var exitView: UIView!
    
    @IBOutlet weak var menuBarTableView: UITableView!
    
    var menuBarList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dismissGesture = UITapGestureRecognizer(target: self, action:  #selector(dismissAction))
        dismissGesture.cancelsTouchesInView = false
        self.menuBarView.addGestureRecognizer(dismissGesture)

        let profileGesture = UITapGestureRecognizer(target: self, action: #selector(profileAction))
        profileGesture.cancelsTouchesInView = false
        self.profileView.addGestureRecognizer(profileGesture)
        
        let selectPetGesture = UITapGestureRecognizer(target: self, action: #selector(selectPetAction))
        selectPetGesture.cancelsTouchesInView = false
        self.selectPetView.addGestureRecognizer(selectPetGesture)
        
        let instructionGesture = UITapGestureRecognizer(target: self, action: #selector(instructionAction))
        instructionGesture.cancelsTouchesInView = false
        self.instructionView.addGestureRecognizer(instructionGesture)
        
        let aboutGesture = UITapGestureRecognizer(target: self, action: #selector(aboutAction))
        aboutGesture.cancelsTouchesInView = false
        self.aboutView.addGestureRecognizer(aboutGesture)
        
        let versionGesture = UITapGestureRecognizer(target: self, action: #selector(versionAction))
        versionGesture.cancelsTouchesInView = false
        self.versionView.addGestureRecognizer(versionGesture)
        
        let logoutGesture = UITapGestureRecognizer(target: self, action: #selector(logoutAction))
        logoutGesture.cancelsTouchesInView = false
        self.logoutView.addGestureRecognizer(logoutGesture)
        
        let exitGesture = UITapGestureRecognizer(target: self, action: #selector(exitAction))
        exitGesture.cancelsTouchesInView = false
        self.exitView.addGestureRecognizer(exitGesture)
        
        menuBarList.append("Profile")
        menuBarList.append("Select Pet")
        menuBarList.append("Instruction")
        menuBarList.append("About")
        menuBarList.append("Version")
        menuBarList.append("Logout")
        menuBarList.append("Exit")
        
        self.menuBarTableView.delegate = self
        self.menuBarTableView.dataSource = self
        
        self.menuBarTableView.tableHeaderView?.frame = CGRect.zero
        self.menuBarTableView.tableFooterView?.frame = CGRect.zero
        self.menuBarTableView.bounces = false
        
        self.menuBarTableView.isHidden = true
    }
    
    @objc func dismissAction() {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func profileAction() {
        self.dismissAction()
        let storyboard = UIStoryboard(name: "Main",bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func selectPetAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.fluid(percentage: 0.80)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = false
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base",bundle:nil)
        let popupViewController: SelectPetViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "selectPetViewController")
            return popupViewController as! SelectPetViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    @objc func instructionAction() {
        let storyboard = UIStoryboard(name: "Base",bundle:nil)
        let controller =    storyboard.instantiateViewController(withIdentifier: "instruction")
        present(controller, animated: true, completion:nil)
    }
    
    @objc func aboutAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.fluid(percentage: 0.30)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base",bundle:nil)
        let popupViewController: aboutViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "aboutViewController")
            return popupViewController as! aboutViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    @objc func versionAction() {
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.custom(size: 200)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Base",bundle:nil)
        let popupViewController: versionViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "versionViewController")
            return popupViewController as! versionViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    @objc func logoutAction() {
        UserGetSetData.Login_Status = false
        UserGetSetData.User_logout = true
        Notification.sharedInstance.removeAllNotification()
        let storyboard = UIStoryboard(name: "Main",bundle:nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "login")
        present(controller, animated: true, completion: nil)
    }
    
    @objc func exitAction() {
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuBarList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuBarCell", for: indexPath)
        cell.textLabel?.text = menuBarList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(menuBarList[indexPath.row])
        switch menuBarList[indexPath.row] {
        case "Profile":
            let storyboard = UIStoryboard(name: "Main",bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "editUser")
            self.present(controller, animated: true, completion: nil)
            break

        case "Select Pet":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.80)
                let customType = PresentationType.custom(width: width, height: height, center: .center)

                let presenter = Presentr(presentationType: customType)

                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = false
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: SelectPetViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "selectPetViewController")
                return popupViewController as! SelectPetViewController
            }()

            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break

        case "Instruction":
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "instruction")
            present(controller, animated: true, completion: nil)
            break

        case "About":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.fluid(percentage: 0.30)
                let customType = PresentationType.custom(width: width, height: height, center: .center)

                let presenter = Presentr(presentationType: customType)

                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: aboutViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "aboutViewController")
                return popupViewController as! aboutViewController
            }()

            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break

        case "Version":
            let presenter: Presentr = {
                let width = ModalSize.fluid(percentage: 0.90)
                let height = ModalSize.custom(size: 200)
                let customType = PresentationType.custom(width: width, height: height, center: .center)

                let presenter = Presentr(presentationType: customType)

                presenter.transitionType = TransitionType.coverHorizontalFromRight
                presenter.dismissOnSwipe = true
                return presenter
            }()
            let storyboard = UIStoryboard(name: "Base",bundle:nil)
            let popupViewController: versionViewController = {
                let popupViewController = storyboard.instantiateViewController(withIdentifier: "versionViewController")
                return popupViewController as! versionViewController
            }()

            presenter.transitionType = nil
            presenter.dismissTransitionType = nil
            presenter.keyboardTranslationType = .compress
            customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
            break

        case "Logout":
            UserGetSetData.Login_Status = false
            UserGetSetData.User_logout = true
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "login")
            present(controller, animated: true, completion: nil)
            break

        case "Exit":
            UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            break

        default:
            print(menuBarList[indexPath.row] + " no handle")
            break
        }
    }
 
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}
