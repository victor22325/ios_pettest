//
//  PetInformationViewController.swift
//  HenrySchein
//
//  Created by BMBMAC on 2017/8/22.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import DatePickerDialog
import EZLoadingActivity
import AudioToolbox
import Presentr
import CoreData

class PetInformationViewController: UIViewController , SSRadioButtonControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate , UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,NSFetchedResultsControllerDelegate{
    
    let species = ["Canine","Feline"]
    
    let gender = ["male","Female"]
    
    @IBOutlet weak var petNameTextField: UITextField!
    @IBOutlet weak var petSpeciesTextField: UITextField!
    @IBOutlet weak var petBreedTextField: UITextField!
    @IBOutlet weak var petWeightTextField: UITextField!
    @IBOutlet weak var petGenderTextField: UITextField!
    @IBOutlet weak var petGlucoseLowerTextField: UITextField!
    @IBOutlet weak var petGlucoseUppperTextField: UITextField!
    
    @IBOutlet weak var petDibetesYearLabel: UILabel!
    @IBOutlet weak var petDibetesYearTextField: UITextField!
    
    @IBOutlet weak var petBirthButton: UIButton!
    
    @IBOutlet weak var lbButton: UIButton!
    @IBOutlet weak var kgButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var diabetesNAbutton: UIButton!
    @IBOutlet weak var diabetesTypeOnebutton: UIButton!
    @IBOutlet weak var diabetesTypeTwobutton: UIButton!
    
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet var avoidingView: UIView!
    
    var petBirth = ""
    var soapConnect: Bool = false
    var soapResult: String = ""
    var message: String = ""
    var petDiabetesType = ""
    
    var radioButtonController: SSRadioButtonsController?
    
    var pet: PetModel!
    var petAll:[PetModel] = []
    var fetchResultController: NSFetchedResultsController<PetModel>!
    
    var notePhoto: NSData? = nil
    var petWeightUnit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Back", style: .plain, target: self, action: #selector(backAction))
        
        self.hideKeyboardWhenTappedAround()
        
        setMode()
        
        //init
        setKeyboard()
        
        KeyboardAvoiding.avoidingView = self.avoidingView
        
        radioButtonController = SSRadioButtonsController(buttons: diabetesNAbutton, diabetesTypeOnebutton, diabetesTypeTwobutton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        let speciesPickerView = UIPickerView()
        speciesPickerView.delegate = self
        speciesPickerView.tag = 1
        petSpeciesTextField.inputView = speciesPickerView
        
        
        let genderPickerView = UIPickerView()
        genderPickerView.delegate = self
        genderPickerView.tag = 2
        petGenderTextField.inputView = genderPickerView
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectButton(aButton: UIButton?) {
        
    }
    
    func setKeyboard(){
        petWeightTextField.keyboardType = UIKeyboardType.decimalPad
        petGlucoseLowerTextField.keyboardType = UIKeyboardType.decimalPad
        petGlucoseUppperTextField.keyboardType = UIKeyboardType.decimalPad
        petDibetesYearTextField.keyboardType = UIKeyboardType.decimalPad
    }
    
    
    
    @IBAction func actionButton(sender: UIButton) {
        // Yes button clicked
        if sender == diabetesNAbutton {
            setClick(sender:diabetesNAbutton)
            petDiabetesType = "na"
        }
        else if sender == diabetesTypeOnebutton {
            setClick(sender:diabetesTypeOnebutton)
            petDiabetesType = "type1"
        }
        else if sender == diabetesTypeTwobutton {
            setClick(sender:diabetesTypeTwobutton)
            petDiabetesType = "type2"
        }
            
        else if(sender == imageButton){
            //photo 相關
            if(notePhoto == nil){
                photoAlert()
            }
            else{
                let alert = UIAlertController(title: "Please select the action to photo", message: "", preferredStyle: .alert)
                let readAction = UIAlertAction(title: "Read", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    Note.notePhoto = self.notePhoto
                    self.photoRead()
                }
                let newAction = UIAlertAction(title: "New", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    self.photoAlert()
                }
                let DeleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) {
                    (action: UIAlertAction!) -> Void in
                    self.notePhoto = nil
                }
                alert.addAction(readAction)
                alert.addAction(newAction)
                alert.addAction(DeleteAction)
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }

            
        else if(sender == petBirthButton){
            let currentDate = Date()
            var startComponent = DateComponents()
            startComponent.year = -67
            let startDate = Calendar.current.date(byAdding: startComponent, to: currentDate)
            
            var endComponent = DateComponents()
            endComponent.year = 33
            let endDate = Calendar.current.date(byAdding: endComponent, to: currentDate)
            
            DatePickerDialog().show("DatePickerDialog", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: startDate, maximumDate: endDate, datePickerMode: .date) { (date) in
                if let dateCheck = date {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    
                    self.petBirthButton.setTitle("\(dateFormatter.string(from: dateCheck))",for: .normal)
                    
                    self.petBirth = dateFormatter.string(from: dateCheck)
                }
            }
        }
            
        else  if sender == saveButton {
            let check = checkout()
            
            if check == "OK"{
                
                if(Pet.petItem != nil){
                    Pet.petItem!.petSpecies = petSpeciesTextField.text!
                    Pet.petItem!.petBreed = petBreedTextField.text!
                    if let weight = Float(petWeightTextField.text!) {
                         Pet.petItem!.petWeight = weight
                    }
                    Pet.petItem!.petDiabetesType = petDiabetesType
                    if let diabetesYear = petDibetesYearTextField.text {
                        Pet.petItem!.petDiabetesYear = diabetesYear
                    }
                    if let gender = petGenderTextField.text {
                        Pet.petItem!.petGender = gender
                    }
                    Pet.petItem!.petBirth = petBirth
                    if let glucoseLower = Int64(petGlucoseLowerTextField.text!) {
                        Pet.petItem!.petGlucoseLower = glucoseLower
                    }
                    if let glucoseUpper = Int64(petGlucoseUppperTextField.text!) {
                        Pet.petItem!.petGlucoseUpper = glucoseUpper
                    }
                    Pet.petItem!.petImage = notePhoto as Data?
                    Pet.petItem?.petWeightUnit = petWeightUnit
                    
                    if(Pet.petItem?.status == 1){
                        UserGetSetData.PetSpecies = petSpeciesTextField.text!
                        UserGetSetData.PetBreed = petBreedTextField.text!
                        if let weight = petWeightTextField.text {
                            UserGetSetData.PetWeight = weight
                        }
                        UserGetSetData.PetDiabetesType =  petDiabetesType
                        if let diabetesYear = petDibetesYearTextField.text {
                            UserGetSetData.PetDiabetesYear = diabetesYear
                        }
                        if let gender = petGenderTextField.text {
                            UserGetSetData.PetGender = gender
                        }
                        UserGetSetData.PetBirth = petBirth
                        if let glucoseLower = petGlucoseLowerTextField.text {
                            UserGetSetData.PreMealLower = glucoseLower
                            UserGetSetData.PostMealLower = glucoseLower
                        }
                        if let glucoseUpper = petGlucoseUppperTextField.text {
                            UserGetSetData.PreMealUpper = glucoseUpper
                            UserGetSetData.PostMealUpper = glucoseUpper
                        }
                        UserGetSetData.WeightUnit = petWeightUnit
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadAction"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadNote"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCalendar"), object: nil)
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPetList"), object: nil)
                    
                    backAction()
                }
                else{
                    if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                        pet = PetModel(context: appDelegate.persistentContainer.viewContext)
                        pet.userId = UserGetSetData.Id
                        pet.petName = petNameTextField.text!
                        pet.petSpecies = petSpeciesTextField.text!
                        pet.petBreed = petBreedTextField.text!
                        if let weight = Float(petWeightTextField.text!) {
                            self.pet.petWeight = weight
                        }
                        pet.petDiabetesType = petDiabetesType
                        if let diabetesYear = petDibetesYearTextField.text {
                            self.pet.petDiabetesYear = diabetesYear
                        }
                        if let gender = petGenderTextField.text {
                            self.pet.petGender = gender
                        }
                        pet.petBirth = petBirth
                        if let glucoseLower = Int64(petGlucoseLowerTextField.text!) {
                            self.pet.petGlucoseLower = glucoseLower
                        }
                        if let glucoseUpper = Int64(petGlucoseUppperTextField.text!) {
                            self.pet.petGlucoseUpper = glucoseUpper
                        }
                        pet.petImage = notePhoto as Data?
                        pet.status = 0
                        pet.petWeightUnit = petWeightUnit
                        
                        appDelegate.saveContext()
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPetList"), object: nil)
                        backAction()
                    }
                }
            }
            else {
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                alert(alertText: check)
            }
        }
        
        
    }
    
    func setClick(sender: UIButton){
        if sender == diabetesNAbutton {
            petDibetesYearTextField.isHidden = true
            petDibetesYearLabel.isHidden = true
        }
        else if sender == diabetesTypeOnebutton {
            petDibetesYearTextField.isHidden = false
            petDibetesYearLabel.isHidden = false
        }
        else if sender == diabetesTypeTwobutton {
            petDibetesYearTextField.isHidden = false
            petDibetesYearLabel.isHidden = false
        }
        
    }
    
    func setButton(sender: UIButton){
        if sender == diabetesNAbutton {
            diabetesNAbutton.isSelected = true
            diabetesTypeOnebutton.isSelected = false
            diabetesTypeTwobutton.isSelected = false
        }
        else if sender == diabetesTypeOnebutton {
            diabetesNAbutton.isSelected = false
            diabetesTypeOnebutton.isSelected = true
            diabetesTypeTwobutton.isSelected = false
            
        }
        else if sender == diabetesTypeTwobutton {
            diabetesNAbutton.isSelected = false
            diabetesTypeOnebutton.isSelected = false
            diabetesTypeTwobutton.isSelected = true
            
        }
        
    }
    
    
    func checkout() -> String{
        var result = "OK";
        
        if(petNameTextField.text?.isEmpty)! {
            result = "Name can't not be empty"
        }
        else if(petSpeciesTextField.text?.isEmpty)! {
            result = "Pecies can't not be empty"
        }
//        else if(petBreedTextField.text?.isEmpty)! {
//            result = "Breed can't not be empty"
//        }
//        else if(petWeightTextField.text?.isEmpty)! {
//            result = "Weight can't not be empty"
//        }
//        else if(petGenderTextField.text?.isEmpty)! {
//            result = "Gender can't not be empty"
//        }
//        else if(petBirth == "") {
//            result = "Date of birth can't not be empty"
//        }
//        else if(petGlucoseUppperTextField.text?.isEmpty)! {
//            result = "Glucose upper can't not be empty"
//        }
//        else if(petGlucoseLowerTextField.text?.isEmpty)! {
//            result = "Glucose lower can't not be empty"
//        }
//        else if((petDiabetesType == "type1" || petDiabetesType == "type2") && (petDibetesYearTextField.text?.isEmpty)!){
//            result = "Diagnosed year can't not be empty"
//        }
//        else{
//            let glucoseUpper: Int =  Int(petGlucoseUppperTextField.text!)!
//            let glucoselower: Int =  Int(petGlucoseLowerTextField.text!)!
//
//            if glucoseUpper <= glucoselower {
//                result = "Glucose lower >= upper"
//            }
//        }
        
        if(checkPetName()){
            result = "Pet Name is exists"
        }
        
        return result
    }
    
    func checkPetName()->Bool{
        var result = false
        var tempPetName = ""
        
        loadData()
        for i in 0 ..< petAll.count{
            if(petAll[i].petName == petNameTextField.text){
                result = true
            }
        }
        
        if(Pet.petItem != nil){
            if(Pet.petItem?.petName == petNameTextField.text){
                result = false
            }
        }
        
        return result
    }
    
    func loadData(){
        //從資料儲存區讀取
        let fethRequest :NSFetchRequest<PetModel> = PetModel.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "petName",ascending: true)
        fethRequest.sortDescriptors = [sortDescriptor]
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
            let context = appDelegate.persistentContainer.viewContext
            fetchResultController = NSFetchedResultsController(fetchRequest: fethRequest,managedObjectContext: context,sectionNameKeyPath: nil,cacheName: nil)
            fetchResultController.delegate = self
            
            do{
                try fetchResultController.performFetch()
                if let fetchedObjects = fetchResultController.fetchedObjects{
                    petAll = fetchedObjects
                }
            }
            catch{
                print(error)
            }
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return species.count
        }
        
        if pickerView.tag == 2 {
            return gender.count
        }
        
        
        return 0
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            petSpeciesTextField.text = species[0]
            return species[row]
        }
        
        if pickerView.tag == 2 {
            petGenderTextField.text = gender[0]
            return gender[row]
        }
        
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            petSpeciesTextField.text = species[row]
        }
        
        if pickerView.tag == 2 {
            petGenderTextField.text = gender[row]
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func alert (alertText : String){
        let alertController = UIAlertController(title: "Warning", message: alertText, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
            (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    //修改profile時
    func setMode(){
        if(Pet.petItem != nil){
            saveButton.setTitle("Save", for: .normal)
            petNameTextField.isEnabled = false
            petNameTextField.textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
            
            petNameTextField.text = Pet.petItem?.petName
            petSpeciesTextField.text = Pet.petItem?.petSpecies
            petBreedTextField.text = Pet.petItem?.petBreed
            petWeightTextField.text = "\((Pet.petItem?.petWeight)!)"
            petDibetesYearTextField.text = "\((Pet.petItem?.petDiabetesYear)!)"
            petGenderTextField.text = Pet.petItem?.petGender
            petBirthButton.setTitle("\((Pet.petItem?.petBirth)!)", for: .normal)
            petGlucoseLowerTextField.text = "\((Pet.petItem?.petGlucoseLower)!)"
            petGlucoseUppperTextField.text = "\((Pet.petItem?.petGlucoseUpper)!)"
            
            notePhoto = Pet.petItem?.petImage as NSData?
            
            self.petBirth = (Pet.petItem?.petBirth)!
            
            if(Pet.petItem?.petDiabetesType == "na"){
                setClick(sender: diabetesNAbutton)
                setButton(sender: diabetesNAbutton)
            }
            else if(Pet.petItem?.petDiabetesType == "type1"){
                setClick(sender: diabetesTypeOnebutton)
                setButton(sender: diabetesTypeOnebutton)
            }
            else if(Pet.petItem?.petDiabetesType == "type2"){
                setClick(sender: diabetesTypeTwobutton)
                setButton(sender: diabetesTypeTwobutton)
            }
        }
        else{
            petDiabetesType = "na";
            petDibetesYearTextField.isHidden = true
            petDibetesYearLabel.isHidden = true
        }
        
        if(UserGetSetData.WeightUnit == "0"){
            petWeightChooseLB()
        }
        else{
            petWeightChooseKG()
        }
    }
    
    @IBAction func lbAction(_ sender: Any) {
        if(!lbButton.isSelected) {
           petWeightChooseLB()
        }
    }
    
    @IBAction func kgAction(_ sender: Any) {
        if(!kgButton.isSelected) {
            petWeightChooseKG()
        }
    }
    
    private func petWeightChooseLB(){
        let lb_image = UIImage(named: "lb_selected")
        lbButton.setImage(lb_image, for: .normal)
        let kg_image = UIImage(named: "kilogram_unselected")
        kgButton.setImage(kg_image, for: .normal)
        petWeightUnit = "lb"
    }
    
    private func petWeightChooseKG() {
        let lb_image = UIImage(named: "lb_unselected")
        lbButton.setImage(lb_image, for: .normal)
        let kg_image = UIImage(named: "kilogram_selected")
        kgButton.setImage(kg_image, for: .normal)
        petWeightUnit = "kg"
    }
 
    
    //新增一張圖片 （現有的 或 拍新的）
    func photoAlert(){
        let alert = UIAlertController(title: "Please select the action to add the photo", message: "This allows PetTest App to take a picutre of your cute pet. The purpose is add pet's profile picture information.", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (action: UIAlertAction!) -> Void in
        })
        let selectAction = UIAlertAction(title: "Take Album", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.photoAdd(mode: "Select")
        }
        let newAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            (action: UIAlertAction!) -> Void in
            self.photoAdd(mode: "New")
        }
        alert.addAction(cancelAction)
        alert.addAction(selectAction)
        alert.addAction(newAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //讀取 photo
    func photoRead(){
        let presenter: Presentr = {
            let width = ModalSize.fluid(percentage: 0.90)
            let height = ModalSize.fluid(percentage: 0.80)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let presenter = Presentr(presentationType: customType)
            
            presenter.transitionType = TransitionType.coverHorizontalFromRight
            presenter.dismissOnSwipe = true
            return presenter
        }()
        let storyboard = UIStoryboard(name: "Notes",bundle:nil)
        let popupViewController: ReadPhotoViewController = {
            let popupViewController = storyboard.instantiateViewController(withIdentifier: "ReadPhotoViewController")
            return popupViewController as! ReadPhotoViewController
        }()
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.keyboardTranslationType = .compress
        self.customPresentViewController(presenter, viewController: popupViewController, animated: true, completion: nil)
    }
    
    //新增一張圖片 （現有的 或 拍新的）
    func photoAdd(mode: String){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            if(mode == "Select"){
                imagePicker.sourceType = .photoLibrary
            }
            else{
                imagePicker.sourceType = .camera
            }
            imagePicker.delegate = self
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //處理 photo回傳
    internal func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey: Any]){
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            let tempImage: UIImage? = selectedImage
            if let noteImage = tempImage{
                if let imageData = noteImage.pngData(){
                    notePhoto = NSData(data: imageData)
                }
            }
            
        }
        dismiss(animated: true, completion: nil)
    }

    
    @objc func backAction(){
        dismiss(animated: true, completion: nil)
    }
    
    //設定鍵盤擋住輸入視窗事件
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == petNameTextField || textField == petSpeciesTextField || textField == petBreedTextField || textField == petWeightTextField){
            KeyboardAvoiding.avoidingView = textField
        }
        else{
            KeyboardAvoiding.avoidingView = self.avoidingView
        }
        
        return true
    }
    
    
    
}
