//
//  PopupViewController.swift
//  PresentrExample
//
//  Created by Daniel Lozano on 8/29/16.
//  Copyright © 2016 danielozano. All rights reserved.
//

import UIKit
import Presentr
import EZLoadingActivity
import SwiftyJSON

class versionViewController: UIViewController {

    @IBOutlet weak var nowVersionLabel: UILabel!
    @IBOutlet weak var lastVersionLabel: UILabel!
    @IBOutlet weak var logLabel: UILabel!
    @IBOutlet weak var updateButton: UIButton!
    
    var soapConnect: Bool = false
    var soapResult: String = ""
    var json: JSON = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nowVersionLabel.text = "Loading ..."
        
        lastVersionLabel.isHidden = true
        logLabel.isHidden = true
        updateButton.isHidden = true
        
        
        if Reachability.isConnectedToNetwork() == true {
            self.cellSoapService()
            
            self.nowVersionLabel.text = "Now version : " + Version.versionName
//            let queue = DispatchQueue(label:"version",qos: DispatchQoS.userInitiated)
//
//            queue.async {
//                var i = 0
//                let timeOut = 10
//                repeat{
//                    Thread.sleep(forTimeInterval: 1)
//                    i = i+1
//                }while(!self.soapConnect && i < timeOut)
//
//                DispatchQueue.main.async {
//                    self.lastVersionLabel.isHidden = false
//                    self.logLabel.isHidden = false
//                }
//                self.checkVersion()
//            }
            
        } else {
            self.nowVersionLabel.text = "Now version : " + Version.versionName
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(sender: UIButton) {
        if(sender == updateButton){
            if let url = URL(string: "https://www.apple.com/tw/itunes/charts/free-apps/"){
                UIApplication.shared.open(url)
            }
        }
    }
    
    func cellSoapService() {
        
        let soapFunc = "getIosPetAppVer"
        let data:NSDictionary = [
            "": ""
        ]
        
        //Sent request
        SOAPServices.sharedInstance.postConnect("urn:plgSOAPAPPCall.APPCall", dic: SOAPServices.sharedInstance.setDataDic(soapFunc: soapFunc, data: data)) { (status, result) in
            
            self.json = SOAPServices.sharedInstance.getAppCall(result!)
            print(status)
            print(self.json)
            self.soapConnect = status
            self.soapResult = self.json["status"].string!
            
        }
    }
    
    func checkVersion(){
        if(self.soapConnect){
            if(self.soapResult == "true"){
                Version.lasetVersionCode = self.json["vercode"].string!
                Version.lastVersionName = self.json["vername"].string!
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.nowVersionLabel.text = "Now version : " + Version.versionName
                    self.lastVersionLabel.text = "Last version : " + Version.lastVersionName
                    
                    
                    if(Int(Version.lasetVersionCode)! > Int(Version.versionCode)!){
                        self.logLabel.text = "It has the latest version."
                        self.updateButton.isHidden = false
                    }
                    else{
                        self.logLabel.text = "It is the latest version."
                        self.updateButton.isHidden = true
                    }
                }
            }
        }
        
        self.soapConnect = false
    }
    



}

// MARK: - Presentr Delegate

extension versionViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension versionViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}
