//
//  PopupViewController.swift
//  PresentrExample
//
//  Created by Daniel Lozano on 8/29/16.
//  Copyright © 2016 danielozano. All rights reserved.
//

import UIKit
import Presentr

class aboutViewController: UIViewController {

    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        productLabel.text = Version.aboutProduct
        versionLabel.text = Version.aboutVersion
        idLabel.text = Version.aboutId
        websiteLabel.text = Version.aboutWebsite
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didSelectDone(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionButton(sender: UIButton) {
        
    }


}

// MARK: - Presentr Delegate

extension aboutViewController: PresentrDelegate {
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        print("Dismissing View Controller")
        return !keyboardShowing
    }
    
}

// MARK: - UITextField Delegate

extension aboutViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print("text")
        return true
    }
    
}
