//
//  tmpViewController.swift
//  advocate
//
//  Created by BMBMAC on 2017/5/8.
//  Copyright © 2017年 BMBMAC. All rights reserved.
//

import UIKit

class DeviceViewController: UIViewController,SSRadioButtonControllerDelegate {
    
    @IBOutlet weak var Glucosebutton: UIButton!
    @IBOutlet weak var Pressurebutton: UIButton!
    @IBOutlet weak var Temperaturebutton: UIButton!
    @IBOutlet weak var selectbutton: UIButton!
    
    var radioButtonController: SSRadioButtonsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //放置logo到navigationItem上
        let logo = UIImage(named: "app_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        radioButtonController = SSRadioButtonsController(buttons: Glucosebutton, Pressurebutton, Temperaturebutton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        setButton(Device_Status: UserGetSetData.Device_Status)
    }
    
    func didSelectButton(aButton: UIButton?) {
        
    }
    
    
    @IBAction func actionButton(sender: UIButton) {
        // Yes button clicked
        if sender == Glucosebutton {
            print("glucose")
            UserGetSetData.Device_Status = 0;
        }
        else if sender == Pressurebutton {
            print("pressure")
            UserGetSetData.Device_Status = 1;
        }
        else if sender == Temperaturebutton {
            print("temperature")
            UserGetSetData.Device_Status = 2;
        }
        else if sender == selectbutton {
            Notification.sharedInstance.removeAllNotification()
            let storyboard = UIStoryboard(name: "Main",bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "TabMain")
            self.present(controller, animated: true, completion: nil)

        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setButton(Device_Status: Int){
        if(Device_Status==0){
            Glucosebutton.isSelected = true
            Pressurebutton.isSelected = false
            Temperaturebutton.isSelected = false
        }
        else if(Device_Status==1){
            Glucosebutton.isSelected = false
            Pressurebutton.isSelected = true
            Temperaturebutton.isSelected = false

        }
        else if(Device_Status==2){
            Glucosebutton.isSelected = false
            Pressurebutton.isSelected = false
            Temperaturebutton.isSelected = true

        }
    
    }

}
